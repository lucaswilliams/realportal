<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BatchOrgs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('upload_batches', function($table) {
            //$table->dropColumn('organisation_id');
            
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('axid')->unsigned()->nullable();
    
            $table->foreign('client_id')->references('id')->on('clients');
            //$table->foreign('axid')->references('axid')->on('ax_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
