<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAndRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->boolean('is_system');
        });

        $role_id = DB::table('roles')
            ->insertGetId([
                'name' => 'admin',
                'is_system' => true
            ]);

        Schema::create('user_roles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
        });

        //Add LW as a user
        $user_id = DB::table('users')
            ->insertGetId([
                'first_name' => 'Lucas',
                'last_name' => 'Williams',
                'email' => 'lucas@lucas-williams.com',
                'password' => Hash::make('changeme')
            ]);
        //Give him the admin role.

        DB::table('user_roles')
            ->insert([
                'user_id' => $user_id,
                'role_id' => $role_id
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_roles');
        Schema::drop('roles');
    }
}
