<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('label', 50);
            $table->string('route', 100);
            $table->integer('parent_id')->unsigned()->nullable();

            $table->foreign('parent_id')->references('id')->on('menu_items');
        });

        Schema::create('menu_item_roles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('menu_id')->references('id')->on('menu_items');
            $table->foreign('role_id')->references('id')->on('roles');
        });

        DB::statement('INSERT INTO menu_items (label, route) VALUES (\'Clients\', \'/clients\')');
        DB::statement('INSERT INTO menu_items (label, route, parent_id) SELECT \'Contacts\', \'/contacts\', id FROM menu_items WHERE route = \'/clients\'');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
