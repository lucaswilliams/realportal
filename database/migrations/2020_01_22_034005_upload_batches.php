<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UploadBatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_batches', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_id')->unsigned();
            $table->string('given_name', 100);
            $table->string('middle_name', 100);
            $table->string('surname', 100);
            $table->string('email', 200);
            $table->string('usi', 10);
            $table->date('date_of_birth');
            $table->integer('validated')->unsigned()->default(0);
            $table->integer('contact_id')->unsigned()->nullable();
            $table->integer('organisation_id')->unsigned()->nullable();
        });
        
        Schema::table('clients', function($table) {
            $table->integer('axid')->unsigned()->nullable();
            $table->string('xero_guid', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upload_batches');
    }
}
