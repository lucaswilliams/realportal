<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AuspostCache extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auspost_cache', function(Blueprint $table) {
            $table->integer('item_id')->unique();
            $table->string('delivery_tracking', 50)->nullable();
            $table->string('delivery_status', 50)->nullable();
            $table->string('return_tracking', 50)->nullable();
            $table->string('return_status', 50)->nullable();
            $table->datetime('last_update');
        });

        Schema::create('auspost_log', function(Blueprint $table) {
            $table->datetime('log_date');
            $table->longtext('tracking_ids');
            $table->longtext('response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auspost_cache');
        Schema::drop('auspost_logs');
    }
}
