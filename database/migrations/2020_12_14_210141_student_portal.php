<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StudentPortal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function(Blueprint $table) {
            $table->id('id');
            $table->string('code', 20);
            $table->string('name', 100);
            $table->string('delivery', 20)->nullable();
        });

        Schema::create('course_groups', function(Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->integer('trainer_id')->unsigned();

            $table->foreign('trainer_id')->references('id')->on('users');
        });

        Schema::create('course_group_dates', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('course_group_id');
            $table->date('group_date');
            $table->time('start_time');
            $table->time('end_date');

            $table->foreign('course_group_id')->references('id')->on('course_groups');
        });

        Schema::create('course_group_students', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('course_group_id');
            $table->integer('student_id')->unsigned();

            $table->foreign('course_group_id')->references('id')->on('course_groups');
            $table->foreign('student_id')->references('id')->on('users');
        });

        Schema::create('assessments', function(Blueprint $table) {
            $table->id('id');
            $table->string('name', 100);
        });

        Schema::create('assessment_sections', function(Blueprint $table) {
            $table->id('id');
            $table->string('name', 100);
            $table->longText('introduction');
            $table->foreignId('assessment_id');

            $table->foreign('assessment_id')->references('id')->on('assessments');
        });

        Schema::create('questions', function(Blueprint $table) {
            $table->id('id');
            $table->longText('question_text');
            $table->foreignId('section_id');

            $table->foreign('section_id')->references('id')->on('assessment_sections');
        });

        Schema::create('course_assessments', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('course_id');
            $table->foreignId('assessment_id');

            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('assessment_id')->references('id')->on('assessments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_assessments');
        Schema::drop('questions');
        Schema::drop('assessment_sections');
        Schema::drop('assessments');
        Schema::drop('course_group_students');
        Schema::drop('course_group_dates');
        Schema::drop('course_groups');
        Schema::drop('courses');
    }
}
