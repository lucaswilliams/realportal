<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UrlClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('equipment_options', 'booking_urls');
        Schema::rename('equipment_option_courses', 'booking_url_courses');

        Schema::table('booking_urls', function($table) {
            $table->integer('client_id')->unsigned()->nullable();
            $table->boolean('invoices');

            $table->foreign('client_id')->references('id')->on('clients');
        });

        Schema::table('clients', function($table) {
            $table->string('logo', 255);
        });
    }

    /**
     * Reverse the migrations.e
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function($table) {
            $table->dropColumn('logo');
        });
    }
}
