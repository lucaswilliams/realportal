<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('address_1', 100);
            $table->string('address_2', 100);
            $table->string('suburb', 50);
            $table->string('state', 3);
            $table->string('postcode', 4);
        });

        Schema::create('contacts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->boolean('is_primary')->default(false);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('clients');
        });

        Schema::create('locations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->string('name', 100);
            $table->string('address_1', 100);
            $table->string('address_2', 100);
            $table->string('suburb', 50);
            $table->string('state', 3);
            $table->string('postcode', 4);

            $table->foreign('client_id')->references('id')->on('clients');
        });

        Schema::table('users', function($table) {
            $table->string('work_phone', 20)->nullable();
            $table->string('mobile', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
