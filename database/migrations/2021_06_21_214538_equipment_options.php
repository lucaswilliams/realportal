<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EquipmentOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_options', function(Blueprint $table) {
            $table->id('id');
            $table->string('link_url', 30);
        });

        Schema::create('equipment_option_courses', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('equipment_option_id');
            $table->string('course_id', 100);
            $table->string('coupon_code', 100);
            $table->string('template_id', 100);

            $table->foreign('equipment_option_id')->references('id')->on('equipment_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
