<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CustomFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_fields', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('booking_url_id');
            $table->string('field_title', 100);
            $table->enum('response_type', ['text', 'number', 'email', 'select', 'multi']);

            $table->foreign('booking_url_id')->references('id')->on('booking_urls');
        });
        
        Schema::create('custom_field_options', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('custom_field_id');
            $table->string('value', 100);

            $table->foreign('custom_field_id')->references('id')->on('custom_fields');
        });

        Schema::table('booking_urls', function($table) {
            $table->string('slack_channel', 100)->default('sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
