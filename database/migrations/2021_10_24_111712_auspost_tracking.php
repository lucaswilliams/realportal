<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AuspostTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjobs', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('file', 255);
            $table->integer('time_y');
            $table->integer('time_m');
            $table->integer('time_d');
            $table->integer('time_h');
            $table->integer('time_i');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cronjobs');
    }
}
