<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_files', function(Blueprint $table) {
            $table->id('id');
            $table->string('name', 100);
            $table->string('file', 150);
            $table->boolean('active');
            $table->smallInteger('type')->default(0);
        });

        Schema::create('playlists', function(Blueprint $table) {
            $table->id('id');
            $table->string('name', 100);
            $table->boolean('active');
        });

        Schema::create('playlist_files', function(Blueprint $table) {
            $table->id('id');
            $table->foreignId('playlist_id');
            $table->foreignId('media_id');

            $table->foreign('playlist_id')->references('id')->on('playlists');
            $table->foreign('media_id')->references('id')->on('media_files');
        });

        Schema::create('user_favourites', function(Blueprint $table) {
            $table->id('id');
            $table->unsignedInteger('user_id');
            $table->foreignId('media_id');

            $table->foreign('user_id')->references('id')->on('users');
        });

        $parent = DB::table('menu_items')
            ->insertGetId([
                'label' => 'App Management',
                'route' => '#',
                'menu_order' => 4
            ]);

        DB::table('menu_items')
            ->insert([
                [
                    'label' => 'Media Files',
                    'route' => 'app/files',
                    'parent_id' => $parent,
                    'menu_order' => 1
                ],
                [
                    'label' => 'Playlist',
                    'route' => 'app/playlist',
                    'parent_id' => $parent,
                    'menu_order' => 2
                ]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menu_items')
            ->where('route', 'app/playlist')
            ->orWhere('route', 'app/files')
            ->orWhere('label', 'App Management')
            ->delete();

        Schema::drop('user_favourites');
        Schema::drop('playlist_files');
        Schema::drop('playlists');
        Schema::drop('media_files');
    }
}
