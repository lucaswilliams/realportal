<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingFieldsJan2022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_urls', function($table) {
            $table->boolean('real_response')->default(false);
            $table->boolean('cq_first_aid')->default(false);
        });

        Schema::table('booking_url_courses', function($table) {
            $table->decimal('price', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_urls', function($table) {
            $table->dropColumn('real_response');
            $table->dropColumn('cq_first_aid');
        });

        Schema::table('booking_url_courses', function($table) {
            $table->dropColumn('price');
        });
    }
}
