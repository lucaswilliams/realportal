<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Coupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_types', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('coupons', function(Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_code')->unique();
            $table->unsignedInteger('coupon_type');
            $table->string('courses')->nullable();
            $table->string('locations')->nullable();
            $table->integer('uses')->default(0);
            $table->double('amount', 8, 2);
            $table->integer('amount_type')->default(0);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->integer('usage_type')->default(0);
            $table->integer('date_type')->default(0);
            $table->integer('discount_type')->default(0);

            $table->foreign('coupon_type')->references('id')->on('coupon_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupons');
    }
}
