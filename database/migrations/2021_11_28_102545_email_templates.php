<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmailTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function(Blueprint $table) {
            $table->id('id');
            $table->string('name', 100);
            $table->longText('content');
        });

        Schema::table('booking_urls', function($table) {
            $table->integer('booking_email');
            $table->foreignId('confirmation_template')->nullable();

            $table->foreign('confirmation_template')->references('id')->on('email_templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_urls', function($table) {
            $table->dropForeign('confirmation_template');
            $table->dropColumn('booking_email');
            $table->dropColumn('confirmation_template');
        });
        Schema::drop('email_templates');
    }
}
