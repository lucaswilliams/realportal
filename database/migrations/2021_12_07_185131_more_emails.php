<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoreEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_templates', function($table) {
            $table->integer('single_participant')->default(0);
            $table->integer('multi_participant')->default(0);
            $table->integer('url_confirmation')->default(0);
        });

        Schema::table('booking_urls', function($table) {
            $table->string('confirmation_email', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_templates', function($table) {
            $table->dropColumn('single_participant');
            $table->dropColumn('multi_participant');
            $table->dropColumn('url_confirmation');
        });

        Schema::table('booking_urls', function($table) {
            $table->dropColumn('confirmation_email');
        });
    }
}
