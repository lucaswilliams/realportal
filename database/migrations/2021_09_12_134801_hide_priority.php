<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HidePriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_urls', function($table) {
            $table->integer('priority_courses')->default(0);
            $table->longText('freetext')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_urls', function($table) {
            $table->dropColumn('priority_courses');
            $table->dropColumn('freetext');
        });
    }
}
