<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoreEquipmentOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipment_options', function($table) {
            $table->string('password', 100)->nullable();
            $table->boolean('equipment')->default(0);
            $table->longText('locations')->nullable();
            $table->longText('types')->nullable();

            $table->unique('link_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipment_options', function($table) {
            $table->dropColumn('password');
            $table->dropColumn('equipment');
            $table->dropColumn('locations');
            $table->dropColumn('types');
        });
    }
}
