<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_url_courses', function($table) {
            $table->renameColumn('equipment_option_id', 'booking_url_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_url_courses', function($table) {
            $table->renameColumn('booking_url_id', 'equipment_option_id');
        });
    }
}
