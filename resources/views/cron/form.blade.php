@extends('layouts.app')

@section('content')
    <form method="POST" action="/cron">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$item->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit Task
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Task Name:</label>
                                <input type="text" name="name" id="name" class="form-control" data-validation="required" value="{{$item->name ?? ""}}">
                            </div>

                            <div class="form-group">
                                <label for="file">Action:</label>
                                <input type="text" name="file" id="file" class="form-control" data-validation="required" value="{{$item->file ?? ""}}">
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="time_y">Year <small>(-1 for all)</small>:</label>
                                        <input type="text" name="time_y" id="time_y" class="form-control" data-validation="required" value="{{$item->time_y ?? ""}}">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="time_m">Month <small>(-1 for all)</small>:</label>
                                        <input type="text" name="time_m" id="time_m" class="form-control" data-validation="required" value="{{$item->time_m ?? ""}}">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="time_d">Day <small>(-1 for all)</small>:</label>
                                        <input type="text" name="time_d" id="time_d" class="form-control" data-validation="required" value="{{$item->time_d ?? ""}}">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="time_h">Hour <small>(-1 for all)</small>:</label>
                                        <input type="text" name="time_h" id="time_h" class="form-control" data-validation="required" value="{{$item->time_h ?? ""}}">
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="time_i">Minute <small>(-1 for all)</small>:</label>
                                        <input type="text" name="time_i" id="time_i" class="form-control" data-validation="required" value="{{$item->time_i ?? ""}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-none" id="removed"></div>
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
