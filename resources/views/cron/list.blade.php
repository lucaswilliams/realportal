@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Scheduled Task List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/cron/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add task</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Task Name</th>
                                <th>Frequency</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($cronjobs as $item) {
                            ?>
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->frequency }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/cron/{{$item->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit Task"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/cron/{{$item->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete Task"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
