@extends('layouts.app')

@section('content')
    <form method="POST" action="/clients" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$client->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit client
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Client Name:</label>
                                <input type="text" name="name" id="name" class="form-control" data-validation="required" value="{{$client->name ?? ""}}">
                            </div>

                            <div class="form-group">
                                <label for="address_1">Address:</label>
                                <input type="text" name="address_1" id="address_1" class="form-control" value="{{$client->address_1 ?? ""}}">
                                <input type="text" name="address_2" id="address_2" class="form-control" value="{{$client->address_2 ?? ""}}">
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="suburb">Suburb:</label>
                                        <input type="text" name="suburb" id="suburb" class="form-control" value="{{$client->suburb ?? ""}}">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="state">State:</label>
                                        <select name="state" id="state" class="form-control">
                                            <option value=""></option>
                                            <option value="ACT" {{$client->state == "ACT" ? "selected" : ""}}>ACT</option>
                                            <option value="NSW" {{$client->state == "NSW" ? "selected" : ""}}>NSW</option>
                                            <option value="NT {{$client->state == "NT" ? "selected" : ""}}">NT</option>
                                            <option value="QLD" {{$client->state == "QLD" ? "selected" : ""}}>QLD</option>
                                            <option value="SA" {{$client->state == "SA" ? "selected" : ""}}>SA</option>
                                            <option value="TAS" {{$client->state == "TAS" ? "selected" : ""}}>TAS</option>
                                            <option value="VIC" {{$client->state == "VIC" ? "selected" : ""}}>VIC</option>
                                            <option value="WA" {{$client->state == "WA" ? "selected" : ""}}>WA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="postcode">Postcode:</label>
                                        <input type="text" name="postcode" id="postcode" class="form-control" value="{{$client->postcode ?? ""}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="axid">aXcelerate Organisation</label>
                                        <select name="axid" id="axid" class="select2 form-control">
                                            <option value="{{$client->axid}}">{{$client->ax_name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="xero_guid">Xero Contact <small>(must be an exact match)</small></label>
                                        <select name="xero_guid" id="xero_guid" class="select2 form-control">
                                            <?php if(strlen($client->xero_guid) > 0) { ?>
                                                <option value="{{$client->xero_guid}}" selected="selected">{{$client->xero_name}}</option>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="xero_name" id="xero_name" value="{{$client->xero_name}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group logoform">
                                        <label for="logo">Logo:</label>
                                        <?php if(strlen($client->logo) > 0) { ?>
                                            <br><img src="{{$client->logo}}" class="client-logo"><br>
                                            <a href="#" class="btn btn-danger remove-logo">Remove</a>
                                            <input type="hidden" name="logo" id="logo" value="{{$client->logo}}">
                                        <?php } else { ?>
                                            <input type="file" id="logo" name="logo">
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });

            $('.remove-logo').click(function(e) {
                e.preventDefault();
                $(this).parent().append('<input type="file" id="logo" name="logo">');
                $('.client-logo').remove();
                $('.logoform br').remove();
                $(this).remove();
                $('#logo').remove();
            });

            $('#axid').select2({
                minimumInputLength: 3,
                ajax: {
                    url: function(params) {
                        return '/axcelerate/organisations/';
                    },
                    dataType: 'json',
                    delay: 500,
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id
                                }
                            })
                        };
                    }
                },
                theme: 'bootstrap4'
            });

            $('#xero_guid').select2({
                minimumInputLength: 3,
                ajax: {
                    url: function (params) {
                        return '/xero/contactLookup/';
                    },
                    dataType: 'json',
                    delay: 500,
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id
                                }
                            })
                        };
                    }
                },
                theme: 'bootstrap4'
            });

            var _oldGUID = $('#xero_guid').val();
            var _oldName = $('#xero_name').val();

            $('#xero_guid').on('select2:clear', function(e) {
                $('#xero_guid option').remove();
                $('#xero_name').val('');
            });

            $('#xero_guid').on('select2:select', function (e) {
                console.log(e.params.data.text);
                $('#xero_name').val(e.params.data.text);
            });
        });
    </script>
@endsection
