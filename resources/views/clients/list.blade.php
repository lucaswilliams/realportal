@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Client List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/clients/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add client</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Client Name</th>
                                <th>Primary Contact</th>
                                <th>Suburb</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($clients as $client) {
                            ?>
                                <tr>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->first_name }} {{ $client->last_name }}</td>
                                    <td>{{ $client->suburb }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/clients/{{$client->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit client"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-primary" href="/clients/{{$client->id}}/contacts" data-toggle="tooltip" data-placement="bottom" title="Edit contacts"><i class="fa fa-fw fa-users"></i></a>
                                        <a class="btn btn-primary" href="/clients/{{$client->id}}/locations" data-toggle="tooltip" data-placement="bottom" title="Edit locations"><i class="fa fa-fw fa-map-marker"></i></a>
                                        <a class="btn btn-danger delete" href="/clients/{{$client->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete {{$client->name}}"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
