@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col">
                                <h1>Delete Client</h1>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo $message; ?>

                        <a class="btn btn-primary" href="{{url('/clients')}}">Back to Client List</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
