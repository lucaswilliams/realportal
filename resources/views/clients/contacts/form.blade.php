@extends('layouts.app')

@section('content')
    <form method="POST" action="<?php echo str_replace('/add', '', $_SERVER['REQUEST_URI']); ?>">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{ $contact->id ?? 0 }}">
                    <input type="hidden" name="client_id" value="{{ $client }}">
                    <div class="card">
                        <div class="card-header">
                            Edit contact
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name:</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control" data-validation="required" value="{{ $contact->first_name ?? "" }}">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name:</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control" data-validation="required" value="{{ $contact->last_name ?? "" }}">
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="email" name="email" id="email" class="form-control" data-validation="required" value="{{ $client->email ?? "" }}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="mobile">Mobile:</label>
                                        <input type="text" id="mobile" name="mobile" class="form-control" value="{{ $client->mobile ?? "" }}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="postcode">Work Phone:</label>
                                        <input type="text" name="work_phone" id="work_phone" class="form-control" value="{{$client->work_phone ?? ""}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });
        });
    </script>
@endsection
