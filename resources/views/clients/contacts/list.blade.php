@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Contacts List</h1>
                            </div>
                            <div class="col-3">
                                <a href="contacts/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add contact</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Contact Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($contacts as $contact) {
                            ?>
                            <tr>
                                <td><?php echo ($contact->is_primary == 1 ? '<strong>' : ''); ?>{{ $contact->first_name }} {{ $contact->last_name }}<?php echo ($contact->is_primary == 1 ? '</strong>' : ''); ?></td>
                                <td>{{ $contact->email }}</td>
                                <td>{{ $contact->mobile }}</td>
                                <td>{{ $contact->work_phone }}</td>
                                <td class="text-right" style="width: 172px">
                                    <a href="contacts/{{ $contact->id }}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit contact"><i class="fa fa-fw fa-pencil"></i></a>
                                    <?php if($contact->is_primary == 1) { ?>
                                        <a href="#" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="User is already primary"><i class="fa fa-fw fa-user-plus"></i></a>
                                    <?php } else { ?>
                                        <a href="contacts/{{ $contact->id }}/primary" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Make primary"><i class="fa fa-fw fa-user-plus"></i></a>
                                    <?php } ?>
                                    <a href="contacts/{{ $contact->id }}/delete" class="btn btn-danger delete" data-toggle="tooltip" data-placement="bottom" title="Delete {{ $contact->first_name }} {{ $contact->last_name }}"><i class="fa fa-fw fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
