@extends('layouts.app')

@section('content')
    <form method="POST" action="/coupons">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$coupon->id}}">
                    <div class="card">
                        <div class="card-header">
                            Edit coupon
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="name">Coupon Code:</label>
                                        <input type="text" name="coupon_code" id="coupon_code" class="form-control" data-validation="required" value="{{$coupon->coupon_code ?? ""}}">
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="name">Coupon Type:</label>
                                        <select name="coupon_type" id="coupon_type" class="form-control">
                                            <option value="">(no category)</option>
                                            <?php foreach($couponTypes as $type) {
                                                echo '<option value="'.$type->id.'">'.$type->name.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="name">Coupon Amount:</label>
                                        <input type="text" name="amount" id="amount" class="form-control" data-validation="required" value="{{$coupon->amount ?? ""}}">
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="name">Amount Type:</label>
                                        <select name="amount_type" id="amount_type" class="form-control">
                                            <option value="0"{{$coupon->amount_type == 0 ? ' selected' : ''}}>Dollars</option>
                                            <option value="1"{{$coupon->amount_type == 1 ? ' selected' : ''}}>Percent</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="name">Discount Type:</label>
                                        <select name="discount_type" id="discount_type" class="form-control">
                                            <option value="0"{{$coupon->discount_type == 0 ? ' selected' : ''}}>Training Only</option>
                                            <option value="1"{{$coupon->discount_type == 1 ? ' selected' : ''}}>Whole Price</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="uses">Number of Uses (0 for unlimited):</label>
                                        <input type="text" name="uses" id="uses" class="form-control" data-validation="required" value="{{$coupon->uses ?? ""}}">
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="usage_type">Usage Type:</label>
                                        <select name="usage_type" id="usage_type" class="form-control">
                                            <option value="0"{{$coupon->usage_type == 0 ? ' selected' : ''}}>One usage per participant</option>
                                            <option value="1"{{$coupon->usage_type == 1 ? ' selected' : ''}}>One usage per booking</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="date_type">Availability Dates:</label>
                                        <select name="date_type" id="date_type" class="form-control">
                                            <option value="0"{{$coupon->date_type == 0 ? ' selected' : ''}}>Always available</option>
                                            <option value="1"{{$coupon->date_type == 1 ? ' selected' : ''}}>Courses dated between</option>
                                            <option value="2"{{$coupon->date_type == 2 ? ' selected' : ''}}>Bookings made between</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4 dates">
                                    <div class="form-group">
                                        <label for="start_date">Start Date</label>
                                        <input type="date" name="start_date" id="start_date" value="{{ $coupon->start_date }}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-4 dates">
                                    <div class="form-group">
                                        <label for="end_date">End Date</label>
                                        <input type="date" name="end_date" id="end_date" value="{{ $coupon->end_date }}" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="courses">Courses:</label>
                                        <select name="courses[]" id="courses" class="form-control" multiple>
                                            <?php foreach($courses as $course) {
                                                echo '<option value="'.$course->id.'"'.(in_array($course->id, $coupon->courses) ? ' selected' : '').'>'.$course->title.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="locations">Locations:</label>
                                        <select name="locations[]" id="locations" class="form-control" multiple>
                                            <?php foreach($locations as $location) {
                                                echo '<option value="'.$location->id.'"'.(in_array($location->id, $coupon->locations) ? ' selected' : '').'>'.$location->title.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });

            $('#date_type').on('change', function(e) {
                updateDates();
            });

            function updateDates() {
                if($('#date_type').val() == 0) {
                    $('.dates').hide();
                } else {
                    $('.dates').show();
                }
            }

            updateDates();
        });
    </script>
@endsection
