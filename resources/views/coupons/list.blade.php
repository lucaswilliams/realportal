@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Coupon List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/coupons/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add coupon</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Coupon Code</th>
                                <th>Uses</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($coupons as $coupon) {
                            ?>
                                <tr>
                                    <td>{{ $coupon->coupon_code }}</td>
                                    <td>{{ $coupon->uses == 0 ? 'Unlimited' : $coupon->uses }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/coupons/{{$coupon->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit coupon"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/coupons/{{$coupon->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete coupon"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
