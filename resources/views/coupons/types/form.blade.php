@extends('layouts.app')

@section('content')
    <form method="POST" action="/coupons/types">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="0">
                    <div class="card">
                        <div class="card-header">
                            Edit coupon
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Coupon Type Name:</label>
                                        <input type="text" name="name" id="name" class="form-control" data-validation="required" value="{{$coupon->coupon_code ?? ""}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });
        });
    </script>
@endsection
