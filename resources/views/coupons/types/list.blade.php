@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Coupon Type List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/coupons/types/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add coupon type</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Coupon Type</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($coupontypes as $coupon) {
                            ?>
                                <tr>
                                    <td>{{ $coupon->name }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/coupons/types/{{$coupon->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit coupon"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/coupons/types/{{$coupon->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete coupon"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
