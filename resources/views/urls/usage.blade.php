@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-12">
                                <h1>Booking URL Usage</h1>
                                <h2>{{ $url }}</h2>
                            </div>
                        </div>
                        <form method="GET">
                            <div class="row">
                                <div class="col-5">
                                    <label for="start-date">Start Date</label>
                                    <input type="date" id="start-date" name="start-date" value="{{ $start }}" class="form-control">
                                </div>
                                <div class="col-5">
                                    <label for="end-date">End Date</label>
                                    <input type="date" id="end-date" name="end-date" value="{{ $end }}" class="form-control">
                                </div>
                                <div class="col-2">
                                    <button type="submit" class="btn btn-primary" style="position: absolute; bottom: 0; right: 15px;">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <a href="#" onclick="doit('xlsx')" class="btn btn-success float-right">Export to Excel</a>
                        <table class="table table-striped" id="usage-table">
                            <tr>
                                <th>Course</th>
                                <th>Location</th>
                                <th>Contact</th>
                                <th>Email</th>
                                <th>Participant Name</th>
                                <th>Type</th>
                                <th>Booking Date</th>
                                <th>Course Date</th>
                                <th>Price</th>
                                <?php foreach($fields as $field) { ?>
                                    <th>{{ $field->field_title }}</th>
                                <?php } ?>
                            </tr>
                            <?php
                            //echo '<pre>'; var_dump($usage); echo '</pre>';
                                foreach($usage as $item) {
                                    $contentparts = explode(' ', $item->booking_content, 4);
                            ?>
                                <tr>
                                    <td>{{ $item->course_name }}</td>
                                    <td>{{ $item->location_name }}</td>
                                    <td>{{ $item->booking_contact }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->participant_name }}</td>
                                    <td>{{ $contentparts[2] }}</td>
                                    <td>{{ $item->booking_date }}</td>
                                    <td>{{ $item->course_date }}</td>
                                    <td class="text-right">${{ $item->price }}</td>
                                    <?php foreach($fields as $idx => $field) { ?>
                                        <td>
                                        <?php if(isset($item->custom_fields['a'.$field->id])) { ?>
                                            {{ $item->custom_fields['a'.$field->id] }}
                                        <?php } ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="//unpkg.com/xlsx/dist/shim.min.js"></script>
    <script type="text/javascript" src="//unpkg.com/xlsx/dist/xlsx.full.min.js"></script>

    <script type="text/javascript" src="//unpkg.com/blob.js@1.0.1/Blob.js"></script>
    <script type="text/javascript" src="//unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
    <script>
        function doit(type, fn, dl) {
            var elt = document.getElementById('usage-table');
            var wb = XLSX.utils.table_to_book(elt, {sheet:"Bookings"});
            return dl ?
                XLSX.write(wb, {bookType:type, bookSST:true, type: 'base64'}) :
                XLSX.writeFile(wb, fn || ('Booking Link Usage | <?php echo $url; ?>.' + (type || 'xlsx')));
        }
    </script>
@endsection
