@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Booking URL List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/urls/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add booking URL</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Booking URL</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($items as $item) {
                            ?>
                                <tr>
                                    <td>{{ $item->link_url }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/urls/{{$item->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit URL"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/urls/{{$item->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete URL"><i class="fa fa-fw fa-trash"></i></a>
                                        <a class="btn btn-warning" href="/urls/{{$item->id}}/list" data-toggle="tooltip" data-placement="bottom" title="Show bookings from this URL"><i class="fa fa-fw fa-file-text-o"></i></a>
                                        <a class="btn btn-warning" href="/urls/{{$item->id}}/fields" data-toggle="tooltip" data-placement="bottom" title="Custom Fields"><i class="fa fa-fw fa-list-ul"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
