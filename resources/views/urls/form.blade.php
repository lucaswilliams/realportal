@extends('layouts.app')

@section('content')
    <form method="POST" action="/urls">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$item->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit Option
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">URL suffix:</label>
                                <input type="text" name="link_url" id="link_url" class="form-control" data-validation="required" value="{{$item->link_url ?? ""}}">
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="equipment">People visiting this link have their own equipment:</label>
                                        <select name="equipment" id="equipment" class="form-control">
                                            <option value="0" {{$item->equipment == 0 ? 'selected' : ''}}>No</option>
                                            <option value="1" {{$item->equipment == 1 ? 'selected' : ''}}>Yes</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password: <small>(leave blank if visiting this Booking URL doesn't require a password)</small></label>
                                        <input type="text" name="password" id="password" value="{{$item->password}}" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="client_id">Client:</label>
                                        <select name="client_id" id="client_id" class="select2">
                                            <option value="0">Public</option>
                                            <?php
                                            foreach($clients as $client) {
                                            ?>
                                                <option value="{{$client->id}}"{{($client->id == $item->client_id ? 'selected' : '')}}>{{$client->name}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="invoices">Course Payment Options:</label>
                                        <select id="invoices" name="invoices" class="form-control">
                                            <option value="0" <?php echo ($item->invoices == 0 ? 'selected' : '') ?>>Credit Card Only</option>
                                            <option value="1" <?php echo ($item->invoices == 1 ? 'selected' : '') ?>>Invoice Only</option>
                                            <option value="2" <?php echo ($item->invoices == 2 ? 'selected' : '') ?>>Credit Card or Invoice</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="priority_courses">Priority Course Status:</label>
                                        <select id="priority_courses" name="priority_courses" class="form-control">
                                            <option value="0" <?php echo ($item->priority_courses == 0 ? 'selected' : '') ?>>Hidden</option>
                                            <option value="1" <?php echo ($item->priority_courses == 1 ? 'selected' : '') ?>>Shown</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="freetext">Text for landing page:</label>
                                        <textarea id="freetext" name="freetext" class="form-control">{{$item->freetext}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="booking_email">Send confirmation:</label>
                                        <select id="booking_email" name="booking_email" class="form-control">
                                            <option value="0" <?php echo ($item->booking_email == 0 ? 'selected' : '') ?>>No</option>
                                            <option value="1" <?php echo ($item->booking_email == 1 ? 'selected' : '') ?>>Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="form-group">
                                        <label for="confirmation_template">Confirmation Template:</label>
                                        <select id="confirmation_template" name="confirmation_template" class="form-control">
                                            <option value="0">(none)</option>
                                            <?php foreach($templates as $template) { ?>
                                                <option value="{{$template->id}}" <?php echo ($item->confirmation_template == $template->id ? 'selected' : '') ?>>{{$template->name}}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="form-group">
                                        <label for="confirmation_email">Send confirmation to:</label>
                                        <input class="form-control" type="text" id="confirmation_email" name="confirmation_email" value="{{$item->confirmation_email}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="real_response">Available for Real Response</label>
                                        <select id="real_response" name="real_response" class="form-control">
                                            <option value="0" <?php echo ($item->real_response == 0 ? 'selected' : '') ?>>No</option>
                                            <option value="1" <?php echo ($item->real_response == 1 ? 'selected' : '') ?>>Yes</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="cq_first_aid">Available for CQ First Aid</label>
                                        <select id="cq_first_aid" name="cq_first_aid" class="form-control">
                                            <option value="0" <?php echo ($item->cq_first_aid == 0 ? 'selected' : '') ?>>No</option>
                                            <option value="1" <?php echo ($item->cq_first_aid == 1 ? 'selected' : '') ?>>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="locations">Location(s):</label>
                                <select multiple class="select2" id="locations" name="locations[]">
                                    <?php $ls = explode(',', $item->locations);
                                    foreach($locations as $location) { ?>
                                        <option value="{{$location->ID}}" <?php echo in_array($location->ID, $ls) ? 'selected' : ''; ?>>{{$location->post_title}}</option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="slack_channel">Channel for Slack Messages:</label>
                                <input class="form-control" id="slack_channel" name="slack_channel" value="{{$item->slack_channel}}" placeholder="sales">
                            </div>

                            <div class="form-group">
                                <table class="table table-striped" id="courses" style="width: 100%">
                                    <tr>
                                        <th>Course</th>
                                        <th style="width: 130px">Course Type</th>
                                        <th>Template ID</th>
                                        <th>Coupon Code</th>
                                        <th>Price Override</th>
                                        <th><a href="#" class="btn btn-success" id="add-course"><i class="fa fa-plus"></i></a></th>
                                    </tr>
                                    <?php if(count($courses) > 0) {
                                        foreach($courses as $course) { ?>
                                        <tr>
                                            <td>
                                                <select name="course_id[]" class="form-control select2">
                                                    <?php if(is_array($wpcourses) && count($wpcourses) > 0) {
                                                        foreach($wpcourses as $wpcourse) { ?>
                                                    <option value="{{$wpcourse->ID}}" {{$course->course_id == $wpcourse->ID ? 'selected' : ''}}>{{$wpcourse->post_title}}{{$wpcourse->for_rr ? ' [RR]' : ''}}{{$wpcourse->for_cq ? ' [CQ]' : ''}}</option>
                                                    <?php }
                                                        } ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="course_type[]" class="form-control">
                                                    <option value="0" {{$course->course_type == 0 ? "selected" : ""}}>Public</option>
                                                    <option value="1" {{$course->course_type == 1 ? "selected" : ""}}>Private</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="template_id[]" value="{{$course->template_id}}" class="form-control"></td>
                                            <td><input type="text" name="coupon_code[]" value="{{$course->coupon_code}}" class="form-control"></td>
                                            <td><input type="text" name="price[]" value="{{$course->price}}" class="form-control"></td>
                                            <td><a href="#" class="btn btn-danger remove-equipment-course" data-id="{{$course->id}}"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    <?php }
                                        } ?>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-none" id="removed"></div>
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2({
                theme: 'bootstrap4'
            });

            $(document).on('click', '.remove-equipment-course', function(e) {
                e.preventDefault();

                var _id = $(this).data('id');
                if(parseInt(_id) > 0) {
                    $('#removed').append('<input type="text" name="removed_courses[]" value="' + _id + '">');
                }

                $(this).parents('tr').remove();
            });

            $(document).on('click', '#add-course', function(e) {
                e.preventDefault();

                $('#courses').append('<tr>\n' +
                    '<td><select name="course_id[]" class="form-control select2">\n' +
                    <?php if(is_array($wpcourses) && count($wpcourses) > 0) {
                        foreach($wpcourses as $wpcourse) { ?>
                    '  <option value="{{$wpcourse->ID}}">{{$wpcourse->post_title}}</option>\n' +
                    <?php }
                        } ?>
                    '</select></td>\n' +
                    '<td><select name="course_type[]" class="form-control">\n' +
                    '\t<option value="0">Public</option>\n' +
                    '\t<option value="1">Private</option>\n' +
                    '</select></td>' +
                    '<td><input type="text" name="template_id[]" class="form-control"></td>\n' +
                    '<td><input type="text" name="coupon_code[]" class="form-control"></td>\n' +
                    '<td><input type="text" name="price[]" class="form-control"></td>\n' +
                    '<td><a href="#" class="btn btn-danger remove-equipment-course" data-id="0"><i class="fa fa-trash"></i></a></td>\n' +
                    '</tr>')
                .find('.select').select2({
                    theme: 'bootstrap4'
                });
            })
        });
    </script>
@endsection
