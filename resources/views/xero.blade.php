@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Connect to Xero
                </div>
                <div class="card-body">
                    @if($error)
                        <div class="alert alert-danger">Your connection to Xero failed</div>
                        <p>{{ $error }}</p>
                        <a href="{{ route('xero.auth.authorize') }}" class="btn btn-primary btn-large mt-4">
                            Reconnect to Xero
                        </a>
                    @elseif($connected)
                        <div class="alert alert-success">You are connected to Xero</div>
                        <a href="{{ route('xero.auth.delete') }}" class="btn btn-primary btn-large mt-4">
                            Disconnect from Xero
                        </a>
                    @else
                        <div class="alert alert-warning">You are not connected to Xero</div>
                        <a href="{{ route('xero.auth.authorize') }}" class="btn btn-primary btn-large mt-4">
                            Connect to Xero
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
