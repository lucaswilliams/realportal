@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="POST">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">Change Email</div>
                        <div class="card-body">
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="email">New email:</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="email_verify">New email (again):</label>
                                <input type="email" class="form-control" id="email_verify" name="email_verify">
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
