@extends('layouts.app')

@section('content')
    <form method="POST" action="/menuitems">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$item->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit Menu Item
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="label">Label:</label>
                                        <input type="text" name="label" id="label" class="form-control" data-validation="required" value="{{$item->label ?? ""}}">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="route">Route:</label>
                                        <input type="text" name="route" id="route" value="{{$item->route}}" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="parent_id">Parent:</label>
                                        <select name="parent_id" class="form-control">
                                            <option value="0">(none)</option>
                                            <?php foreach($parents as $parent) { ?>
                                                <option value="{{$parent->id}}" {{$parent->id == $item->parent_id ? "selected" : ""}}>{{$parent->label}}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="menu_order">Menu Order:</label>
                                        <input type="num" name="menu_order" id="menu_order" value="{{$item->menu_order}}" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="role_id">Roles</label>
                                        <select class="form-control select2" id="role_id" name="role_id[]" multiple>
                                            <?php
                                                foreach($roles as $role) {
                                                    echo '<option value="'.$role->id.'" '.($role->menu_id == null ? "" : "selected").'>'.$role->name.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-none" id="removed"></div>
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2({
                theme: 'bootstrap4'
            });
        });
    </script>
@endsection