@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Menu Item List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/menuitems/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add menu item</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Menu Item</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($items as $item) {
                            ?>
                                <tr>
                                    <td><?php echo $item->label; ?></td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/menuitems/{{$item->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit menu item"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/menuitems/{{$item->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete menu item"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
