@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Corporate Enroller</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" id="uploadBulkForm">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="organisation">Organisation</label>
                            <select name="organisation" id="organisation" class="select2 form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="uploadfile">Excel File</label>
                            <input type="file" name="uploadfile" id="uploadfile">
                        </div>
                        <button class="btn btn-danger" id="uploadBulk">Upload</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#organisation').select2({
        minimumInputLength: 3,
        ajax: {
            url: function(params) {
                return '/axcelerate/organisations/';
            },
            dataType: 'json',
            delay: 500,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            id: item.id
                        }
                    })
                };
            }
        },
        theme: 'bootstrap4'
    });
</script>
@endsection
