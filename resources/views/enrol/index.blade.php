<?php
    $baseurl = 'https://www.realresponse.com.au';
    $bookingurl = $baseurl.'/booking/';
    if($_SERVER['HTTP_HOST'] == 'realportal.local') {
        $baseurl = 'https://realresponse.local';
        $bookingurl = $baseurl.'/booking/';
    }
    if($_SERVER['HTTP_HOST'] == 'devportal.realresponse.com.au') {
        $baseurl = 'https://dev.realresponse.com.au';
        $bookingurl = $baseurl.'/booking/';
    }
?>
@extends('layouts.app')

@section('styles')
    <!-- Gravity Forms -->
    <link rel='stylesheet' id='gforms_reset_css-css'  href='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/css/formreset.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_formsmain_css-css'  href='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/css/formsmain.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_ready_class_css-css'  href='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/css/readyclass.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_browsers_css-css'  href='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/css/browsers.min.css?ver=2.3.6' type='text/css' media='all' />

    <!-- Bootstrap CSS -->
    <style> .container { max-width: 100%; } </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $baseurl; ?>/booking/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo $baseurl; ?>/booking/css/style.css?ver=<?php echo time(); ?>">
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <div id="booking-form" class="text-center">
                    <i class="fa fa-spinner fa-pulse fa-4x fa-fw" style="margin-top: 128px"></i><br>Loading booking form
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        var _privkey = '<?php echo $stripe_pk; ?>';
        var _baseurl = '<?php echo $bookingurl; ?>';
        var _mybaseurl = '<?php echo $bookingurl; ?>';
        var _formType = 'Portal';
    </script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script type='text/javascript' src='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/js/jquery.json.min.js?ver=2.3.6'></script>
    <script type='text/javascript' src='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/js/gravityforms.min.js?ver=2.3.6'></script>
    <script type='text/javascript' src='<?php echo $baseurl; ?>/wp-content/plugins/gravityforms/js/placeholders.jquery.min.js?ver=2.3.6'></script>

    <script type="text/javascript" src="<?php echo $baseurl; ?>/booking/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseurl; ?>/booking/js/booking.js?ver=<?php echo time(); ?>"></script>

    <script>
        $(document).ready(function() {
            $('#booking-form').load(_baseurl + ' #current-page', function(response, status, xhr) {
                $('#current-page').removeClass('col-md-8');
                $('.booking-page-container:first-of-type').show();
                $('#booking-form').removeClass('text-center');
                attachStripe();
                setUpMCB();
                _baseurl = _mybaseurl;
            });
        })
    </script>
@endsection
