@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">Corporate Enrol Boxes</div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Batch ID</th>
                                <th>Enrolments</th>
                            </tr>
                            <?php foreach($batches as $batch) { ?>
                                <tr>
                                    <td><a href="/enrol/corporate/{{ $batch->batch_id }}">{{ $batch->batch_id }}</a></td>
                                    <td>{{$batch->enrolments}}</td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
