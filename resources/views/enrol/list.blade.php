@extends('layouts.app')

@section('content')
<form method="POST">
    {{ csrf_field() }}
    <input type="hidden" id="batch_id" value="{{ $batch }}">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">Corporate Enroller</div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Given Name</th>
                                <th>Middle Name</th>
                                <th>Surname</th>
                                <th>Date of Birth</th>
                                <th>Email</th>
                                <th>USI</th>
                                <th>Status</th>
                            </tr>
                        <?php foreach($contacts as $contact) { ?>
                            <?php if($contact->validated > 0) { ?>
                            <tr>
                                <td>{{ $contact->given_name }}</td>
                                <td>{{ $contact->middle_name }}</td>
                                <td>{{ $contact->surname }}</td>
                                <td>{{ $contact->date_of_birth }}</td>
                                <td>{{ $contact->email }}</td>
                                <td>{{ $contact->usi }}</td>
                                <td>{{$contact->message}}</td>
                            </tr>
                            <?php } else { ?>
                                <tr>
                                    <td><input type="text" class="form-control" name="given_name[{{ $contact->id }}]" value="{{ $contact->given_name }}"></td>
                                    <td><input type="text" class="form-control" name="middle_name[{{ $contact->id }}]" value="{{ $contact->middle_name }}"></td>
                                    <td><input type="text" class="form-control" name="surname[{{ $contact->id }}]" value="{{ $contact->surname }}"></td>
                                    <td><input type="text" class="form-control" name="date_of_birth[{{ $contact->id }}]" value="{{ $contact->date_of_birth }}"></td>
                                    <td><input type="text" class="form-control" name="email[{{ $contact->id }}]" value="{{ $contact->email }}"></td>
                                    <td><input type="text" class="form-control" name="usi[{{ $contact->id }}]" value="{{ $contact->usi }}"></td>
                                    <td>{{$contact->message}}</td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">Add to course</div>

                    <div class="card-body">
                        <?php
                        if($validated != count($contacts)) { ?>
                            <p>Not all entries have been validated against data in aXcelerate.  You will need to fix these validation errors before you can enrol everyone in a course.</p>
                            <p>
                                <button type="submit" class="btn btn-success">Update</button>
                                <?php if($validated > 0) { echo '<button type="button" id="add-to-course-btn" class="btn btn-warning">Send validated entries</button>'; } ?>
                            </p>
                        <?php } else { ?>
                            <div id="add-to-course">
                                <div class="form-group">
                                    <label for="course_id">aXcelerate Course Instance ID</label>
                                    <input type="text" class="form-control" id="course_id">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="send_course_enrol">Check</button>
                                </div>
                                <div id="course_details"></div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="confirm_course_enrol" disabled>Enrol</button>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
