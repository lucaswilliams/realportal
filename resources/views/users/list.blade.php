@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>User List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/users/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add user</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Phone</th>
                                <th>Level</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($users as $user) {
                            ?>
                                <tr>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->mobile }}</td>
                                    <td>{{ $user->work_phone }}</td>
                                    <td>{{ $user->level }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/users/{{$user->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit user"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-primary" href="/users/{{$user->id}}/password" data-toggle="tooltip" data-placement="bottom" title="Edit password"><i class="fa fa-fw fa-key"></i></a>
                                        <?php if($admin) { ?>
                                        <a class="btn btn-danger delete" href="/users/{{$user->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete user"><i class="fa fa-fw fa-trash"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
