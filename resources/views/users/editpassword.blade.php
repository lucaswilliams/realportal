@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-12">
                                <h1>New password</h1>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            {{ csrf_field() }}
                            <p>You are changing the password for {{ $user->first_name }} {{ $user->last_name }}.</p>
                            <div class="form-group">
                                <label for="password">New password</label>
                                <input type="password" id="password" name="password" class="form-control">
                            </div>
                            <button class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
