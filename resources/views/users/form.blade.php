@extends('layouts.app')

@section('content')
    <form method="POST" action="/users">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$user->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            {{($user->id ?? 0 == 0 ? 'Add' : 'Edit')}} user
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name:</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control" data-validation="required" value="{{$user->first_name ?? ""}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name:</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control" data-validation="required" value="{{$user->last_name ?? ""}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" name="email" id="email" class="form-control" data-validation="required" value="{{$user->email ?? ""}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="mobile">Mobile:</label>
                                        <input type="text" name="mobile" id="mobile" class="form-control" value="{{$user->mobile ?? ""}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label for="work_phone">Work Phone:</label>
                                        <input type="text" name="work_phone" id="work_phone" class="form-control" value="{{$user->work_phone ?? ""}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="level">User Level</label>
                                        <select class="form-control" id="level" name="level">
                                            <?php foreach($levels as $level) {
                                                echo '<option value="'.$level->id.'"'.($level->id == ($user->level ?? 0) ? ' selected' : '').'>'.$level->name.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });
        });
    </script>
@endsection
