@extends('layouts.app')

@section('content')
    <form method="POST" action="/emailtemplatesusage">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Edit Email Template Usage
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p>All students will be sent a confirmation email from aXcelerate; these emails are in addition to any of those.  "Participant" emails will not be sent if the contact is the only student on the booking.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Template</th>
                                                <th>One Participant</th>
                                                <th>Many Participants</th>
                                                <th>Booking URL</th>
                                                <th>Invoice Email</th>
                                                <th>CQ Invoice Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($templates as $template) { ?>
                                                <tr>
                                                    <td>{{$template->name}}</td>
                                                    <td><input type="checkbox" name="single_participant[{{$template->id}}]" value="1" <?php echo ($template->single_participant == 1 ? 'checked' : ''); ?> class="single_participant"></td>
                                                    <td><input type="checkbox" name="multi_participant[{{$template->id}}]" value="1" <?php echo ($template->multi_participant == 1 ? 'checked' : ''); ?> class="multi_participant"></td>
                                                    <td><input type="checkbox" name="url_confirmation[{{$template->id}}]" value="1" <?php echo ($template->url_confirmation == 1 ? 'checked' : ''); ?>></td>
                                                    <td><input type="checkbox" name="invoice_email[{{$template->id}}]" value="1" <?php echo ($template->invoice_email == 1 ? 'checked' : ''); ?>></td>
                                                    <td><input type="checkbox" name="cq_invoice_email[{{$template->id}}]" value="1" <?php echo ($template->cq_invoice_email == 1 ? 'checked' : ''); ?>></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.single_participant').click(function(e) {
            if($(this).prop('checked') === true) {
                $('.single_participant').prop('checked', false);
                $(this).prop('checked', true);
            }
        });

        $('.multi_participant').click(function(e) {
            if($(this).prop('checked') === true) {
                $('.multi_participant').prop('checked', false);
                $(this).prop('checked', true);
            }
        });
    });
</script>
@endsection