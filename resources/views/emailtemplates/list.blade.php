@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Email Template List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/emailtemplates/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add email template</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Template Name</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($items as $item) {
                            ?>
                                <tr>
                                    <td><?php echo $item->name; ?></td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/emailtemplates/{{$item->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit menu item"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/emailtemplates/{{$item->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete menu item"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
