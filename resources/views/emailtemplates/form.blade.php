@extends('layouts.app')

@section('content')
    <form method="POST" action="/emailtemplates">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$item->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit Email Template
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" name="name" id="name" class="form-control" data-validation="required" value="{{$item->name ?? ""}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-10">
                                    <div class="form-group">
                                        <label for="content">Content:</label>
                                        <textarea class="form-control description" id="content" name="content">{{$item->content}}</textarea>
                                    </div>
                                </div>

                                <div class="col-12 col-md-2 mergefields">
                                    <p>You can use the following merge fields</p>
                                    <ul>
                                        <li><strong>[[first_name]]</strong> The booking contact's first name</li>
                                        <li><strong>[[last_name]]</strong> The booking contact's last name</li>
                                        <li><strong>[[email]]</strong> The booking contact's email</li>
                                        <li><strong>[[phone]]</strong> The booking contact's phone number</li>
                                        <li><strong>[[course_name]]</strong> The name of the selected course</li>
                                        <li><strong>[[location]]</strong> The selected location for the course</li>
                                        <li><strong>[[date]]</strong> The date of the selected course</li>
                                        <li><strong>[[student_list]]</strong> The list of student names, phone numbers and email addresses.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="https://cdn.tiny.cloud/1/85bdttmuxhnpzji9j1w2d9ya3e07hfklf55chb4l5m4vndwi/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector:'textarea.description',
            width: '100%',
            height: 300,
            plugins: 'code',
            toolbar: 'undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
            menubar: 'tools'
        });
    </script>
@endsection