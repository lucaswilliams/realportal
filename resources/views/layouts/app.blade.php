<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="noindex,nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2-bootstrap4.min.css') }}" rel="stylesheet" />

    @yield('styles')
</head>
<body class="{{ \Request::route()->getName() }}">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/realresponse.png') }}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                            <?php
                            $user_id = Auth::user()->id;

                            $menuQ = DB::table('menu_items')
                                ->join('menu_item_roles', 'menu_item_roles.menu_id', '=', 'menu_items.id')
                                ->join('user_roles', 'user_roles.role_id', '=', 'menu_item_roles.role_id')
                                ->whereNull('parent_id')
                                ->where('user_roles.user_id', $user_id)
                                ->orderBy('menu_order')
                                ->orderBy('id')
                                ->select('menu_items.*');
                            $menus = $menuQ->get();

                            foreach($menus as $menu) {
                                //get the children for this menu
                                $children = DB::table('menu_items')
                                    ->join('menu_item_roles', 'menu_item_roles.menu_id', '=', 'menu_items.id')
                                    ->join('user_roles', 'user_roles.role_id', '=', 'menu_item_roles.role_id')
                                    ->where('parent_id', '=', $menu->id)
                                    ->where('user_roles.user_id', $user_id)
                                    ->orderBy('menu_order')
                                    ->orderBy('id')
                                    ->select('menu_items.*')
                                    ->get();

                                if(count($children) > 0) {
                                    echo '<li class="nav-item dropdown">
								<a id="accountDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									'.$menu->label.' <span class="caret"></span>
								</a>

								<div class="dropdown-menu" aria-labelledby="'.strtolower(str_replace(' ', '', $menu->label)).'Dropdown">';
                                    foreach($children as $child) {
                                        echo '<a class="dropdown-item" href="'.$child->route.'">
                                            '.$child->label.'
                                        </a>';
                                    }

                                    echo '</div>
							        </li>';
                                }
                                else {
                                    echo '<li class="nav-item">
                                            <a class="nav-link" href="'.$menu->route.'">'.$menu->label.'</a>
                                        </li>';
                                }
                            }
                            ?>
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <!--<li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>-->
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->first_name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('/account') }}">My Account</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php
            $success = session('success');
            if($success) {
                echo '<div class="container"><div class="row"><div class="col"><div class="alert alert-success">';
                echo $success;
                echo '</div></div></div></div>';
            }

            $error = session('error');
            if($error) {
                echo '<div class="container"><div class="row"><div class="col"><div class="alert alert-danger">';
                echo $success;
                echo '</div></div></div></div>';
            }
            ?>
            @yield('content')
        </main>
    </div>
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    @yield('scripts')
</body>
</html>
