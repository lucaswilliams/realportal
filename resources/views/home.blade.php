@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <p>You are logged in!</p>
                    <p>Some task lists and such will live here eventually</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
