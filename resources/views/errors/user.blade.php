@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">Error</div>

                    <div class="card-body">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
