@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Log Contents</h1>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
