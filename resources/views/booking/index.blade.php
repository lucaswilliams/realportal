<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="google-site-verification" content="gUXCJmVa2EAIIX4PvkEjAt1kSSHQ2Q2xT0zditzkXjk" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Booking - Real Response</title>

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo env('APP_URL'); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo env('APP_URL'); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo env('APP_URL'); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo env('APP_URL'); ?>/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo env('APP_URL'); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Gravity Forms -->
    <link rel='stylesheet' id='gforms_reset_css-css'  href='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/css/formreset.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_formsmain_css-css'  href='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/css/formsmain.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_ready_class_css-css'  href='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/css/readyclass.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_browsers_css-css'  href='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/css/browsers.min.css?ver=2.3.6' type='text/css' media='all' />

    <!-- Bootstrap CSS -->
    <style> .container { max-width: 100%; } </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo env('APP_URL'); ?>/booking/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo env('APP_URL'); ?>/booking/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo env('APP_URL'); ?>/booking/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo env('APP_URL'); ?>/booking/css/style.css?ver=<?php echo time(); ?>">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
        var _vwo_code=(function(){
            var account_id=333205,
                settings_tolerance=2000,
                library_tolerance=2500,
                use_existing_jquery=false,
                f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->

    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">//<![CDATA[
        var gtm4wp_datalayer_name = "dataLayer";
        var dataLayer = dataLayer || [];
        //]]>
    </script>
    <!-- End Google Tag Manager for WordPress by gtm4wp.com -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-596GZC');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<input type="hidden" id="geelong" value="<?php echo (int)$geelong; ?>">
<input type="hidden" id="colac" value="<?php echo (int)$colac; ?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="row">
            <div class="col-3 d-md-none align-items-center" style="display: flex;">

            </div>
            <div class="col-6 offset-md-3">
                <a href="https://realresponse.com.au" id="home-link">
                    <?php
                    if($geelong || $colac) {
                        echo '<img src="img/geelongfirstaid.png" id="gfa-logo"><span id="partof">is now part of</span><img src="img/realresponse.png" id="rr-logo">';
                    } else {
                        echo '<img src="img/realresponse.png" id="rr-logo" class="rr-only">';
                    }
                    ?>
                </a>
            </div>
            <!--<div class="d-none d-lg-flex col-lg-4 align-items-center">
                <p id="questions-text">Got questions? <strong>1300 744 980</strong></p>
            </div>-->
            <div class="col-3 col-md-2 text-right align-items-center" style="display: flex;">
                <a id="help-button"><span class="circle">?</span>Help</a>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid" id="progressBar">
    <div class="progress-bar"><div class="color" id="progress-color"></div></div>
</div>

<div class="container">
    <div class="row h100 justify-content-center">
        <div class="col-12 col-md-8" id="current-page">
            <input type="hidden" id="contactEmail">
            <input type="hidden" id="courseAmt" value="<?php echo $iamt; ?>">
            <input type="hidden" id="courseAmtEach" value="<?php echo $iamt; ?>">
            <input type="hidden" id="courseInstance" value="<?php echo $iid; ?>">
            <input type="hidden" id="courseDate" value="<?php echo ($idate != '' ? date('j F Y', strtotime($idate)) : ''); ?>">
            <input type="hidden" id="selected-date">
            <input type="hidden" id="homeURL" value="<?php echo env('APP_URL'); ?>/booking">
            <input type="hidden" id="thisURL" value="<?php echo env('APP_URL').$_SERVER['REQUEST_URI']; ?>">
            <input type="hidden" id="refererURL" value="<?php echo $referer; ?>">

            <input type="hidden" id="booking-clientid">
            <input type="hidden" id="booking-gclid">
            <input type="hidden" id="booking-trafficsource">

            <input type="hidden" id="iid" value="<?php echo $iid; ?>">

            <input type="hidden" id="clicks">
            <?php
            @include('booking.pages.state.php');
            @include('booking.pages.region.php');
            @include('booking.pages.type.php');
            @include('booking.pages.course.php');

            @include('booking.pages.corporate-participants.php');
            @include('booking.pages.corporate-calendar.php');
            @include('booking.pages.corporate-contact.php');
            @include('booking.pages.corporate-address.php');

            @include('booking.pages.instance.php');
            @include('booking.pages.public-contact.php');
            @include('booking.pages.public-participants.php');
            @include('booking.pages.public-match.php');

            @include('booking.pages.payment.php');
            @include('booking.pages.error.php');
            ?>
        </div>
        <div class="d-none d-md-block col-md-4" id="summary-panel">
            <p class="header">Booking Details</p>
            <div id="booking-summary">
                <p id="location-text"></p>
            </div>
            <p class="header" id="summary-total">Total <span class="total-amount"></span></p>
        </div>
    </div>
</div>

<div id="help-panel">
    <div class="container">
        <div class="row" id="help-header">
            <div class="col-12 col-lg-4 offset-lg-8 text-right align-items-center">
                <div class="content">
                    <a id="help-close"><span class="circle">X</span>Close</a>
                </div>
            </div>
        </div>
        <div class="row" id="help-content">
            <div class="col-12 col-lg-4 offset-lg-8">
                <div class="content">
                    <p class="header">Frequently Asked Questions</p>
                    <div id="help-scroll">
                        <?php
                        /*$faqs = get_posts([
                            'post_type' => 'faq',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'faq_category',
                                    'field' => 'slug',
                                    'terms' => 'booking-faq',
                                    'operator' => 'IN'
                                )
                            ),
                            'numberposts' => -1
                        ]);*/
                        $faqs = [];
                        $i = 0;
                        echo '<div class="accordion" id="faqAccordion">';
                        foreach($faqs as $faq) { $i++;
                        ?>

                        <div class="card">
                            <div class="card-header" id="heading<?php echo $i; ?>">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                                        <i class="fa fa-caret-down"></i>
                                        <?php echo $faq->post_title; ?>
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading" data-parent="#faqAccordion">
                                <div class="card-body">
                                    <?php echo wpautop($faq->post_content); ?>
                                </div>
                            </div>
                        </div>

                        <?php
                        }
                        echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script>
    <?php
    $baseurl = 'https://www.realresponse.com.au';
    if($_SERVER['HTTP_HOST'] == 'realportal.local' || $_SERVER['HTTP_HOST'] == 'realresponse.local') {
        $baseurl = 'https://realportal.local';
    }
    if($_SERVER['HTTP_HOST'] == 'dev.realresponse.com.au') {
        $baseurl = 'https://dev.realresponse.com.au';
    }
    ?>
    var _baseurl = '<?php echo $baseurl; ?>/booking/';
    var _privkey = '<?php echo env('STRIPE_PK'); ?>';
    var _formType = 'Website';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script type='text/javascript' src='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/js/jquery.json.min.js?ver=2.3.6'></script>
<script type='text/javascript' src='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/js/gravityforms.min.js?ver=2.3.6'></script>
<script type='text/javascript' src='<?php echo env('APP_URL'); ?>/wp-content/plugins/gravityforms/js/placeholders.jquery.min.js?ver=2.3.6'></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="<?php echo env('APP_URL'); ?>/booking/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo env('APP_URL'); ?>/booking/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo env('APP_URL'); ?>/booking/js/booking.js?ver=<?php echo time(); ?>"></script>
</body>
</html>
