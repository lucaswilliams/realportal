<div id="page-error" class="booking-page-container">
    <div id="page-error-1" class="booking-page">
        <h1 id="unavailable-location"><?php the_field('unavailable_page_title', $page->ID); ?><br><span class="red"><?php the_field('unavailable_page_subtitle', $page->ID); ?></span></h1>
        <h1 id="unavailable-course"><?php the_field('unavailable_course_title', $page->ID); ?><br><span class="red"><?php the_field('unavailable_page_subtitle', $page->ID); ?></span></h1>
        <div class="words">
            <?php echo wpautop(get_field('unavailable_page_text', $page->ID)); ?>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="error-block" id="error-contact">
                    <div class="content">
                        <div class="inner">
                            <img src="https://realresponse.com.au/booking/img/form.png">
                            <p>Enquiry</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="error-block" id="error-phone">
                    <div class="content">
                        <div class="inner">
                            <img src="https://realresponse.com.au/booking/img/phone.png">
                            <p>Call back</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page-error-contact" class="booking-page" style="display: none">
        <h1>Enquire Now</h1>
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
            hbspt.forms.create({
                region: "na1",
            portalId: "20865841",
            formId: "5c83e7b9-4634-4327-9936-f56d2d986e18"
            });
        </script>
    </div>

    <div id="page-error-phone" class="booking-page" style="display: none">
        <h1>Enter your number and we can call you back</h1>

        <?php
        echo do_shortcode('[gravityform id="6" title="false" description="false" ajax="true"]');
        ?>
    </div>
</div>