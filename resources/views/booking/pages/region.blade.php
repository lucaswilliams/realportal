<div id="page-location" class="booking-page-container <?php echo ($lid !== null ? 'prefilled' : ''); echo (($lid !== null || $iid > 0) ? ' nevershow' : ''); ?>">
    <div class="booking-page">
        <h1><?php the_field('region_page_title', $page->ID); ?></h1>

        <div class="row">
            <?php $regions = get_posts( array(
                'post_type' => 'rfa_locations',
                'hide_empty' => false,
                'numberposts' => -1
            ) );

            $states = get_terms(array(
                'taxonomy' => 'rfa_states',
                'parent' => 0,
                'hide_empty' => false,
            ));
            //echo '<pre>'; var_dump($states); echo '</pre>';
            foreach($regions as $region)
            {
                //echo '<pre>'; var_dump($region); echo '</pre>';
                $catID = get_the_terms($region->ID, 'rfa_states');
                $slug = '';
                if($catID) {
                    $slug = strtoupper($catID[0]->slug);
                }

                $deliveryfee = (float)get_field('online_equipment_delivery_fee', $page->ID);
                $deliverydays = 0;

                //find the state
                foreach($states as $state) {
                    if(strcasecmp($state->slug, $slug) == 0) {
                        //echo 'rfa_states_'.$state->term_id.'<br>';
                        $newfee = get_field('delivery_fees', 'rfa_states_'.$state->term_id);
                        //var_dump($newfee); echo '<br>';
                        if($newfee != null) {
                            $deliveryfee = $newfee;
                        }

                        $newdays = get_field('delivery_days', 'rfa_states_'.$state->term_id);
                        //var_dump($newdays); echo '<br>';
                        if($newdays != null) {
                            $deliverydays = $newdays;
                        }
                    }
                }

                //echo '<br>';

                $equippick = get_field('available_for_pickup', $region->ID);
                $equipface = get_field('available_for_face-to-face', $region->ID);
                $newfee = get_field('delivery_fee', $region->ID);
                if($newfee != null) {
                    $deliveryfee = $newfee;
                }

                $newdays = get_field('delivery_lockout', $region->ID);
                if($newdays != null) {
                    $deliverydays = $newdays;
                }

                if($lid == "VIC") { $lid = 5173; }
                if($lid == "NSW") { $lid = 5174; }
                if($lid == "WA") { $lid = 12658; }
            ?>
                    <div class="col-12">
                        <div class="training-location <?php echo ($lid == $region->ID ? 'active' : ''); ?>" data-id="<?php echo $region->ID; ?>" data-state="<?php echo $slug; ?>" data-public="<?php the_field('available_for_public_courses', $region->ID); ?>" data-equippick="<?php echo $equippick; ?>" data-equipface="<?php echo $equipface; ?>" data-deliverfees="<?php echo $deliveryfee; ?>" data-deliverdays="<?php echo $deliverydays; ?>">
                            <div class="content">
                                <div class="inner">
                                    <?php
                                    $img = '';
                                    $image = get_field('image', 'rfa_locations_'.$region->ID);
                                    if($image) {
                                        $img = $image['url'];
                                    }
                                    ?>
                                    <!--<img src="<?php echo $img; ?>">-->
                                    <p><strong><?php echo $region->post_title; ?></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
            }

            foreach($states as $state) {
                $deliveryfee = get_field('delivery_fees', 'rfa_states_'.$state->term_id);
                $deliverydays = get_field('delivery_days', 'rfa_states_'.$state->term_id);
            ?>

            <div class="col-12">
                <div class="training-location" data-state="<?php echo strtoupper($state->slug); ?>" data-deliverfees="<?php echo $deliveryfee; ?>" data-deliverdays="<?php echo $deliverydays; ?>">
                    <div class="content">
                        <div class="inner">
                            <p><strong>Other</strong></p>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            }
            ?>
        </div>

        <div class="row">
            <div class="col">
                <?php the_field('location_page_text', $page->ID); ?>
            </div>
        </div>
    </div>
</div>