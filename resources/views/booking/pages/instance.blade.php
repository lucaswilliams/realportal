<div id="page-public-instance" class="booking-page-container <?php echo ($iid > 0 ? ($cid > 0 && $lid > 0 && $tid > 0 ? 'prefilled' : 'prefilled') : ''); echo ($iid > 0 ? ' nevershow' : ''); ?>">
    <div id="page-equipment" class="booking-page">
        <h1>Equipment Options</h1>
        <?php the_field('equipment_description', $page->ID); ?>
        <p>Delivered equipment incurs an additional fee of $<strong class="delivery-fee"></strong> per person.</p>
        <div class="alert alert-danger" id="equipment-alert">You need to select a location for delivery costs. <a id="equipment-location" href="#">Please click here to update your location</a>.</div>
        <input type="hidden" id="deliveryFee" class="delivery-fee" value="0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg online-delivery" data-id="0">
                    <h3>Pick-up</h3>
                    <p><?php the_field('pickup_words', $page->ID); ?></p>
                </div>
                <div class="col-12 col-lg online-delivery" data-id="1">
                    <h3>Delivered</h3>
                    <p>(+$<span class="delivery-fee"></span> per person)</p>
                </div>
                <div class="col-12 col-lg online-delivery" data-id="2">
                    <h3>Face-to-Face</h3>
                    <p><?php the_field('face_to_face_words', $page->ID); ?></p>
                </div>
            </div>
            <div class="row" style="margin-right: -30px; margin-left: -30px; width: calc(100% + 60px);">
                <div class="col-12">
                    <div class="alert alert-danger" id="error-online-type">You must select how you'd like to use equipment for this course.</div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="online-delivery-button">Next</button>
        </div>
    </div>

    <div id="page-public-instance-1" class="booking-page">
        <h1><?php the_field('instance_page_title', $page->ID); ?></h1>
        <?php the_field('instance_page_text', $page->ID); ?>

        <div class="row">
            <div class="col">
                <div class="alert alert-danger" id="unavailable-instance" style="display:none;">The instance you are attempting to book into is no longer available, please select another course instance.</div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="row">
                    <?php
                        include '../config.php';
                        $headers = array(
                            'WSToken: ' . $ws_token,
                            'APIToken: ' . $api_token
                        );
                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, $ax_url . 'venues/?displayLength=50');
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        if ($proxy) {
                            curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                        }

                        $curl_response = curl_exec($curl);
                        curl_close($curl);
                        $venues = json_decode($curl_response);
                        $venuedata = [];
                        foreach($venues as $venue) {
                            $city = ucwords(strtolower($venue->SCITY));
                            if(!isset($venuedata[$city])) {
                                $venuedata[$city] = ['ids' => [], 'state' => $venue->SSTATE];
                            }

                            $venuedata[$city]['ids'][] = $venue->CONTACTID;
                        }
                        ksort($venuedata);
                        //echo '<pre>'; var_dump($venuedata); echo '</pre>';
                    ?>
                    <div class="col-12 col-md-4">
                        <label for="booking_delivery">Mode of Delivery</label>
                        <select id="booking_delivery" class="form-control">
                            <option value="1">Public</option>
                            <option value="2">Online</option>
                        </select>
                    </div>

                    <div class="col-12 col-md-4">
                        <label for="booking_location">Location</label>
                        <select id="booking_location" class="form-control">
                        <?php
                            foreach($venuedata as $location => $data) {
                                echo '<option value="'.implode(',', $data['ids']).'" data-state="'.$data['state'].'">'.$location.'</option>';
                            }
                        ?>
                            <option value="0" data-state="Online">Online</option>
                        </select>
                    </div>

                    <div class="col-12 col-md-4">
                        <label for="booking_equipment">Equipment</label>
                        <select id="booking_equipment" class="form-control">

                        </select>
                    </div>
                </div>
            </div>
            <!--<div class="col-2">
                <a href="#" id="filterAxcel" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
            </div>-->
        </div>

        <div id="axcel-courses"></div>
    </div>

    <div id="page-equipment-details" class="booking-page" style="display: none;">
        <div id="online-delivery-pickup">
            <p>Please select one of our locations for pickup: </p>
            <div class="container">
                <div class="row">
                    <?php $regions = get_posts( array(
                        'post_type' => 'rfa_locations',
                        'hide_empty' => false,
                        'numberposts' => -1
                    ) );
                    foreach($regions as $region) {
                        $pickup = get_field('available_for_pickup', $region->ID);
                        if($pickup) {
                        ?>

                        <div class="col pickup-location" data-id="<?php echo $region->post_title; ?>" data-location="<?php echo $region->ID; ?>">
                            <div class="row">
                                <div class="col">
                                    <p><strong><?php echo $region->post_title; ?></strong><br></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-4">
                                    <p><?php the_field('pickup_address', $region->ID); ?></p>
                                </div>
                                <div class="col-12 col-lg-8 text-lg-right">
                                    <p><?php the_field('pickup_and_delivery_times', $region->ID); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                    }
                    ?>
                </div>
                <div class="row" style="margin-right: -30px; margin-left: -30px; width: calc(100% + 60px);">
                    <div class="col-12">
                        <div class="alert alert-danger" id="error-online-pickup">You must select a pickup location.</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="online-delivery-delivered">
            <div class="form-group">
                <label for="online-address-street">Building, No & Street Name</label>
                <input type="text" id="online-address-street" class="form-control">
                <div class="alert alert-danger" id="error-online-street">Please enter your street address</div>
            </div>

            <div class="form-group">
                <label for="online-address-city">City, Suburb or Town</label>
                <input type="text" id="online-address-city" class="form-control">
                <div class="alert alert-danger" id="error-online-city">Please enter your city</div>
            </div>

            <div class="form-group">
                <label for="online-address-state">State / Territory</label>
                <input type="text" id="online-address-state" class="form-control">
                <div class="alert alert-danger" id="error-online-state">Please enter your state</div>
            </div>

            <div class="form-group">
                <label for="online-address-postcode">Postcode</label>
                <input type="email" id="online-address-postcode" class="form-control">
                <div class="alert alert-danger" id="error-online-postcode">Please enter your postcode</div>
            </div>

            <div class="form-group">
                <label for="online-address-comments">Delivery instructions</label>
                <textarea id="address-online-comments" class="form-control"></textarea>
            </div>
        </div>
        <div id="online-delivery-face">
            <p>Once you have completed your online learning component, we will be in contact to arrange your face-to-face assessment.</p>
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="online-delivery-option-button">Next</button>
        </div>
    </div>
</div>