<div id="page-corporate-address" class="booking-page-container">
    <div id="page-corporate-address-1" class="booking-page">
        <h1><?php the_field('address_page_title', $page->ID); ?></h1>
        <div class="form-group">
            <label for="address-street">Building, No & Street Name</label>
            <input type="text" id="address-street" class="form-control">
            <div class="alert alert-danger" id="error-street">Please enter your street address</div>
        </div>

        <div class="form-group">
            <label for="address-city">City, Suburb or Town</label>
            <input type="text" id="address-city" class="form-control">
            <div class="alert alert-danger" id="error-city">Please enter your city</div>
        </div>

        <div class="form-group">
            <label for="address-state">State / Territory</label>
            <input type="text" id="address-state" class="form-control">
            <div class="alert alert-danger" id="error-state">Please enter your state</div>
        </div>

        <div class="form-group">
            <label for="address-postcode">Postcode</label>
            <input type="email" id="address-postcode" class="form-control">
            <div class="alert alert-danger" id="error-postcode">Please enter your postcode</div>
        </div>

        <div class="form-group">
            <label for="address-comments">Comments / Questions or other information</label>
            <textarea id="address-comments" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="address-button">Next</button>
        </div>
    </div>
</div>