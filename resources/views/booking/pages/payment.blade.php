<div id="page-payment" class="booking-page-container">
    <div id="page-payment-1" class="booking-page">
        <h1><?php the_field('payments_page_title', $page->ID); ?></h1>
        <p id="public-total"><strong>Total <span class="total-amount"></span></strong></p>
        <p id="portal-total"><label>Total:</label><input type="text" id="total-amount" class="form-control"/></p>
        <p><?php the_field('payments_page_text', $page->ID); ?></p>

        <div class="form-group" id="grp-coupon">
            <p>Do you have an access code? <a href="#" id="show-coupon">Click here to enter it</a></p>
            <div class="alert alert-warning" id="coupon-message">Details about your booking have changed, so you'll need to enter your access code again.</div>
            <div id="grp-coupon-inner">
                <label for="coupon-code">Access Code</label>
                <div class="row">
                    <div class="col-9">
                        <input type="text" class="form-control" id="coupon-code">
                        <input type="hidden" id="coupon-code-valid">
                    </div>
                    <div class="col-3">
                        <a href="#" id="apply-coupon" class="btn btn-success">Apply</a>
                    </div>
                    <div class="col-12">
                        <div class="alert alert-danger" id="coupon-errors">The access code you have entered is invalid.</div>
                        <div class="alert alert-success" id="coupon-success">Success! Your access code has been applied.</div>
                    </div>
                </div>
            </div>
            <p>An invoice will be sent to the person listed above in "Contact Details." <a href="#" id="show-invoice-details">Click here to change details</a></p>
            <div id="div-invoice-details">
                <h3>Invoicee details</h3>
                <p>Please confirm the details of the person to receive the invoice</p>
                <div class="form-group">
                    <label for="invoice-contact-first">First Name</label>
                    <input type="text" id="invoice-contact-first" name="invoice-contact-first" class="form-control">
                </div>
                <div class="form-group">
                    <label for="invoice-contact-last">Last Name</label>
                    <input type="text" id="invoice-contact-last" name="invoice-contact-last" class="form-control">
                </div>
                <div class="form-group">
                    <label for="invoice-contact-email">Email</label>
                    <input type="text" id="invoice-contact-email" name="invoice-contact-email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="invoice-contact-organisation">Organisation</label>
                    <input type="text" id="invoice-contact-organisation" name="invoice-contact-organisation" class="form-control">
                </div>
            </div>
        </div>

        <a href="#" id="btn-credit" class="btn btn-default">Pay Now</a>
        <a href="#" id="btn-invoice" class="btn btn-default">Invoice</a>
        <div id="div-credit">
            <form id="payment-form" method="POST" action="/charge">
                <p id="accept"><small>We accept</small><br /><img src="https://realresponse.com.au/booking/img/cards.png"></p>
                <input type="hidden" name="stripeToken" id="stripeToken">
                <div class="form-group">
                    <label for="card-element">
                        Card Number
                    </label>
                    <div id="card-element">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-6 col-md-3">
                        <label for="card-expiry">
                            Expiry Date
                        </label>
                        <div id="card-expiry">
                            <!-- A Stripe Element will be inserted here. -->
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <label for="card-cvc">
                            CVC
                        </label>
                        <div id="card-cvc">
                            <!-- A Stripe Element will be inserted here. -->
                        </div>
                    </div>
                </div>
                <!-- Used to display Element errors. -->
                <div id="card-errors" role="alert" class="alert alert-danger"></div>
            </form>

            <div class="form-group" id="booking-confirmation">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="confirmation-check">
                    <label class="form-check-label" for="confirmation-check"><?php the_field('confirmation_text', $page->ID); ?></label>
                </div>
            </div>

            <div id="online-tc">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="online-tc-check">
                    <label class="form-check-label" for="online-tc-check"><?php the_field('terms_and_conditions_text', $page->ID); ?></label>
                </div>
                <div class="alert alert-danger" id="online-tc-alert">You must accept the Terms and Conditions to proceed.</div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" id="payment-button">Pay and confirm</button>
            </div>
        </div>
        <div id="div-invoice">
            <?php the_field('invoice_text', $page->ID); ?>
            <button class="btn btn-primary" id="invoice-button">Confirm</button>
        </div>

        <div id="participant-check">

        </div>
    </div>
</div>