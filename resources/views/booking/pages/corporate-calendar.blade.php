<div id="page-corporate-calendar" class="booking-page-container">
    <div id="page-corporate-calendar-1" class="booking-page">
        <h1><?php the_field('date_page_title', $page->ID); ?></h1>
        <div id="calendar">
            <div id="calendar-controls">
                <div id="calendar-prev" class="calendar-control"><i class="fa fa-chevron-circle-left"></i></div>
                <div id="calendar-month">FEBRUARY</div>
                <div id="calendar-next" class="calendar-control"><i class="fa fa-chevron-circle-right"></i></div>
            </div>
            <div id="calendar-days">
                <div class="day-title">MON</div>
                <div class="day-title">TUE</div>
                <div class="day-title">WED</div>
                <div class="day-title">THU</div>
                <div class="day-title">FRI</div>
                <div class="day-title">SAT</div>
                <div class="day-title">SUN</div>
            </div>
            <div id="calendar-out"></div>
        </div>
        <div class="alert alert-danger" id="error-date">Please select a valid date.</div>

        <div class="form-group row">
            <div class="col-6">
                <label for="start-time">Start Time</label>
                <select id="start-time" class="form-control">
                    <option value="0"></option>
                </select>
            </div>
            <div class="col-6">
                <label for="fin-time">Finish Time</label>
                <input type="hidden" id="fin-time">
                <p id="finish-time"></p>
            </div>

            <select id="start-time-template" style="display:none">
                <?php
                for($i = 480; $i <= 1320; $i+=30) {
                    $time = str_pad(floor($i/60), 2, '0', STR_PAD_LEFT).':'.str_pad($i%60, 2, '0', STR_PAD_LEFT);
                    echo '<option value="'.$time.'">'.$time.'</option>';
                }
                ?>
            </select>
        </div>
        <div class="alert alert-danger" id="error-start-time">Please select a start time</div>

        <?php the_field('date_page_text', $page->ID); ?>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="date-button">Next</button>
        </div>
    </div>
</div>