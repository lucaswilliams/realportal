<div id="page-public-contact" class="booking-page-container <?php echo ($iid > 0 && $cid > 0 && $lid > 0 && $tid > 0 ? '' : ''); ?>">
    <div id="page-public-contact-1" class="booking-page">
        <h1><?php the_field('public_contact_page_title', $page->ID); ?></h1>
        <?php the_field('public_contact_page_text', $page->ID); ?>
        <div class="form-group">
            <label for="public-contact-first">First Name</label>
            <input type="text" id="public-contact-first" class="form-control">
            <div class="alert alert-danger" id="error-public-first">Please enter your first name</div>
        </div>

        <div class="form-group">
            <label for="public-contact-last">Last Name</label>
            <input type="text" id="public-contact-last" class="form-control">
            <div class="alert alert-danger" id="error-public-last">Please enter your last name</div>
        </div>

        <div class="form-group">
            <label for="public-contact-email">Email Address</label>
            <input type="text" id="public-contact-email" class="form-control">
            <div class="alert alert-danger" id="error-public-email">Please enter a valid email</div>
        </div>

        <div class="form-group">
            <label for="public-contact-phone">Phone Number</label>
            <input type="text" id="public-contact-phone" class="form-control">
            <div class="alert alert-danger" id="error-public-phone">Please enter your phone number</div>
        </div>

        <div class="form-group">
            <label for="public-contact-who">Who are you booking this course for?</label>
            <div class="row">
                <div class="col-12 col-md booking-for-container partner public">
                    <a href="#" class="btn btn-default booking-for" data-id="0"><span>Myself</span></a>
                </div>
                <div class="col-12 col-md booking-for-container public">
                    <a href="#" class="btn btn-default booking-for" data-id="1"><span>Myself and others</span></a>
                </div>
                <div class="col-12 col-md booking-for-container public">
                    <a href="#" class="btn btn-default booking-for" data-id="2"><span>Other people</span></a>
                </div>
                <div class="col-12 col-md booking-for-container partner">
                    <a href="#" class="btn btn-default booking-for" data-id="3"><span>Myself and partner</span></a>
                </div>
            </div>
            <div class="alert alert-danger" id="error-public-type" style="clear: left;">Please select who the training is for</div>
        </div>

        <div class="form-group" id="online-multiples">
            <?php the_field('equipment_fees_description', $page->ID); ?>
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="public-contact-button">Next</button>
        </div>

        <div class="clearfix"></div>
    </div>
</div>