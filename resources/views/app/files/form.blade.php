@extends('layouts.app')

@section('content')
    <form method="POST" action="/app/files" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$file->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit media
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Media Name:</label>
                                <input type="text" name="name" id="name" class="form-control" data-validation="required" value="{{$file->name ?? ""}}">
                            </div>

                            <div class="form-group">
                                <label for="name">Media Artist:</label>
                                <input type="text" name="artist" id="artist" class="form-control" value="{{$file->artist ?? ""}}">
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="type">Media Type:</label>
                                        <select name="type" id="type" class="form-control" data-validation="required">
                                            <option value=""></option>
                                            <option value="0" {{$file->type === 0 ? "selected" : ""}}>Music</option>
                                            <option value="1" {{$file->type === 1 ? "selected" : ""}}>SFX</option>
                                            <option value="2" {{$file->type === 2 ? "selected" : ""}}>Siren</option>
                                            <option value="3" {{$file->type === 3 ? "selected" : ""}}>Clinical</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="active">Active:</label>
                                        <select name="active" id="active" class="form-control" data-validation="required">
                                            <option value="1" {{$file->active === 1 ? "selected" : ""}}>Active</option>
                                            <option value="0" {{$file->active === 0 ? "selected" : ""}}>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="art">Album Art:</label>
                                <input type="file" name="art" id="art" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="file">Media File:</label>
                                <input type="file" name="file" id="file" class="form-control">

                                <?php if(strlen($file->file) > 0) { ?>
                                    <p>Preview</p>
                                    <audio controls>
                                        <source src="/storage/media/<?php echo $file->file; ?>">
                                    </audio>
                                <?php } else { ?>
                                    <p>No media file has been uploaded.</p>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });
        });
    </script>
@endsection
