@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Media Files List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/app/files/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add media file</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>File Name</th>
                                <th>Artist</th>
                                <th>File Type</th>
                                <th>Active</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($files as $file) {
                            ?>
                                <tr>
                                    <td><?php echo (strlen($file->file) > 0 ? '' : '<i class="fa fa-exclamation-triangle"></i> '); ?>{{ $file->name }}</td>
                                    <td>{{ $file->artist }}</td>
                                    <td>{{ ($file->type == 0 ? 'Music' : ($file->type == 1 ? 'SFX' : ($file->type == 2 ? 'Siren' : 'Clinical'))) }}</td>
                                    <td>{{ ($file->active == 0 ? 'No' : 'Yes') }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/app/files/{{$file->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit file"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="/app/files/{{$file->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete file"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
