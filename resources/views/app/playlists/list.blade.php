@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>App Playlists List</h1>
                            </div>
                            <div class="col-3">
                                <a href="/app/playlists/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add playlist</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Playlist Name</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($playlists as $playlist) {
                            ?>
                                <tr>
                                    <td>{{ $playlist->name }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="/app/playlists/{{$playlist->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit playlist"><i class="fa fa-fw fa-pencil"></i></a>

                                        <a class="btn btn-danger delete" href="/app/playlists/{{$playlist->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete playlist"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
