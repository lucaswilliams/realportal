@extends('layouts.app')

@section('content')
    <form method="POST" action="/app/playlists" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$playlist->id ?? 0}}">
                    <div class="card">
                        <div class="card-header">
                            Edit playlist
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Playlist Name:</label>
                                <input type="text" name="name" id="name" class="form-control" data-validation="required" value="{{$playlist->name ?? ""}}">
                            </div>

                            <div class="form-group">
                                <label for="name">Playlist Artist:</label>
                                <input type="text" name="artist" id="artist" class="form-control" data-validation="required" value="{{$playlist->artist ?? ""}}">
                            </div>

                            <div class="form-group">
                                <label for="active">Active:</label>
                                <select name="active" id="active" class="form-control" data-validation="required">
                                    <option value="1" {{$playlist->active === 1 ? "selected" : ""}}>Active</option>
                                    <option value="0" {{$playlist->active === 0 ? "selected" : ""}}>Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $.validate({
                onSuccess: function($form) {
                    $('input[disabled]').removeAttr('disabled');
                },
                onError: function($form) {
                    var body = $("html, body");
                    body.stop().animate({scrollTop:0}, 500, 'swing');
                    $('.card-body').prepend('<div class="alert alert-danger">There were errors with the data you have supplied.  Please check the form for errors and try again.</div>')
                }
            });
        });
    </script>
@endsection
