@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-content-center">
                            <div class="col-9">
                                <h1>Booking URL Custom Field List</h1>
                                <h2>{{$booking_url}}</h2>
                            </div>
                            <div class="col-3">
                                <a href="{{$_SERVER['REQUEST_URI']}}/add" class="btn btn-success float-right"><i class="fa fa-plus"></i> Add custom field</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr>
                                <th>Field Title</th>
                                <th>Field Type</th>
                                <th></th>
                            </tr>
                            <?php
                                foreach($fields as $item) {
                            ?>
                                <tr>
                                    <td>{{ $item->field_title }}</td>
                                    <td>{{ $item->response_type }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-primary" href="{{$_SERVER['REQUEST_URI']}}/{{$item->id}}" data-toggle="tooltip" data-placement="bottom" title="Edit URL"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger delete" href="{{$_SERVER['REQUEST_URI']}}/{{$item->id}}/delete" data-toggle="tooltip" data-placement="bottom" title="Delete URL"><i class="fa fa-fw fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                                }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
