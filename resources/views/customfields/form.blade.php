@extends('layouts.app')

@section('content')
    <form method="POST" action="/urls/{{$item->booking_url_id}}/fields">
        {{csrf_field()}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <input type="hidden" name="id" value="{{$item->id ?? 0}}">
                    <input type="hidden" name="booking_url_id" value="{{$item->booking_url_id}}">
                    <div class="card">
                        <div class="card-header">
                            Edit Custom Field
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="field_title">Custom Field Title:</label>
                                <input type="text" name="field_title" id="field_title" class="form-control" data-validation="required" value="{{$item->field_title ?? ""}}">
                            </div>

                            <div class="form-group">
                                <label for="response_type">Response Type:</label>
                                <select name="response_type" id="response_type" class="form-control" data-validation="required">
                                    <option value="text" {{$item->response_type == 'text' ? 'selected' : ''}}>Text</option>
                                    <option value="number" {{$item->response_type == 'number' ? 'selected' : ''}}>Number</option>
                                    <option value="email" {{$item->response_type == 'email' ? 'selected' : ''}}>Email</option>
                                    <option value="select" {{$item->response_type == 'select' ? 'selected' : ''}}>Selection</option>
                                    <option value="multi" {{$item->response_type == 'multi' ? 'selected' : ''}}>Multi-select</option>
                                </select>
                            </div>

                            <div class="form-group" id="responses">
                                <div class="row">
                                    <div class="col-10">
                                        <label>Responses:</label>
                                    </div>
                                    <div class="col-2 text-right">
                                        <a href="#" class="btn btn-success add-row"><i class="fa fa-fw fa-plus"></i></a>
                                    </div>
                                </div>
                                <?php foreach($responses as $response) { ?>
                                <div class="row">
                                    <div class="col-10">
                                        <input type="text" name="response[]" class="form-control" value="{{$response->value}}">
                                    </div>
                                    <div class="col-2 text-right">
                                        <a href="#" class="btn btn-danger remove-row"><i class="fa fa-fw fa-trash-o"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-none" id="removed"></div>
                            <button type="submit" class="btn btn-success float-right">Save details</button>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="response-template" class="row">
        <div class="col-10">
            <input type="text" name="response[]" class="form-control">
        </div>
        <div class="col-2 text-right">
            <a href="#" class="btn btn-danger remove-row"><i class="fa fa-fw fa-trash-o"></i></a>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            if($('#response_type').val() == 'select' || $('#response_type').val() == 'multi') {
                $('#responses').show();
            }

            $('#response_type').on('change', function() {
                var _sel = $('#response_type option:selected').attr('value');
                if(_sel == 'select' || _sel == 'multi') {
                    $('#responses').show();
                } else {
                    $('#responses').hide();
                }
            });

            $(document).on('click', '.remove-row', function(e) {
                e.preventDefault();
                if(confirm('Are you sure you would like to remove this option?')) {
                    $(this).parents('.row').remove();
                }
            });

            $(document).on('click', '.add-row', function(e) {
                e.preventDefault();
                var newRow = $('#response-template').clone();
                newRow.removeAttr('id');
                newRow.appendTo('#responses');
            });
        });
    </script>
@endsection

@section('styles')
    <style>
        #response-template, #responses {
            display: none;
        }
    </style>
@endsection
