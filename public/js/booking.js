function updateCalendar(_year, _month, _location, _category) {
    _url = _baseurl + 'calendar.php';
    if(_year !== undefined) {
        _url += '&year=' + _year;
    }
    if(_month !== undefined) {
        _url += '&month=' + _month;
    }
    if(_location !== undefined) {
        _url += '&calendar=' + _location;
    }
    if(_category !== undefined) {
        _url += '&category=' + _category;
    }

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: _url.replace('&', '?'),
        success: function(data) {
            $('#calendar-month').text(data.month);
            $('#calendar-prev').data('year', data.prevYear).data('month', data.prevMonth);
            $('#calendar-next').data('year', data.nextYear).data('month', data.nextMonth);
            $('#calendar-out').html(data.calendar);
        }
    });
}

$('.training-type').on('click', function(e) {
    $('.training-type').removeClass('active');
    $(this).addClass('active');
    switch ($('.training-type.active').data('id')) {
        case 1:
            $.each($('.training-location[data-public=1]'), function(k,v) {
                $(v).parent().show();
            });
            $.each($('.training-location[data-public=0]'), function(k,v) {
            $(v).parent().hide();
        });
            $('#page-start-2').slideDown();
            $('#current-page').animate({scrollTop: $('#page-start-2').position().top}, 500);
            break;
        case 0:
            $.each($('.training-location[data-onsite=1]'), function(k,v) {
                $(v).parent().show();
            });
            $.each($('.training-location[data-onsite=0]'), function(k,v) {
            $(v).parent().hide();
        });
            $('#page-start-2').slideDown();
            $('#current-page').animate({scrollTop: $('#page-start-2').position().top}, 500);
            break;
        case 2:
            $('.training-course[data-online=0]').hide();
            $('#page-courses-1').slideDown();
            $('#current-page').animate({scrollTop: $('#page-courses-1').position().top}, 500);
            break;
    }
});

$('.training-state').on('click', function(e) {
    $('.training-state').removeClass('active');
    $(this).addClass('active');

    $('#page-public-instance-1').slideUp();

    $('.training-course').hide();
    $('.training-course.active').removeClass('active');

    //Hide courses that are not available at this location
    var _location = $('.training-state.active').data('axid');
    $.each($('.training-course'), function(k,v) {
        var _locations = $(v).data('locations');
        if(_locations !== undefined) {
            if (_locations.includes(_location) == true) {
                $(v).show();
            } else {
                $(v).hide();
            }
        } else {
            $(v).show();
        }
    });

    //Hide courses that don't match
    switch ($('.training-type.active').data('id')) {
        case 1:
            //$('.training-course[data-public=1]').show();
            $('.training-course[data-public=0]').hide();
            break;
        case 0:
            //$('.training-course[data-onsite=1]').show();
            $('.training-course[data-onsite=0]').hide();
            break;
        case 2:
            //$('.training-course[data-online=1]').show();
            $('.training-course[data-online=0]').hide();
            break;
    }

    var date = new Date();

    updateCalendar(date.getFullYear(), date.getMonth() + 1, $('.training-location.active').text().trim(), $('.training-course.active').data('category'));
    $('#page-courses-1').slideDown(300, function() {
        $('#current-page').animate({scrollTop: $('#page-courses-1').position().top}, 500);
    });
});

$(document).on('click', '.training-course', function(e) {
    $('.training-course').removeClass('active');
    $(this).addClass('active');

    _hours = $('.training-course.active').data('hours');
    _items = $('#start-time-template').children().clone();

    $('#start-time').html('<option value="0">(please select)</option>');
    for(i = 0; i < (_items.length - (_hours * 2)); i++) {
        $('#start-time').append(_items[i]);
    }
    $('#start-time').val(0);

    $('#progress-3').addClass('active');
    $('#course-text').remove();
    $('#booking-summary').append('<p id="course-text">The course you are booking is <a href="#" class="back-link" id="back-course">' + $('.training-course.active p strong').text().trim() + '</a></p>');

    if ($(this).data('duration') == 0) {
        //Half day, so show the prompt, and allow half days in the calendar
        $('#booking-calendar .half-day').show();
        $('#half-day').show();
    } else {
        $('#booking-calendar .half-day').hide();
        $('#half-day').hide();
    }

    if($('.training-type.active').text().trim() != 'Onsite') {
        //Go to aXcelerate and get the course data.
        $('#axcel-courses').html('<div class="course-grid container">    <div class="row header">        <div class="col-12">Available Sessions</div>    </div><div class="row">    <div class="col-12 text-center loading"><span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span></div></div></div>');
        if($('.training-type.active').data('id') == 2 && $('.training-course.active').data('type') == 'w') {
            //Show equipment
            $('#page-equipment').slideDown();
            $('#page-public-instance-1').hide();
        } else {
            //Show instances
            $('#page-equipment').hide();
            $('#page-public-instance-1').slideDown();
            getAxcelCourses();
        }
    } else {
        $('.booking-page.onsite').slideDown();
    }

    var date = new Date();

    updateCalendar(date.getFullYear(), date.getMonth() + 1, $('.training-location.active').text().trim(), $('.training-course.active').data('category'));
});

function getAxcelCourses() {
    $.ajax({
        'url': _baseurl + 'axcel.php',
        'dataType': 'html',
        'data': {
            'aid': $('.training-course.active').data('axid'),
            'cid': $('.training-course.active').data('id'),
            'lid': $('.training-state.active').data('axid'),
            'location_id': $('.pickup-location.active').data('location'),
            'geelong' : ($('.training-location.active p').text() == 'Geelong' || $('.training-location.active p').text() == 'Colac'),
            'city' : $('.training-location.active p').text()
        },
        'success': function (data) {
            $('#axcel-courses').html(data);
            $('#current-page').animate({scrollTop: $('#axcel-courses').position().top}, 500);
            $('[data-toggle="popover"]').popover();
        }
    });
}

$(document).on('click', '.training-location', function(e) {
    //hide all other
    $('.training-location').removeClass('active');
    $this = $(this);
    $this.addClass('active');
    //expand the list of courses at this location.
    $('.training-location-courses:not([data-id=' + $this.data('id') + '])').slideUp();
    $('.training-location-courses[data-id=' + $this.data('id') + ']').slideDown();
});

$(document).on('click', '.public-instance-button', function(e) {
    e.preventDefault();
    $parent = $(this).parents('.session');
    $('.session').hide();
    $parent.show();
    $(this).hide();
    $('#courseInstance').val($(this).data('id'));
    $('#courseAmtEach').val($(this).data('amt'));
    $('#selected-date').val($(this).data('date'));
    $('#participant-instance').val($(this).data('id'));
    $('#participant-type').val($('.training-course.active').data('type'));
    $('#page-public-contact-1').slideDown(300, function () {
        var _scroll = $('#current-page').scrollTop() + $('#page-public-contact').position().top;
        $('#current-page').animate({scrollTop: _scroll}, 500);
    });
});

$(document).on('click', '.online-delivery', function(e) {
    $('.online-delivery').removeClass('active');
    $(this).addClass('active');
    $('#error-online-type').hide();

    if($(this).data('id') == 1) {
        $('#online-delivery-delivered').slideDown();
        $('#online-delivery-pickup').slideUp();
    } else {
        $('#online-delivery-delivered').slideUp();
        $('#online-delivery-pickup').slideDown();

        $('#online-address-street').val('');
        $('#online-address-city').val('');
        $('#online-address-state').val('');
        $('#online-address-postcode').val('');
        $('#online-address-comments').val('');
    }

    updateCosts();
});

$(document).on('click', '#online-delivery-button', function(e) {
    //Validate
    $('#error-online-pickup').hide();
    $('#error-online-street').hide();
    $('#error-online-city').hide();
    $('#error-online-state').hide();
    $('#error-online-postcode').hide();
    _error = false;

    if($('.online-delivery.active').length > 0) {
        if ($('.online-delivery.active').data('id') == 1) {
            if ($('#online-address-street').val().trim().length == 0) {
                $('#error-online-street').show();
                _error = true;
            }

            if ($('#online-address-city').val().trim().length == 0) {
                $('#error-online-city').show();
                _error = true;
            }

            if ($('#online-address-state').val().trim().length == 0) {
                $('#error-online-state').show();
                _error = true;
            }

            if ($('#online-address-postcode').val().trim().length == 0) {
                $('#error-online-postcode').show();
                _error = true;
            }
        } else {
            if($('.pickup-location.active').length > 0) {
                //We have a location, we good
            } else {
                $('#error-online-pickup').show();
                _error = true;
            }
        }
    } else {
        $('#error-online-type').show();
        _error = true;
    }

    if(!_error) {
        getAxcelCourses();
        $('#page-public-instance-1').slideDown(300, function () {
            var _scroll = $('#current-page').scrollTop() + $('#page-public-instance-1').position().top;
            $('#current-page').animate({scrollTop: _scroll}, 500);
        });
    }
});

$(document).on('click', '.pickup-location', function() {
    $('.pickup-location').removeClass('active');
    $(this).addClass('active');
});

$(document).on('click', '#public-match-button', function(e) {
    $('.booking-for').hide();
    $('.remove-participant').hide();
    $('#add-participant-button').hide();
    $('#page-public-contact input').attr('readonly', 'readonly');
    $('#page-public-participants input').attr('readonly', 'readonly');
    $this = $(this);
    $this.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', 'disabled');

    if($('.booking-for.active').data('id') == 2) {
        $('#participant-booker').html('');
    } else {
        $('#participant-booker').html('<input type="hidden" name="public-participant-first[]" value="' + $('#public-contact-first').val() + '">' +
            '<input type="hidden" name="public-participant-last[]" value="' + $('#public-contact-last').val() + '">' +
            '<input type="hidden" name="public-participant-email[]" value="' + $('#public-contact-email').val() + '">' +
            '<input type="hidden" name="public-participant-phone[]" value="' + $('#public-contact-phone').val() + '">');
    }
    $.ajax({
        'type' : 'POST',
        'url' : _baseurl + 'axcel-person.php',
        'dataType' : 'json',
        'data' : $('#participant-form').serialize(),
        'success' : function(data) {
            $('#participant-match-results').html(data.html).prepend('<button type="button" class="btn btn-primary" id="change-participant-button">Change participants</button><p>&nbsp;</p>');
            $this.hide().html('Check participants').removeAttr('disabled');

            //Show payment stuff
            $('#page-payment-1').slideDown();
        }
    });
});

$(document).on('click', '.match-button', function() {
    $this = $(this);
    $this.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', 'disabled');
    //Find any filled input in this container and compare it to the container's attributes.
    _updated = false;
    $parent = $(this).parent().parent();
    $matchInfo = $parent.parent();
    _dob_d = '';
    _dob_m = '';
    _dob_y = '';
    _contact_id = 0;
    $.each($parent.find('input, select'), function(k,v) {
        if($(v).val().length > 0) {
            if($(v).hasClass('input-dob-d')) {
                s = '00' + $(v).val();
                _dob_d = s.substr(s.length - 2);
            }
            if($(v).hasClass('input-dob-m')) {
                _dob_m = $(v).val();
            }
            if($(v).hasClass('input-dob-y')) {
                _dob_y = $(v).val();
            }
        }
    });

    if(_dob_d.length > 0 && _dob_m.length > 0 && _dob_y.length > 0) {
        $.each($matchInfo.find('.pMatch[data-dob="' + _dob_y + '-' + _dob_m + '-' + _dob_d +  '"]'), function(l,w) {
            console.log($(w));
            $matchInfo.find('.public-participant').val($(w).data('id'));
            //$parent.find('.public-participant-detail').remove();
            _updated = true;
        });
    }

    if(_updated) {
        //flash a success?
        $matchInfo.append('<div class="col-12"><p>This user has been found in our system.</p></div>');
    } else {
        $(this).html('<i class="fa fa-ban"></i>').addClass('btn-danger').removeClass('btn-success');
        $matchInfo.append('<div class="col-12"><p>This user can\'t be found in our system.  A new user will be created.</p></div>');
    }

    $this.parent().parent().hide().prev().hide();
});

$(document).on('click', '#change-participant-button', function(e) {
    $(this).hide();
    $(this).parent().html('');
    $('#public-match-button').show();
    $('.booking-for').show();
    $('.remove-participant').show();
    $('#add-participant-button').show();
    $('#page-public-contact input').removeAttr('readonly');
    $('#page-public-participants input').removeAttr('readonly');
    $('#page-payment-1').slideUp();
});

$('.booking-for').on('click', function(e) {
    e.preventDefault();
    $('#participant-match-form-container').show();
    $('.booking-for').removeClass('active');
    $(this).addClass('active');
    if($(this).data('id') > 0) {
        if(!$('#page-public-participants-1').is(':visible')) {
            addParticipant();
        }
        $('#page-public-participants-1').slideDown();
        updateParticipantNumber();
    } else {
        $('#page-public-participants-1').slideUp();
        $('#participants').html('');
    }

    updateCosts();
});

$('#public-contact-button').on('click', function(e) {
    _error = false;
    if($('#public-contact-first').val().trim().length == 0) {
        $('#error-public-first').show();
        _error = true;
    }

    if($('#public-contact-last').val().trim().length == 0) {
        $('#error-public-last').show();
        _error = true;
    }

    if($('#public-contact-email').val().trim().length == 0) {
        $('#error-public-email').show();
        _error = true;
    } else {
        if(!validateEmail($('#public-contact-email').val().trim())) {
            $('#error-public-email').show();
            _error = true;
        }
    }

    if($('#public-contact-phone').val().trim().length == 0) {
        $('#error-public-phone').show();
        _error = true;
    }

    if($('.booking-for.btn-primary') == null || $('.booking-for.btn-primary').length == 0) {
        $('#error-public-type').show();
        _error = true;
    }

    if(!_error) {
        $('#page-public-contact').addClass('left');
        $('#progress-5').addClass('active');

        _bookType = $('.booking-for.btn-primary').data('id');

        $('#participant-booker').show().html('<h2>Participant <span class="participant-number">1</span></h2>' +
            '<p><strong>First Name:</strong> ' + $('#public-contact-first').val() + '</p>' +
            '<p><strong>Last Name:</strong> ' + $('#public-contact-last').val() + '</p>' +
            '<p><strong>Email:</strong> ' + $('#public-contact-email').val() + '</p>' +
            '<p><strong>Phone:</strong> ' + $('#public-contact-phone').val() + '</p>' +
            '<input type="hidden" name="public-participant-first[]" value="' + $('#public-contact-first').val() + '">' +
            '<input type="hidden" name="public-participant-last[]" value="' + $('#public-contact-last').val() + '">' +
            '<input type="hidden" name="public-participant-email[]" value="' + $('#public-contact-email').val() + '">' +
            '<input type="hidden" name="public-participant-phone[]" value="' + $('#public-contact-phone').val() + '">'
        );

        if(_bookType == 0) {
            //Only myself
            $('#contact-text').remove();
            $('#booking-summary').append('<p id="contact-text">The person being booked for is<br><a href="#" class="back-link" id="back-public-contact">' + $('#public-contact-first').val() + ' ' + $('#public-contact-last').val() + '</a></p>');
            showMatchPage();
        } else {
            $('#page-public-participants').removeClass('right');

            $('#contact-text').remove();
            $('#booking-summary').append('<p id="contact-text">The point of contact will be<br><a href="#" class="back-link" id="back-public-contact">' + $('#public-contact-first').val() + ' ' + $('#public-contact-last').val() + '</a></p>');

            if(_bookType == 1) {
                //Myself and others
            } else {
                $('#participant-booker').html('').hide();
            }

            addParticipant();
        }

        $('#current-page').scrollTop(0);

        updateCosts();
    }
});

$(document).on('click', '#add-participant-button', function(e) {
    e.preventDefault();
    addParticipant();
});

$(document).on('click', '.remove-participant', function(e) {
    e.preventDefault();
    $(this).parents('.participant-details').remove();

    //Update numbers or whatever...
    updateParticipantNumber();

    updateCosts();
});

$(document).on('click', '#btn-credit', function(e) {
    e.preventDefault();
    $(this).addClass('active');
    $('#btn-invoice').removeClass('active');
    $('#div-credit').slideDown();
    $('#div-invoice').slideUp();
});

$(document).on('click', '#btn-invoice', function(e) {
    e.preventDefault();
    $(this).addClass('active');
    $('#btn-credit').removeClass('active');
    $('#div-invoice').slideDown();
    $('#div-credit').slideUp();
});

$(document).on('blur', '#public-contact-first', function(e) {
    updateInvoiceContactPublic();
});

$(document).on('blur', '#public-contact-last', function(e) {
    updateInvoiceContactPublic();
});

$(document).on('blur', '#public-contact-email', function(e) {
    updateInvoiceContactPublic();
});

$(document).on('blur', '#contact-first', function(e) {
    updateInvoiceContactCorp();
});

$(document).on('blur', '#contact-last', function(e) {
    updateInvoiceContactCorp();
});

$(document).on('blur', '#contact-email', function(e) {
    updateInvoiceContactCorp();
});

$(document).on('blur', '#contact-organisation', function(e) {
    updateInvoiceContactCorp();
});

function updateInvoiceContactPublic() {
    $('#invoice-contact-first').val($('#public-contact-first').val());
    $('#invoice-contact-last').val($('#public-contact-last').val());
    $('#invoice-contact-email').val($('#public-contact-email').val());
    $('#confirm_email').val($('#public-contact-email').val());
    $('#invoice-contact-organisation').val('');
}

function updateInvoiceContactCorp() {
    $('#invoice-contact-first').val($('#contact-first').val());
    $('#invoice-contact-last').val($('#contact-last').val());
    $('#invoice-contact-email').val($('#contact-email').val());
    $('#confirm_email').val($('#contact-email').val());
    $('#invoice-contact-organisation').val($('#contact-organisation').val());
}

function updateParticipantNumber() {
    $i = ($('.booking-for.active').data('id') == 1 ? 2 : 1);
    $.each($('.participant-number'), function(k,v) {
        $(v).text($i);
        $i++;
    });
}

function addParticipant() {
    //how many are already there?
    _count = $('#participants').children('div').length;
    if(!$('#participant-booker').is(':hidden')) {
        _count++;
    }

    $clone = $('#participant-details-template').clone();
    $.each($clone.find('input'), function(k,v) {
        $(v).attr('id', $(v).attr('id') + (_count + 1));
    });

    $.each($clone.find('.participant-number'), function(k,v) {
        $(v).parent().append('<a href="#" class="btn btn-primary float-right remove-participant"><i class="fa fa-times"></i></a>');
    });

    $clone.removeAttr('id').appendTo('#participants');

    updateCosts();
    updateParticipantNumber();
}

function updateCosts() {
    var _costs = 0;
    var _participants = 0; //$('#participant-number').spinner('value');
    var _text = '';

    if($('.training-type.active').text().trim() == 'Onsite') {
        if ($('.training-course.active').length > 0) {
            $.each($('.training-course.active').data('pricing'), function (k, v) {
                if (_participants >= parseInt(v.minimum_participants) && _participants <= parseInt(v.maximum_participants)) {
                    if (parseInt(v.pricing_type) == 0) {
                        _costs = parseFloat(v.price_per_participant).toFixed(2);
                    } else {
                        _costs = (_participants * parseFloat(v.price_per_participant)).toFixed(2);
                    }
                    _text = v.information;
                }
            });

            if (_costs.length > 0) {
                $('.total-amount').text('$' + _costs);
                $('#courseAmt').val(_costs * 100);
                $('#participant-info').text(_text);
                $('#summary-total').show();
            }
        }
    } else {
        //Public pricing
        _count = $('#participants').children('div:visible').length + ($('.booking-for.active').data('id') != '2' ? 1 : 0);
        _costs = $('#courseAmtEach').val() * _count;

        if($('.training-type.active').data('id') == '2' && $('.online-delivery.active').data('id') == '1') {
            _costs += parseFloat($('#deliveryFee').val());
        }

        $('.total-amount').text('$' + _costs);
        $('#courseAmt').val(_costs * 100);
        if(_costs > 0) {
            $('#summary-total').show();
        } else {
            $('#summary-total').hide();
        }
    }
}

$('#apply-coupon').on('click', function(e) {
    e.preventDefault();
    _participants = $('#participants').children('div').length + ($('.booking-for.btn-primary').data('id') != '2' ? 1 : 0);
    $.ajax({
        'url' : _baseurl + 'coupon-check.php',
        'type' : 'post',
        'dataType' : 'json',
        'data' : {
            'coupon' : $('#coupon-code').val(),
            'participants' : _participants,
            'course' : $('.training-course.active').data('id'),
            'location' : $('.training-location.active').data('id'),
            'courseDate' : $('#selected-date').val()
        },
        'success' : function(data) {
            if(data.success == true) {
                console.log('success');
                //Apply it
                _costs = $('#courseAmt').val() / 100;
                if(data.type == 2) {
                    //percentage
                    _costs = _costs * ((100 - data.amount) / 100);
                } else {
                    if(data.useType == 1) {
                        _costs = _costs - data.amount;
                    } else {
                        _costs = _costs - (data.amount * _participants);
                    }
                }

                if (_costs < 0) {
                    _costs = 0;
                }

                _costs = _costs.toFixed(2);

                if(_costs == 0) {
                    $('#payment-form').hide();
                    $('#payment-button').text('Confirm');
                } else {
                    $('#payment-form').show();
                    $('#payment-button').text('Pay and confirm');
                }

                $('.total-amount').html('<strike>$' + ($('#courseAmt').val() / 100) + '</strike> $' + _costs);
                $('#courseAmt').val(_costs * 100);
                if(data.axType == 0) {
                    $('#courseAmtEach').val(_costs / _participants);
                }
                $('#coupon-success').show();
                $('#coupon-code-valid').val($('#coupon-code').val());
                $('#apply-coupon').attr('disabled', 'disabled').addClass('disabled');
            } else {
                console.log('error');
                //Show an error
                $('#coupon-errors').text(data.message).show();
                $('#coupon-errors').append('<div id="coupon-error-details"></div>');
                $.each(data.details, function(k,v) {
                    $('#coupon-error-details').append('<p>' + v + '</p>');
                });
                $('#coupon-code-valid').val('');
            }
        }
    });
});

$('#payment-button').on('click', function(e) {
    $(this).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', 'disabled');
    if($('#payment-form').is(':hidden')) {
        //probably safe to assume it's a $0 booking, right?
        processPayment(0);
    } else {
        getToken();
    }
});

$(document).on('click', '#change-amount', function(e) {
    $('#booking-amount').slideDown();
});

$(document).on('click', '#change-amount-btn', function(e) {
    _costs = $('#new-amount').val();

    $('.total-amount').text('$' + _costs);
    $('#courseAmt').val(_costs * 100);
    if(_costs == 0) {
        $('#payment-form').hide();
        $('#payment-button').text('Confirm');
    } else {
        $('#payment-form').show();
        $('#payment-button').text('Pay and confirm');
    }

    $('#booking-amount').slideUp();
});

$(document).on('click', '#find-in-xero', function() {
    $('#xero-results').html('<div class="row">' +
        '   <div class="col-12 text-center loading">' +
        '       <span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading contacts</span>' +
        '   </div>' +
        '</div>');

    $.ajax({
        'url': '/xero/contacts',
        'type': 'POST',
        'dataType': 'json',
        'data': {
            '_token': $('input[name="_token"]').val(),
            'first_name': $('#invoice-contact-first').val(),
            'last_name': $('#invoice-contact-last').val(),
            'email': $('#invoice-contact-email').val(),
            'organisation': $('#invoice-contact-organisation').val()
        },
        'success': function(data) {
            $('#xero-results').html('');

            if(data.length > 0) {
                $.each(data, function (k, v) {
                    $('#xero-results').append('<div class="xero-result">' +
                        '   <h4>' + v.organisation + '</h4>' +
                        '   <p>' + v.first_name + ' ' + v.last_name + '</p>' +
                        '   <p>' + v.email + '</p>' +
                        '   <input type="hidden" class="xero-guid" value="' + v.guid + '"' +
                        '</div>');
                });

                if(data.length == 1) {
                    $('.xero-result').addClass('active');
                }
            } else {
                $('#xero-results').append('<div class="xero-result active">' +
                    '   <h4>Create new contact</h4>' +
                    '   <p>A new contact will be created with the details above.</p>' +
                    '</div>');

                //slide down payment
                $('#page-payment-2').slideDown();
            }
        }
    })
});

$(document).on('click', '.xero-result', function() {
    $(this).addClass('active');
    $('#page-payment-2').slideDown();
});

/* Stripe */
var stripe = Stripe(_privkey);
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
var style = {
    base: {
        // Add your base input styles here. For example:
        fontSize: '16px',
        color: '#32325d'
    }
};

// Create an instance of the card Element.
var cardNumber = elements.create('cardNumber', {style: style});
var cardExpiry = elements.create('cardExpiry', {style: style});
var cardCVC = elements.create('cardCvc', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
cardNumber.mount('#card-element');
cardExpiry.mount('#card-expiry');
cardCVC.mount('#card-cvc');

// Create a token or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
    event.preventDefault();
    getToken();
});

function getToken() {
    $('#card-errors').hide();

    var tokenData = {};
    tokenData.name = $('#back-contact').text();
    tokenData.address_country = 'AU';

    stripe.createToken(cardNumber, tokenData).then(function(result) {
        if (result.error) {
            // Inform the customer that there was an error.
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
            $('#card-errors').show();
            $('#payment-button').html('Next').removeAttr('disabled');
        } else {
            // Send the token to your server.
            stripeTokenHandler(result.token);
        }
    });
}

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('id', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    processPayment(1);
}

$('#invoice-button').on('click', function(e) {
    $(this).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', 'disabled');
    processPayment(0);
});

function processPayment(_stripe) {
    if(_stripe === undefined) {
        _stripe = 1;
    }

    _type = $('.training-type.active').text().trim();
    $location =  $('.training-location.active');
    _price = $('.total-amount:first-of-type').html();
    _priceEach = $('#courseAmtEach').val();
    _priceTotal = $('#courseAmt').val();
    //Send the form off to Stripe'
    if(_type == 'Onsite') {
        _email = $('#contact-email').val();
        _first = $('#contact-first').val();
        _last = $('#contact-last').val();
        _org = $('#contact-organisation').val();
        _phone = $('#contact-number').val();
        _participants = $('#participant-number').val();
        _participant_detail = null;
    } else {
        _email = $('#public-contact-email').val();
        _first = $('#public-contact-first').val();
        _last = $('#public-contact-last').val();
        _org = '';
        _phone = $('#public-contact-phone').val();
        _participants = $('.public-participant').serialize();
        _participant_detail = $('.public-participant-detail').serialize();
    }
    $.ajax({
        'type' : 'POST',
        'url' : _baseurl + 'charge.php',
        'dataType' : 'json',
        'data' : {
            'first_name': _first,
            'last_name': _last,
            'email' : _email,
            'phone': _phone,
            'org' : _org,
            'position': $('#contact-position').val(),
            'takePayment' : _stripe,
            'token' : $('#stripeToken').val(),
            'amount' : $('#courseAmt').val(),
            'course_id' : $('.training-course.active').data('id'),
            'course_name': $('.training-course.active p').text().trim(),
            'location_id' : $location.data('id'),
            'location_name' : $location.text(),
            'ctype': $('.training-course.active').data('type'),
            'ctemp': $('.training-course.active').data('template'),
            'state_name' : $('.training-state.active').text().trim(),
            'date': $('#selected-date').val(),
            'type': _type,
            'instance': $('#courseInstance').val(),
            'geelong' : $('#geelong').val(),
            'invoice_first' : $('#invoice-contact-first').val(),
            'invoice_last' : $('#invoice-contact-last').val(),
            'invoice_email' : $('#invoice-contact-email').val(),
            'invoice_org' : $('#invoice-contact-organisation').val(),
            'invoice_guid' : $('.xero-result.active .xero-guid').val(),
            'calendar': $location.text().trim(),
            'client': _org,
            'time' : $('#start-time').val(),
            'finish-time' : $('#fin-time').val(),
            'street_name': $('#address-street').val(),
            'city': $('#address-city').val(),
            'state': $('#address-state').val(),
            'postcode': $('#address-postcode').val(),
            'comments': $('#address-comments').val(),
            'price': _price,
            'priceEach': _priceEach,
            'priceTotal': _priceTotal,
            'participants': _participants,
            'participant-detail': _participant_detail,
            'coupon' : $('#coupon-code-valid').val(),
            'clientid' : $('#booking-clientid').val(),
            'gclid' : $('#booking-gclid').val(),
            'trafficsource' : $('#booking-trafficsource').val(),
            'createdfrom' : 'Portal',
            'to_deliver' : $('.online-delivery.active').data('id') == 1 ? 'Yes' : 'No',
            'delivery_address' : $('#online-address-street').val().trim(),
            'delivery_city' : $('#online-address-city').val().trim(),
            'delivery_state' : $('#online-address-state').val().trim(),
            'delivery_postcode' : $('#online-address-postcode').val().trim(),
            'delivery_comments' : $('#address-online-comments').val().trim(),
            'pickup_location' : $('.pickup-location.active').data('id')
        },
        success : function(data) {
            //console.log(data);
            if(data.success) {
                processEmailSuccess((data.coupon ? 'coupon' : _type), _email);
            } else {
                $('#card-errors').html(data.message).show();
                $('#payment-button').html('Pay and confirm').removeAttr('disabled');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#card-errors').html(jqXHR.responseJSON.message).show();
            $('#payment-button').html('Pay and confirm').removeAttr('disabled');
        }
    });
}

function processEmailSuccess(type, email) {
    $('.card-body').html('<h1>Success</h1><p>Your booking has completed successfully, and a confirmation email will be sent to ' + email + '</p>');
}
