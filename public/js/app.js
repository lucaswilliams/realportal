$(document).ready(function() {
    $('#add-to-course-btn').click(function(e) {
        $('#add-to-course').slideDown();
    });

    $('#course_id').on('keyup', function(e) {
        $('#confirm_course_enrol').attr('disabled', 'disabled');
        $('#send_course_enrol').removeAttr('disabled').html('Check');
    });

    $('#send_course_enrol').unbind('click').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#confirm_course_enrol').attr('disabled', 'disabled')
        $(this).attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-pulse"></i>');
        $this = $(this);

        $.ajax({
            'method': 'GET',
            'url': '/axcelerate/coursedetails/' + $('#course_id').val(),
            'dataType': 'json',
            'success': function (data) {
                console.log(data);
                $('#course_details').html(
                    '<p><strong>Course Name:</strong> ' + data.COURSENAME + '</p>' +
                    '<p><strong>Course Code:</strong> ' + data.CODE + '</p>' +
                    '<p><strong>Course Date:</strong> ' + data.DATEDESCRIPTOR + '</p>' +
                    '<p><strong>Location:</strong> ' + data.LOCATION + '</p>');

                $('#confirm_course_enrol').removeAttr('disabled');
                $this.html('Check');
            },
            'error' : function(jqXHR, textStatus, errorThrown) {
                $('#course_details').html('<p>The course ID you have entered could not be located.</p>');
                $this.removeAttr('disabled');
                $this.html('Check');
            }
        });
    });

    $('#confirm_course_enrol').unbind('click').click(function(e) {
        $this = $(this);
        $this.attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-pulse"></i>');

        //Send enrolments to aXcelerate
        $.ajax({
            'method': 'POST',
            'url': '/axcelerate/enrol',
            'data': {
                '_token': $('input[name="_token"]').val(),
                'batch': $('#batch_id').val(),
                'course_id': $('#course_id').val()
            },
            'success': function(data) {
                console.log(data);
                $this.text('Done');
            }
        });
    });

    $('#uploadBulk').click(function(e) {
        if($(this).hasClass('disabled')) {
            e.preventDefault();
            e.stopPropagation();
        } else {
            $(this).addClass('disabled');
            $('#uploadBulkForm').submit();
        }
    });
});

$(document).on('click', '.delete', function(e) {
    e.stopImmediatePropagation();
    return confirm('Are you sure you want to ' + $(this).data('original-title'));
});
