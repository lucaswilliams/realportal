<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('/webhook', 'LogsController@processWebhook');

Route::get('/authenticate', function()
{
    return Forrest::authenticate();
});

Route::get('/sfcallback', function()
{
    Forrest::callback();

    return Redirect::to('/');
});

//Route::get('/form/locations', 'FormController@getLocations');
Route::get('/booking', 'FormController@getBookingForm');
Route::get('/booking/{id}', 'FormController@getBookingForm');

Route::post('/webhook', 'WebhookController@receiveMoodle');
Route::post('/webhook/beta', 'WebhookController@receiveMoodleBeta');
Route::get('/webhook/moodle', 'WebhookController@prepareMoodle');

Route::get('/cron/run', 'CronController@runCron');
Route::get('/cron/monday', 'CronController@Monday');
Route::get('/cron/auspost', 'CronController@AusPost');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    Route::get('/manage/xero', 'XeroController@index')->name('xero.auth.success');
    Route::get('/xero/auth/callback', 'XeroController@callback')->name('xero.auth.callback');
    Route::get('/manage/xero/delete', 'XeroController@delete')->name('xero.auth.delete');

    Route::get('/xero/contacts', 'XeroController@getOrCreateContact');
    Route::post('/xero/contacts', 'XeroController@getOrCreateContact');

    Route::get('/xero/contactLookup', 'XeroController@lookupContact');

    //Account
    Route::get('/account', 'AccountController@index');
    Route::post('/account', 'AccountController@save');
    Route::get('/account/email', 'AccountController@changeEmail');
    Route::post('/account/email', 'AccountController@saveEmail');
    Route::get('/account/password', 'AccountController@changePassword');
    Route::post('/account/password', 'AccountController@savePassword');

    Route::get('/clients', 'ClientsController@index');
    Route::get('/clients/add', 'ClientsController@showForm');
    Route::get('/clients/{id}', 'ClientsController@showForm')->where('id', '[0-9]+');
    Route::post('/clients', 'ClientsController@save');

    Route::get('/axcelerate/organisations/', 'AxcelerateController@getOrganisations');
    Route::get('/axcelerate/coursedetails/{instanceId}', 'AxcelerateController@getCourseDetails')->where('id', '[0-9]+');
    Route::post('/axcelerate/enrol/', 'AxcelerateController@enrolPeople');

    Route::get('/clients/{id}/locations', 'ClientsController@showLocations')->where('id', '[0-9]+');
    Route::get('/clients/{id}/locations/{location_id}', 'ClientsController@showLocationForm')->where('id', '[0-9]+')->where('location_id', '[0-9]+');
    Route::post('/clients/{id}/locations', 'ClientsController@saveLocation')->where('id', '[0-9]+');
    Route::get('/clients/{id}/delete', 'ClientsController@checkDelete')->where('id', '[0-9]+');

    Route::get('/clients/{id}/contacts', 'ClientsController@showContacts')->where('id', '[0-9]+');
    Route::get('/clients/{id}/contacts/add', 'ClientsController@showContactForm')->where('id', '[0-9]+');
    Route::get('/clients/{id}/contacts/{contact_id}', 'ClientsController@showContactForm')->where('id', '[0-9]+')->where('contact_id', '[0-9]+');
    Route::post('/clients/{id}/contacts', 'ClientsController@saveContact')->where('id', '[0-9]+');
    Route::get('/clients/{id}/contacts/{contact_id}/delete', 'ClientsController@deleteContact')->where('id', '[0-9]+')->where('contact_id', '[0-9]+');

    Route::get('/urls', 'URLController@showList');
    Route::get('/urls/add', 'URLController@showForm');
    Route::get('/urls/{id}', 'URLController@showForm')->where('id', '[0-9]+');
    Route::get('/urls/{id}/list', 'URLController@showUsage')->where('id', '[0-9]+');
    Route::post('/urls', 'URLController@saveForm');

    Route::post('/urls/{url_id}/fields', 'URLController@saveFields')->where('url_id', '[0-9]+');
    Route::get('/urls/{url_id}/fields', 'URLController@showFields')->where('url_id', '[0-9]+');
    Route::get('/urls/{url_id}/fields/{id}', 'URLController@showField')->where('url_id', '[0-9]+')->where('id', '[0-9]+');
    Route::get('/urls/{url_id}/fields/add', 'URLController@showField')->where('url_id', '[0-9]+');

    Route::get('/menuitems', 'MenuItemController@showList');
    Route::get('/menuitems/add', 'MenuItemController@showForm');
    Route::get('/menuitems/{id}', 'MenuItemController@showForm')->where('id', '[0-9]+');
    Route::get('/menuitems/{id}/delete', 'MenuItemController@delete')->where('id', '[0-9]+');
    Route::post('/menuitems', 'MenuItemController@saveForm');

    Route::get('/emailtemplates', 'EmailTemplateController@showList');
    Route::get('/emailtemplates/add', 'EmailTemplateController@showForm');
    Route::get('/emailtemplates/{id}', 'EmailTemplateController@showForm')->where('id', '[0-9]+');
    Route::get('/emailtemplates/{id}/delete', 'EmailTemplateController@delete')->where('id', '[0-9]+');
    Route::post('/emailtemplates', 'EmailTemplateController@saveForm');

    Route::get('/emailtemplatesusage', 'EmailTemplateController@showUsage');
    Route::post('/emailtemplatesusage', 'EmailTemplateController@saveUsage');

    Route::get('/users', 'UsersController@showList');
    Route::get('/users/add', 'UsersController@showForm');
    Route::get('/users/{id}', 'UsersController@showForm')->where('id', '[0-9]+');
    Route::get('/users/{id}/delete', 'UsersController@deleteUser')->where('id', '[0-9]+');
    Route::get('/users/{id}/password', 'UsersController@showResetPassword')->where('id', '[0-9]+');
    Route::post('/users/{id}/password', 'UsersController@saveResetPassword')->where('id', '[0-9]+');
    Route::post('/users', 'UsersController@saveUser');

    Route::get('/enrol', 'EnrolmentController@showForm');
    Route::get('/enrolcq', 'EnrolmentController@showCQForm');
    Route::get('/enrol/corporate', 'EnrolmentController@showBulk');
    Route::get('/enrol/corporate/list', 'EnrolmentController@showList');
    Route::post('/enrol/corporate', 'EnrolmentController@uploadBulk');
    Route::get('/enrol/corporate/{id}', 'EnrolmentController@validateBulk');
    Route::post('/enrol/corporate/{id}', 'EnrolmentController@saveUpdates');

    //App Management
    Route::get('/app/files', 'AppController@listFiles');
    Route::get('/app/files/add', 'AppController@showFileForm');
    Route::get('/app/files/{id}', 'AppController@showFileForm')->where('id', '[0-9]+');
    Route::post('/app/files', 'AppController@saveFile');
    Route::get('/app/files/{id}/delete', 'AppController@deleteFile');

    Route::get('/app/playlists', 'AppController@listPlaylists');
    Route::get('/app/playlists/add', 'AppController@showPlaylistForm');
    Route::get('/app/playlists/{id}', 'AppController@showPlaylistForm')->where('id', '[0-9]+');
    Route::post('/app/playlists', 'AppController@savePlaylist');

    Route::get('/logs', 'LogsController@showList');
    Route::get('/logs/{date}', 'LogsController@showLogs');

    Route::get('/cron', 'CronController@showList');
    Route::get('/cron/add', 'CronController@showForm');
    Route::get('/cron/{id}', 'CronController@showForm')->where('id', '[0-9]+');
    Route::post('/cron', 'CronController@saveCron');
    Route::get('/cron/{id}/delete', 'CronController@deleteCron')->where('id', '[0-9]+');

    //Need to do some better permissions stuff in here
    Route::get('/courses', 'CoursesController@showList');
    Route::get('/courses/{id}', 'CoursesController@showOne')->where('id', '[0-9]+');
    Route::post('/courses', 'CoursesController@saveCourse');

    Route::get('/courses/{id}/instances', 'CoursesController@showInstances')->where('id', '[0-9]+');

    Route::get('/coupons', 'CouponController@showList');
    Route::get('/coupons/{id}', 'CouponController@showForm')->where('id', '[0-9]+');
    Route::get('/coupons/{id}/delete', 'CouponController@deleteCoupon')->where('id', '[0-9]+');
    Route::get('/coupons/add', 'CouponController@showForm');
    Route::post('/coupons', 'CouponController@saveCoupon');

    Route::get('/coupons/types', 'CouponController@showCouponTypes');
    Route::get('/coupons/types/{id}', 'CouponController@showTypesForm')->where('id', '[0-9]+');
    Route::get('/coupons/types/{id}/delete', 'CouponController@deleteCouponType')->where('id', '[0-9]+');
    Route::get('/coupons/types/add', 'CouponController@showTypesForm');
    Route::post('/coupons/types', 'CouponController@saveCouponType');
});
