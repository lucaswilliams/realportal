<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
    Route::post('/login', 'Auth\LoginController@apiLogin');
    
    Route::middleware('auth:api')->group(function () {
        //App routes used to live in here.  Now they don't
    });
    Route::get('/media', 'AppController@getMedia');
    Route::get('/media/{id}', 'AppController@downloadMedia');
    Route::get('/art/{id}', 'AppController@downloadArt');

    Route::get('/form/locations', 'FormController@getLocations');
    Route::get('/form/courses', 'FormController@getCourses');
    Route::get('/form/courses/{locations}', 'FormController@getCourses');
    Route::get('/form/match/{first}/{last}', 'FormController@matchPeople');

    Route::post('/form/submit', 'FormController@sendBooking');

    Route::post('/coupon', 'CouponController@checkCoupon');
});
