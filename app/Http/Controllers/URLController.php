<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use DB;

class URLController extends Controller
{
    private function getQuery() {
        $q = DB::table('booking_urls');

        return $q;
    }

    public function getList() {
        $items = $this->getQuery()->get();
        return $items;
    }

    public function showList() {
        $items = $this->getList();

        return view('urls.list', ['items' => $items]);
    }

    public function getOne($id) {
        $query = $this->getQuery()->where('id', $id)->get();
        return $query[0] ?? null;
    }

    public function showForm($id = 0) {
        if($id > 0) {
            $item = $this->getOne($id);
        } else {
            $item = new \stdClass();
            $item->id = 0;
            $item->link_url = '';
            $item->equipment = 0;
            $item->password = '';
            $item->client_id = 0;
            $item->invoices = 0;
            $item->priority_courses = 0;
            $item->freetext = '';
            $item->booking_email = 0;
            $item->confirmation_template = 0;
            $item->confirmation_email = '';
            $item->real_response = 0;
            $item->cq_first_aid = 0;
            $item->locations = '';
            $item->courses = '';
            $item->slack_channel = '';
        }

        $courses = DB::table('booking_url_courses')
            ->where('booking_url_id', $id)
            ->get();

        //get the wordpress courses.  can't use wordpress functions, can use API
        $ch = curl_init();
        $url = env('WP_API')."booking/courses";
        //var_dump($url); echo '<br>';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        //var_dump($output); echo '<br>';

        $wpcourses = json_decode($output);
        //var_dump($wpcourses);
        //die();

        $ch = curl_init();
        $url = env('WP_API')."booking/locations";
        //var_dump($url); echo '<br>';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        //var_dump($output); echo '<br>';

        $wplocations = json_decode($output);
        if($wplocations == null) {
            $wplocations = [];
        }

        $cc = new ClientsController();
        $clients = $cc->getClients();

        $tc = new EmailTemplateController();
        $templates = $tc->getURLList();

        $data = [
            'item' => $item,
            'courses' => $courses,
            'wpcourses' => $wpcourses,
            'clients' => $clients,
            'templates' => $templates,
            'locations' => $wplocations,
        ];

        return view('urls.form', $data);
    }

    public function saveForm(Request $request) {
        $id = $request->id;

        if(is_array($request->locations)) {
            $locations = implode(',', $request->locations);
        } else {
            $locations = $request->locations;
        }

        $data = [
            'link_url' => $request->link_url,
            'equipment' => $request->equipment,
            'password' => $request->password,
            'client_id' => ($request->client_id > 0 ? $request->client_id : null),
            'invoices' => $request->invoices,
            'priority_courses' => $request->priority_courses,
            'freetext' => $request->freetext,
            'booking_email' => $request->booking_email,
            'confirmation_template' => $request->confirmation_template,
            'confirmation_email' => $request->confirmation_email,
            'real_response' => $request->real_response > 0 ? 1 : 0,
            'cq_first_aid' => $request->cq_first_aid > 0 ? 1 : 0,
            'locations' => $locations,
            'slack_channel' => $request->slack_channel
        ];

        if($data['confirmation_template'] == '0') {
            $data['confirmation_template'] = null;
        }

        if($id > 0) {
            DB::table('booking_urls')
                ->where('id', $id)
                ->update($data);
        } else {
            //Insert
            $id = DB::table('booking_urls')
                ->insertGetId($data);
        }

        //then put in all of the courses
        if(isset($request->course_id) && is_array($request->course_id) && count($request->course_id) > 0) {
            foreach ($request->course_id as $idx => $cse) {
                $coupon = $request->coupon_code[$idx];
                $template = $request->template_id[$idx];
                $type = $request->course_type[$idx];
                $price = $request->price[$idx];

                $existing = DB::table('booking_url_courses')
                    ->where('booking_url_id', $id)
                    ->where('course_id', $cse)
                    ->get();
                if (count($existing) > 0) {
                    $cseid = $existing[0]->id;
                } else {
                    $cseid = 0;
                }

                if ($cseid > 0) {
                    //Update
                    DB::table('booking_url_courses')
                        ->where('id', $cseid)
                        ->update([
                            'coupon_code' => $coupon,
                            'template_id' => $template,
                            'course_type' => $type,
                            'price' => $price
                        ]);
                } else {
                    //Insert
                    DB::table('booking_url_courses')
                        ->insert([
                            'booking_url_id' => $id,
                            'course_id' => $cse,
                            'coupon_code' => $coupon,
                            'template_id' => $template,
                            'course_type' => $type,
                            'price' => $price
                        ]);
                }
            }
        }

        //delete the ones marked to delete
        if(isset($request->removed_courses) && count($request->removed_courses) > 0) {
            foreach ($request->removed_courses as $cse) {
                DB::table('booking_url_courses')
                    ->where('id', $cse)
                    ->delete();
            }
        }

        return redirect('/urls');
    }

    public function showUsage($id) {
        $item = $this->getOne($id);
        //echo '<pre>'; var_dump($item); echo '</pre>';

        //One line for each participant, not each booking.  Could be fun.

        if(!isset($_GET['start-date'])) {
            $start_date = date('Y-m').'-01';
            $end_date = date('Y-m-t');
        } else {
            $start_date = $_GET['start-date'];
            $end_date = $_GET['end-date'];
        }

        $usage = DB::connection('wordpress')
            ->table('wp_rr_bookings')
            ->where('booking_url', $item->link_url)
            ->where('booking_date', '>=', $start_date)
            ->where('booking_date', '<=', $end_date.' 23:59:59')
            ->get();

        $usages = [];
        foreach($usage as $use) {
            $participants = explode(',', $use->participant_names);
            foreach($participants as $participant) {
                $fields = [];
                parse_str(urldecode($use->custom_fields), $fields);
                $thisuse = new \stdClass;
                $thisuse->course_name = $use->course_name;
                $thisuse->location_name = $use->location_name;
                $thisuse->booking_contact = $use->booking_contact;
                $thisuse->email = $use->email;
                $thisuse->booking_content = $use->booking_content;
                $thisuse->booking_date = $use->booking_date;
                $thisuse->course_date = $use->course_date;
                $thisuse->price = floatval(str_replace(['$0.00', '$'], '', $use->price)) / $use->participants;
                $thisuse->participant_name = $participant;
                $thisuse->custom_fields = $fields['custom_fields'] ?? [];
                //$thisuse->price = floatval($use->price) / $use->participants;
                $usages[] = $thisuse;
            }
        }

        $fields = DB::table('custom_fields')
            ->where('booking_url_id', $id)
            ->get();

        return view('urls.usage', [
            'url' => $item->link_url,
            'start' => $start_date,
            'end' => $end_date,
            'usage' => $usages,
            'fields' => $fields
        ]);
    }

    public function showField($url_id, $id = 0){
        $item = new \stdClass();

        if($id > 0) {
            //load data.
            $items = DB::table('custom_fields')
                ->where('id', $id)
                ->get();
            if(count($items) > 0) {
                $item = $items[0];
            }
        }

        if(!property_exists($item, 'field_title')) {
            $item->booking_url_id = $url_id;
            $item->field_title = '';
            $item->response_type = 'text';
        }

        $responses = DB::table('custom_field_options')
            ->where('custom_field_id', $id)
            ->get();

        return view('customfields.form', ['item' => $item, 'responses' => $responses]);
    }

    public function deleteField($url_id, $id){
        //delete!!!
    }

    public function showFields($url_id){
        $fields = DB::table('custom_fields')
            ->where('booking_url_id', $url_id)
            ->get();

        $booking_url = DB::table('booking_urls')
            ->where('id', $url_id)
            ->get()[0]->link_url;

        return view('customfields.list', ['fields' => $fields, 'booking_url' => $booking_url]);
    }

    public function saveFields($url_id, Request $request) {
        $id = $request->id;

        $data = [
            'booking_url_id' => $request->booking_url_id,
            'field_title' => $request->field_title,
            'response_type' => $request->response_type
        ];

        if($id > 0) {
            DB::table('custom_fields')
                ->where('id', $id)
                ->update($data);
        } else {
            //Insert
            $id = DB::table('custom_fields')
                ->insertGetId($data);
        }

        //Now responses
        DB::table('custom_field_options')
            ->where('custom_field_id', $id)
            ->delete();

        if(($request->response_type == 'select' || $request->response_type == 'multi') && is_array($request->response)) {
            foreach($request->response as $response) {
                DB::table('custom_field_options')
                    ->insert([
                        'custom_field_id' => $id,
                        'value' => $response
                    ]);
            }
        }

        return redirect('/urls/'.$url_id.'/fields')->with('success', 'The custom field was saved successfully');
    }
}
