<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CronController extends Controller
{
    public function getList() {
        return DB::table('cronjobs')
            ->select()
            ->get();
    }

    public function showList() {
        $cronjobs = $this->getList();
        $jobs = [];
        foreach($cronjobs as $job) {
            //work out frequency
            if($job->time_y == -1 && $job->time_m == -1 && $job->time_d == -1) {
                //hourly or minute-ly
                $freq = 'Every '.($job->time_h == -1 ? $job->time_i.' minute'.($job->time_i == 1 ? '' : 's') : $job->time_h.' hour'.($job->time_h == 1 ? '' : 's'));
            } else {
                $freq = 'Once at '.sprintf("%02d", $job->time_d).'/'.sprintf("%02d", $job->time_m).'/'.sprintf("%04d", $job->time_y).' '.sprintf("%02d",$job->time_h).':'.sprintf("%02d",$job->time_i);
            }

            $job->frequency = $freq;
            $jobs[] = $job;
        }

        return view('cron.list', ['cronjobs' => $jobs]);
    }

    public function showForm($id = 0) {
        $cronjobs = DB::table('cronjobs')
            ->where('id', $id)
            ->select()
            ->get();

        if(count($cronjobs) > 0) {
            $cron = $cronjobs[0];
        } else {
            $cron = new \stdClass();
            $cron->id = 0;
            $cron->name = '';
            $cron->file = '';
            $cron->time_y = -1;
            $cron->time_m = -1;
            $cron->time_d = -1;
            $cron->time_h = -1;
            $cron->time_i = -1;
        }

        return view('cron.form', ['item' => $cron]);
    }

    public function saveCron(Request $request) {
        $content = $this->convertPost($request);

        if(isset($content['id'])) {
            $id = intval($content['id']);
            unset($content['id']);
        } else {
            $id = 0;
        }

        if($id == 0) {
            DB::table('cronjobs')
                ->insert($content);
            $addedit = 'added';
        } else {
            DB::table('cronjobs')
                ->where('id', '=', $id)
                ->update($content);
            $addedit = 'updated';
        }

        return redirect('/cron')->with(['success' => 'The cron job has been '.$addedit.' successfully.']);
    }

    public function deleteCron($id) {
        DB::table('cronjobs')
            ->where('id', $id)
            ->delete();

        return redirect('/cron')->with(['success' => 'The cron job has been deleted successfully.']);
    }

    public function runCron() {
        $time = time();
        //$time = strtotime('2021-10-25 10:05');
        $y = intval(date('Y', $time));
        $m = intval(date('m', $time));
        $d = intval(date('d', $time));
        $h = intval(date('H', $time));
        $i = intval(date('i', $time));

        $jobs = $this->getList();
        foreach($jobs as $job) {
            //echo '<pre>'; var_dump($job); echo '</pre>';
            $do_y = false;
            $do_m = false;
            $do_d = false;
            $do_h = false;
            $do_i = false;
            $once = false;

            if($job->time_y != -1) {
                $once = true;
            } else {
                //is the hour and minute a multiple of their respective things?
                if($job->time_h == 0 && $job->time_i == 0) {
                    //Midnight. Special case
                } elseif ($job->time_i ==0) {
                    //On the hour.
                    if($h % $job->time_h == 0) {
                        $do_h = true;
                    }

                    if($i == 0) {
                        $do_i = 0;
                    }
                } else {
                    //Probably minutes
                    if($h % $job->time_h == 0) {
                        $do_h = true;
                    }

                    if($i % $job->time_i == 0) {
                        $do_i = true;
                    }
                }
            }

            echo '<pre>';
            var_dump($do_y);
            var_dump($do_m);
            var_dump($do_d);
            var_dump($once);
            var_dump($do_h);
            var_dump($do_i);
            echo '</pre>';

            if(($do_y && $do_m && $do_d) || (!$once && $do_h && $do_i)) {
                //run the thing listed
                echo $job->file.'<br>';
                $fc = file_get_contents($job->file);
                echo $fc.'<hr>';
            }
        }
    }

    // Put CRON tasks here.
    public function Monday() {
        //Monday, run every hour.
        $token = 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExODY5ODU0OCwidWlkIjo5MzU4MDUyLCJpYWQiOiIyMDIxLTA3LTI3VDIzOjM0OjI4LjE1MVoiLCJwZXIiOiJtZTp3cml0ZSIsImFjdGlkIjo0MjU2MjQ1LCJyZ24iOiJ1c2UxIn0.CJ270Pp8J6HKcTmFRvpLu3Cz4qaPDCEFkyNWDIO_-h4';
        $apiUrl = 'https://api.monday.com/v2';
        $headers = ['Content-Type: application/json', 'Authorization: ' . $token];

        $query = 'query {
            items_by_multiple_column_values (board_id: 1644401690, column_id: "status7", column_values: ["Sent", "Booked"]) {
                id
                name
                column_values {
                  title
                  text
                }
            }
        }';

        echo $query;
        $data = @file_get_contents($apiUrl, false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => $headers,
                'content' => json_encode(['query' => $query]),
            ]
        ]));
        $responseContent = json_decode($data, true);
        if(isset($responseContent['data'])) {
            foreach ($responseContent['data']['items_by_multiple_column_values'] as $value) {
                $delivery_tracking = '';
                $delivery_status = '';
                $return_tracking = '';
                $return_status = '';
                foreach ($value['column_values'] as $column) {
                    if ($column['title'] == 'Delivery Tracking') {
                        $delivery_tracking = $column['text'];
                    }
                    if ($column['title'] == 'Delivery Status') {
                        $delivery_status = $column['text'];
                    }
                    if ($column['title'] == 'Return Tracking') {
                        $return_tracking = $column['text'];
                    }
                    if ($column['title'] == 'Return Status') {
                        $return_status = $column['text'];
                    }
                }

                /*echo '<pre>';
                var_dump($value['id']);
                var_dump($delivery_tracking);
                var_dump($delivery_status);
                var_dump($return_tracking);
                var_dump($return_status);
                echo '</pre>';*/

                if(strlen($delivery_tracking) > 0 || strlen($return_tracking) > 0) {
                    $sql = "INSERT INTO auspost_cache(item_id, delivery_tracking, delivery_status, return_tracking, return_status, last_update) SELECT ".$value['id'].", '".$delivery_tracking."', '".$delivery_status."', '".$return_tracking."', '".$return_status."', '".date('Y-m-d H:i:s')."' FROM DUAL WHERE NOT EXISTS (SELECT * FROM auspost_cache WHERE item_id = ".$value['id'].")";

                    DB::statement(DB::raw($sql));
                }
            }
        }
    }

    public function AusPost() {
        $trackSql = DB::table('auspost_cache')
            ->where('delivery_status', '<>', 'Delivered')
            ->where('return_status', '<>', 'Delivered')
            ->orderBy('last_update', 'asc')
            ->take(10);
        echo $trackSql->toSql();
        $trackings = $trackSql->get();

        foreach($trackings as $idx => $tracking) {
            echo '<pre>'.$idx;
            //var_dump($tracking);
            echo '</pre>';

            $tracking_id = [];
            $item_id = $tracking->item_id;
            $delivery_tracking = $tracking->delivery_tracking;
            $delivery_status = $tracking->delivery_status;
            $return_tracking = $tracking->return_tracking;
            $return_status = $tracking->return_status;

            if ($delivery_status != 'Delivered' && strlen($delivery_tracking) > 0) {
                $tracking_id[] = str_replace([' ', '/'], ['', ','], $delivery_tracking);
            }

            if ($return_status != 'Delivered' && strlen($return_tracking) > 0) {
                $tracking_id[] = str_replace([' ', '/'], ['', ','], $return_tracking);
            }

            $token = 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExODY5ODU0OCwidWlkIjo5MzU4MDUyLCJpYWQiOiIyMDIxLTA3LTI3VDIzOjM0OjI4LjE1MVoiLCJwZXIiOiJtZTp3cml0ZSIsImFjdGlkIjo0MjU2MjQ1LCJyZ24iOiJ1c2UxIn0.CJ270Pp8J6HKcTmFRvpLu3Cz4qaPDCEFkyNWDIO_-h4';
            $apiUrl = 'https://api.monday.com/v2';
            $headers = ['Content-Type: application/json', 'Authorization: ' . $token];

            if (count($tracking_id) > 0) {
                $ch = curl_init();
                $url = "https://digitalapi.auspost.com.au/shipping/v1/track?tracking_ids=" . implode(',', $tracking_id);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, "134cbf72-6b5a-47b2-aeb1-68e0661f90a9:x57#786FADE47BB69B1!");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                //curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $result = curl_exec($ch);
                curl_close($ch);

                //var_dump($result);
                //put $tracking_id and $result into the log table
                DB::table('auspost_log')
                    ->insert([
                        'log_date' => date('Y-m-d H:i:s'),
                        'tracking_ids' => $url,
                        'response' => $result
                    ]);

                $tr = json_decode($result, true);
                if (isset($tr['tracking_results'])) {
                    $query = '';
                    foreach ($tr['tracking_results'] as $track) {

                        echo '<h1>TRACK</h1>';
                        echo '<pre>'; var_dump($track); echo '</pre>';

                        $status = '';
                        if (isset($track['status'])) {
                            $status = $track['status'];
                        } elseif (isset($track['trackable_items'])) {
                            $status = $track['trackable_items'][0]['items'][0]['status'];
                        } elseif (isset($track['errors']) && isset($track['errors'][0]['message'])) {
                            $status = $track['errors'][0]['message'];
                        }
                        var_dump($status);

                        //Cleaner instead of doing this for each status case
                        if ($delivery_tracking == $track['tracking_id']) {
                            $query = 'mutation { change_simple_column_value(item_id:'.$item_id.', board_id:1644401690, column_id:"text1", value:"'.$status.'") {
id
}}';
                            $delivery_status = $status;
                        } else {
                            $query = 'mutation { change_simple_column_value(item_id:'.$item_id.', board_id:1644401690, column_id:"text_10", value:"'.$status.'") {
id
}}';
                            $return_status = $status;
                        }

                        echo $query . '<br><br>';

                        if (strlen($query) > 0) {
                            $data = @file_get_contents($apiUrl, false, stream_context_create([
                                'http' => [
                                    'method' => 'POST',
                                    'header' => $headers,
                                    'content' => json_encode(['query' => $query]),
                                ]
                            ]));

                            echo '<pre>Updating '; var_dump($idx); var_dump($item_id); echo '</pre>';

                            DB::table('auspost_cache')
                                ->where('item_id', $item_id)
                                ->update([
                                    'delivery_status' => $delivery_status,
                                    'return_status' => $return_status,
                                    'last_update' => date('Y-m-d H:i:s')
                                ]);
                        }
                    }
                }
            }
            echo $item_id.'<hr>';
        }
    }
}
