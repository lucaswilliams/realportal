<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;

class CouponController extends Controller
{
    public function showList() {
        $page = 1;
        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        $coupons = DB::table('coupons')
            ->offset(($page-1) * 100)
            ->limit(100)
            ->get();

        return view('coupons.list', ['coupons' => $coupons]);
    }

    public function showForm($id = 0) {
        $coupon = $this->getCoupon($id);

        //Get the locations and the courses so we can select from them
        Forrest::authenticate();
        $sql = "SELECT Id, Name FROM Account WHERE RecordType.Name = 'Location' AND ParentID = '' ORDER BY Order_Display__c";
        $res = Forrest::query($sql);

        $locations = [];
        foreach($res['records'] as $l) {
            $sql2 = "SELECT Id, Name FROM Account WHERE RecordType.Name = 'Location' AND ParentID = '".$l['Id']."' ORDER BY Order_Display__c, Name";
            $res2 = Forrest::query($sql2);
            foreach($res2['records'] as $rec) {
                $loc = new \stdClass();
                $loc->id = $rec['Id'];
                $loc->title = $rec['Name'];
                $locations[] = $loc;
            }
        }

        $sql = "SELECT Id, rio_ed__Description__c, Name, RecordType.Name FROM rio_ed__Session__c WHERE RecordType.Name = 'Template'";
        $res = Forrest::query($sql);
        $courses = [];
        foreach($res['records'] as $rec) {
            $cse = new \stdClass();
            $cse->id = $rec['Id'];
            $cse->title = $rec['Name'];
            $courses[] = $cse;
        }

        $coupon_types = $this->getCouponTypes();

        return view('coupons.form', ['coupon' => $coupon, 'courses' => $courses, 'locations' => $locations, 'couponTypes' => $coupon_types]);
    }

    public function getCoupon($id) {
        $coupons = DB::table('coupons')
            ->where('id', $id)
            ->get();
        if(count($coupons) > 0) {
            $coupon = $coupons[0];
            $coupon->locations = explode(',', $coupon->locations);
            $coupon->courses = explode(',', $coupon->courses);
        } else {
            //Make a new one
            $coupon = new \stdClass();
            $coupon->id = 0;
            $coupon->coupon_code= '';
            $coupon->coupon_type = 0;
            $coupon->courses = [];
            $coupon->locations = [];
            $coupon->uses= 0;
            $coupon->amount= 0.00;
            $coupon->amount_type= 0;
            $coupon->start_date= '';
            $coupon->end_date= '';
            $coupon->usage_type= 0;
            $coupon->date_type= 0;
            $coupon->discount_type = 0;
        }

        return $coupon;
    }

    public function getCouponTypes() {
        return DB::table('coupon_types')
            ->orderBy('name')
            ->get();
    }

    public function showCouponTypes() {
        $coupons = $this->getCouponTypes();
        return view('coupons.types.list', ['coupontypes' => $coupons]);
    }

    public function showTypesForm($id = 0) {
        $coupons = DB::table('coupon_types')
            ->where('id', $id)
            ->get();
        if(count($coupons) > 0) {
            $coupon = $coupons[0];
        } else {
            //Make a new one
            $coupon = new \stdClass();
            $coupon->id = 0;
            $coupon->name = '';
        }

        return view('coupons.types.form', ['coupon' => $coupon]);
    }

    public function deleteCoupon($id) {
        DB::table('coupons')
            ->where('id', $id)
            ->delete();

        return redirect('/coupons')->with('success', 'The coupon type has been deleted successfully');
    }

    public function deleteCouponType($id) {
        DB::table('coupon_types')
            ->where('id', $id)
            ->delete();

        return redirect('/coupons/types')->with('success', 'The coupon type has been deleted successfully');
    }

    public function saveCoupon(Request $request) {
        $content = $this->convertPost($request);

        //dates could be null
        if(strlen($content['start_date']) == 0) {
            $content['start_date'] = null;
        }
        if(strlen($content['end_date']) == 0) {
            $content['end_date'] = null;
        }
        if(strlen($content['coupon_type']) == 0) {
            $content['coupon_type'] = null;
        }
        if(!isset($content['courses'])) {
            $content['courses'] = null;
        }
        if(!isset($content['locations'])) {
            $content['locations'] = null;
        }

        if(is_array($content['courses'])) {
            $content['courses'] = implode(',', $content['courses']);
        }
        if(is_array($content['locations'])) {
            $content['locations'] = implode(',', $content['locations']);
        }

        if($content['id'] == '0') {
            unset($content['id']);
            DB::table('coupons')
                ->insert($content);
            $addedit = 'added';
        } else {
            DB::table('coupons')
                ->where('id', '=', $content['id'])
                ->update($content);
            $addedit = 'updated';
        }

        return redirect('/coupons')->with('success', 'The coupon ('.$content['coupon_code'].') has been '.$addedit.' successfully');
    }

    public function saveCouponType(Request $request) {
        $content = $this->convertPost($request);

        if($content['id'] == '0') {
            unset($content['id']);
            DB::table('coupon_types')
                ->insert($content);
            $addedit = 'added';
        } else {
            DB::table('coupon_types')
                ->where('id', '=', $content['id'])
                ->update($content);
            $addedit = 'updated';
        }

        return redirect('/coupons/types')->with('success', 'The coupon ('.$content['name'].') has been '.$addedit.' successfully');
    }

    public function checkCoupon(Request $request) {
        $content = $this->convertPost($request);
        //Does the coupon exist?
        $coupons = DB::table('coupons')
            ->where('coupon_code', $content['coupon_code'])
            ->get();
        if(count($coupons) == 0) {
            return json_encode(['success' => false, 'message' => 'Coupon code is invalid', 'details' => 'The coupon code specified is not in our database']);
        }

        //It exists.
        $coupon = $coupons[0];
        $totalAmt = floatval($content['courseAmt']) / 100;
        $amtEach = floatval($content['courseAmtEach']);
        $participants = intval($content['participants']);
        if(strlen($coupon->locations) > 0) {
            //It must be in this location (Region level)
            $coupon->locations = explode(',', $coupon->locations);
            if(!in_array($content['location'], $coupon->locations)) {
                return json_encode(['success' => false, 'message' => 'Coupon code is invalid for your selected location', 'details' => 'The location supplied ('.$content['location'].') does not match the ones in the DB ('.implode(', ', $coupon->locations).')']);
            }
        }

        if(strlen($coupon->courses) > 0) {
            //It must be in this course
            $coupon->courses = explode(',', $coupon->courses);
            if(!in_array($content['course'], $coupon->courses)) {
                return json_encode(['success' => false, 'message' => 'Coupon code is invalid for your selected course', 'details' => 'The course supplied ('.$content['course'].') does not match the ones in the DB ('.implode(', ', $coupon->courses).')']);
            }
        }

        //Does the date type match?
        switch($coupon->date_type) {
            case 0 :
                //do nothing
                break;

            case 1:
                if(strtotime($content['course_date']) >= strtotime($coupon->start_date) &&  strtotime($content['course_date']) <= strtotime($coupon->end_date)) {
                    //We good
                } else {
                    return json_encode(['success' => false, 'message' => 'Coupon code is only valid for courses between '.date('d/m/Y', strtotime($coupon->start_date)).' and '.date('d/m/Y', strtotime($coupon->end_date)), 'details' => 'Course date supplied ('.date('d/m/Y', $content['course_date']).') was outside '.date('d/m/Y', strtotime($coupon->start_date)).' and '.date('d/m/Y', strtotime($coupon->end_date))]);
                }
                break;

            case 2:
                if(time() >= strtotime($coupon->start_date) &&  strtotime($content['course_date']) <= strtotime($coupon->end_date)) {
                    //We good
                } else {
                    return json_encode(['success' => false, 'message' => 'Coupon code is only valid for use between '.date('d/m/Y', strtotime($coupon->start_date)).' and '.date('d/m/Y', strtotime($coupon->end_date)), 'details' => 'Booking date supplied ('.date('d/m/Y').') was outside '.date('d/m/Y', strtotime($coupon->start_date)).' and '.date('d/m/Y', strtotime($coupon->end_date))]);
                }
                break;
        }

        $div = ($coupon->amount_type == 0 ? 1 : ($coupon->amount / 100.00));
        $delivery = $totalAmt - ($amtEach * $participants);
        //Time to apply the discount
        if($coupon->discount_type == 1) {
            //Whole thing.  Maybe also relevant for dollars?
            $totalAmt -=  $totalAmt * $div;
        } else {
            //Course fees only
            $amtEach -= $amtEach * $div;
            $totalAmt = $amtEach * $participants + $delivery;
        }

        return json_encode(['success' => true, 'message' => 'Your coupon has been applied successfully', 'courseAmt' => $totalAmt * 100, 'courseAmtEach' => $amtEach]);
    }
}
