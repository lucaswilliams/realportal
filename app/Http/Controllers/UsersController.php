<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Password;
use Message;
use Hash;

class UsersController extends Controller
{
    public static function GeneratePassword() {
        $sets = array();
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        $sets[] = '23456789';
        $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < 10 - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        return $password;
    }

    public function showList() {
        //We need to see our own roles here.
        $users = DB::table('users')
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('roles.role_level', '<=', $this->user->level)
            ->where('users.user_type', '=', 0)
            ->select('users.*', 'roles.name as level')
            ->get();

        $admin = $this->hasRole('Super Admin');
        return view('users.list', ['users' => $users, 'admin' => $admin]);
    }

    public function getUser($id) {
        $users = DB::table('users')
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('users.id', '=', $id)
            ->select('users.*', 'roles.name as level')
            ->get();
        if(count($users) > 0) {
            return $users[0];
        } else {
            return null;
        }
    }

    public function showForm($id = 0) {
        $user = $this->getUser($id);
        return view('users.form', ['user' => $user, 'levels' => $this->getUserLevels()]);
    }

    public function saveUser(Request $request) {
        $content = $this->convertPost($request);

        $role_id = $content['level'];
        unset($content['level']);
        if($content['id'] == '0') {
            unset($content['id']);
            $content['username'] = $content['email'];
            $user_id = DB::table('users')
                ->insertGetId($content);

            DB::table('user_roles')
                ->insert([
                    'user_id' => $user_id,
                    'role_id' => $role_id
                ]);
            $addedit = 'added';

            $credentials = ['email' => $content['email']];
            $response = Password::sendResetLink($credentials, function (Message $message) {
                $message->subject('Set your password for Real Response Portal');
            });
        } else {
            DB::table('users')
                ->where('id', '=', $content['id'])
                ->update($content);

            DB::table('user_roles')
                ->where('user_id', '=', $content['id'])
                ->update([
                    'role_id' => $role_id
                ]);
            $addedit = 'updated';
        }

        return redirect('/users')->with('success', 'The user has been '.$addedit.' successfully');
    }

    public function deleteUser($id = 0) {
        if($id > 0) {
            DB::beginTransaction();
            DB::table('user_roles')
                ->where('user_id', '=', $id)
                ->delete();

            DB::table('contacts')
                ->where('user_id', '=', $id)
                ->delete();

            DB::table('users')
                ->where('id', '=', $id)
                ->delete();
            DB::commit();
        }

        return redirect('/users')->with('success', 'The user has been deleted successfully');
    }

    public function getUserLevels() {
        return DB::table('roles')
            ->where('role_level', '<=', $this->user->level)
            ->orderBy('role_level', 'desc')
            ->get();
    }

    public function showResetPassword($id) {
        $user = DB::table('users')->where('id', '=', $id)->get();
        return view('users.editpassword', ['id' => $id, 'user' => $user[0]]);
    }

    public function saveResetPassword($id, Request $request) {
        $password = Hash::make($request->password);

        DB::table('users')
            ->where('id', '=', $id)
            ->update(['password' => $password]);
        return redirect('/users')->with('success', 'The password has been updated successfully.');
    }
}
