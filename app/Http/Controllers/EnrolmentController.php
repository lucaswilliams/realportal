<?php

namespace App\Http\Controllers;

use Faker\Provider\Payment;
use Illuminate\Http\Request;
use DB;
use Storage;

class EnrolmentController extends Controller
{
    public function showForm() {
        $stripe_pk = env('STRIPE_PK');
        return view('enrol.index', ['stripe_pk' => $stripe_pk]);
    }

    public function showCQForm() {
        $stripe_pk = env('STRIPE_PK');
        return view('enrol.cqindex', ['stripe_pk' => $stripe_pk]);
    }

    public function showBulk() {
        $organisations = DB::table('clients')
            ->get();

        return view('enrol.bulk', []);
    }

    public function uploadBulk(Request $request) {
        $storagePath = str_replace('\\storage\\app\\', '\\resources\\', Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix());

        $file = $request->file('uploadfile')->store('uploads');
        $path = storage_path('app/'.$file);

        $batch_id = time();

        $ax_url = env('AX_URL');
        $_WSTOKEN = env('WS_TOKEN');
        $_APITOKEN = env('API_TOKEN');

        require $storagePath.'SpreadsheetReader.php';

        $Reader = new \SpreadsheetReader($path);
        $Sheets = $Reader->Sheets();

        $success = true;

        foreach ($Sheets as $Index => $Name) {
            $Reader->ChangeSheet($Index);
            $i = 0;
            DB::beginTransaction();
            foreach ($Reader as $Row) {
                if($success) {
                    if ($i == 0) {
                        $i++;
                    } else {
                        //echo '<pre>'; var_dump($Row); echo '</pre>';
                        $usi = substr($Row[4], 0, 10);
                        $given = $this->FixChars($Row[0]);
                        $middle = $this->FixChars($Row[1]);
                        $surname = $this->FixChars($Row[2]);
                        $dob = $Row[3];
                        $email = $this->FixChars(trim($Row[5]));

                        //echo 'EMAIL BEFORE '; var_dump($email);

                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $email = '';
                        }

                        //echo 'EMAIL AFTER '; var_dump($email);


                        if ($this->validateDate($dob, 'm-d-y')) {
                            $day = substr($dob, 3, 2);
                            $month = substr($dob, 0, 2);
                            $year = substr($dob, 6, 2);

                            if ((int)'20' . $year > (int)date('Y')) {
                                $year = '19' . $year;
                            } else {
                                $year = '20' . $year;
                            }

                            $dob = $year . '-' . $month . '-' . $day;
                        } else {
                            $dob = null;
                        }

                        $insData = [
                            'batch_id' => $batch_id,
                            'given_name' => $given,
                            'middle_name' => $middle,
                            'surname' => $surname,
                            'date_of_birth' => $dob,
                            'email' => $email,
                            'usi' => $usi,
                            'axid' => $request->organisation
                        ];

                        if (strlen($given) > 0) {
                            //put them into a batch table
                            try {
                                DB::table('upload_batches')
                                    ->insert($insData);
                            } catch (\Throwable $ex) {
                                DB::rollback();
                                $error = 'This row was not imported, there was an issue:<br>' . $ex->getMessage();
                                $error .= '<pre>';
                                $error .= print_r($insData, true);
                                $error .= '</pre>';
                                $success = false;
                            }
                        }
                    }
                }
            }
        }

        if($success) {
            DB::commit();
            return redirect('/enrol/corporate/'.$batch_id);
        } else {
            return view('errors.user', ['content' => $error.'<a href="' . url('/enrol/corporate/') . '" class="btn btn-primary">Back to upload</a>']);
        }
    }

    function validateDate($date, $format = 'Y-m-d'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public function validateBulk($id) {
        //Get all of the people from this batch, run through aXcelerate if they are not validated and go nuts
        $contacts = DB::table('upload_batches')
            ->leftJoin('ax_ids', 'ax_ids.axid', '=', 'upload_batches.axid')
            ->where('batch_id', '=', $id)
            ->get();

        $ax_url = env('AX_URL');
        $_WSTOKEN = env('WS_TOKEN');
        $_APITOKEN = env('API_TOKEN');

        $contactList = [];
        $validated = 0;

        foreach($contacts as $contact) {
            $given = $contact->given_name;
            $middle = $contact->middle_name;
            $surname = $contact->surname;
            $dob = $contact->date_of_birth;
            $email = $contact->email;
            $usi = $contact->usi;
            $contact->message = '';

            if ($contact->validated == 0) {
                if(strlen($given) > 0 && strlen($surname) > 0 && strlen($email)) {
                    //Can we find them and update their USI and DOB?
                    $service_url = $ax_url . 'contacts/search?givenName=' . urlencode($given) . '&surname=' . urlencode($surname).'&emailAddress='.urlencode($email);
                    $headers = array(
                        'wstoken: ' . $_WSTOKEN,
                        'apitoken: ' . $_APITOKEN
                    );

                    $curl = curl_init($service_url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    if (env('proxy')) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }

                    $curl_response = curl_exec($curl);
                    $data = json_decode($curl_response);
                    //var_dump($data); die();
                    //Look to see if they have the organisation
                    $contact_id = 0;
                    if(is_object($data) && property_exists($data, 'ERROR') && $data->ERROR) {
                        DB::table('upload_batches')
                            ->where('id', '=', $contact->id)
                            ->update([
                                'field_error' => $data->FIELDNAMES,
                                'error_message' => $data->DETAILS
                            ]);

                        $contact->message = $data->DETAILS;
                    } else {
                        foreach ($data as $item) {
                            if ($item->ORGANISATION == $contact->name) {
                                $contact_id = $item->CONTACTID;
                                DB::table('upload_batches')
                                    ->where('id', '=', $contact->id)
                                    ->update(['contact_id' => $contact_id]);
                            }
                        }

                        //echo $contact_id.'<br >';
                        if ($contact_id > 0) {
                            //Update the USI
                            $data = array();
                            if (strlen($usi) > 0) {
                                $data["USI"] = $usi;
                                $data["usiExemption"] = 'false';
                            }
                            if (strlen($dob) > 0) {
                                $data["dob"] = $dob;
                            }

                            $data['emailAddress'] = $email;

                            //var_dump($data);
                            $service_url = $ax_url . 'contact/' . $contact_id;
                            //echo $service_url.'<br>';
                            $curl = curl_init($service_url);
                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                            if (env('proxy')) {
                                curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                            }

                            $response = curl_exec($curl);
                            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                            //var_dump($httpcode);
                            if (substr($httpcode, 0, 1) == "4") {
                                $data = json_decode($response);
                                $contact->message = $data->DETAILS;
                            } else {
                                DB::table('upload_batches')
                                    ->where('id', '=', $contact->id)
                                    ->update(['contact_id' => $contact_id, 'validated' => 1]);
                                $contact->message = 'Validated';
                                $contact->validated = 1;
                                $validated++;
                            }
                        } else {
                            if (strlen($email) > 0) {
                                $service_url = $ax_url . 'contact';

                                $headers = array(
                                    'wstoken: ' . $_WSTOKEN,
                                    'apitoken: ' . $_APITOKEN
                                );

                                $params = array(
                                    'givenName' => $given,
                                    'surname' => $surname,
                                    'organisation' => $contact->name
                                );

                                $params['emailAddress'] = $email;

                                if (strlen($dob) > 0) {
                                    $params['dob'] = $dob;
                                }

                                if (strlen($middle) > 0) {
                                    $params['middleName'] = $middle;
                                }

                                if (strlen($usi) > 0) {
                                    $params['USI'] = $usi;
                                }

                                $fieldsstring = '';
                                foreach ($params as $key => $value) {
                                    $fieldsstring .= $key . '=' . $value . '&';
                                }
                                rtrim($fieldsstring, '&');

                                $curl = curl_init($service_url);
                                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($curl, CURLOPT_POST, true);
                                curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                                if (env('proxy')) {
                                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                                }


                                $curl_response = curl_exec($curl);
                                $data = json_decode($curl_response);
                                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                                if (substr($httpcode, 0, 1) == "4") {
                                    $contact->message = $data->DETAILS;
                                } else {
                                    $contact_id = $data->CONTACTID;
                                    DB::table('upload_batches')
                                        ->where('id', '=', $contact->id)
                                        ->update(['contact_id' => $contact_id, 'validated' => 1]);
                                    $contact->message = 'Validated';
                                    $contact->validated = 1;
                                    $validated++;
                                }
                            }
                        }
                    }
                } else {
                    //They don't have an email address, provide ability to add that in.
                    if(strlen($given) == 0) {
                        $contact->message = 'No given name';
                    }

                    if(strlen($surname) == 0) {
                        $contact->message = 'No surname';
                    }

                    if(strlen($email) == 0) {
                        $contact->message = 'No email';
                    }
                }
            } else {
                //probably validated.
                $contact->message = ($contact->validated == 1 ? 'Validated' : 'Enrolled');
                $validated++;
            }

            $contactList[] = $contact;
        }

        return view('enrol.list', ['contacts' => $contactList, 'validated' => $validated, 'batch' => $id]);
    }

    public function saveUpdates(Request $request) {
        $content = $this->convertPost($request);

        foreach($content as $id => $data) {
            DB::table('upload_batches')
                ->where('id', '=', $id)
                ->update($data);
        }

        return redirect($_SERVER['REQUEST_URI']);
    }

    public function showList() {
        $batches = DB::select(DB::raw('SELECT batch_id, COUNT(id) as enrolments FROM upload_batches GROUP BY batch_id ORDER BY batch_id'));

        return view('enrol.batchlist', ['batches' => $batches]);
    }
}
