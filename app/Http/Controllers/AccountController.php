<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index() {
        return view('account.form', ['user' => $this->user]);
    }

    public function save(Request $request) {
        $data = $this->convertPost($request);

        DB::table('users')
            ->where('id', '=', $this->user->id)
            ->update($data);

        return redirect('/account');
    }

    public function changePassword() {
        return view('account.password', ['user' => $this->user]);
    }

    public function savePassword(Request $request) {
        if (Hash::check($request->old_password, $this->user->password)) {
            if (strcmp($request->password, $request->password_verify) == 0) {
                DB::table('users')
                    ->where('id', '=', $this->user->id)
                    ->update([
                        'password' => Hash::make($request->password)
                    ]);
                return redirect('/account')->with('success', 'Your password has changed successfully');
            } else {
                return redirect('/account/password')->with('error', 'The new passwords don\'t match');
            }
        } else {
            return redirect('/account/password')->with('error', 'Your current password is incorrect');
        }
    }

    public function changeEmail(Request $request) {
        if(strcmp($request->email, $request->email_verify) == 0) {
            //send a verification email, much like the forgotten password one.

        } else {
            return redirect('/account/email')->with('error', 'The new email addresses don\'t match');
        }
    }

    public function saveEmail(Request $request) {
        DB::table('users')
            ->where('id', '=', $this->user->id)
            ->update($data);

        return redirect('/account');
    }

    public function updateEmail($verify) {
        echo 'update '.$verify;
    }
}
