<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use DB;

class  Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $user;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            if(is_object($this->user)) {
                $level = DB::table('roles')
                    ->join('user_roles', 'user_roles.role_id', '=', 'roles.id')
                    ->where('user_roles.user_id', '=', $this->user->id)
                    ->max('roles.role_level');

                $this->user->level = $level;
            }

            return $next($request);
        });

        /*if($this->user == null) {
            $this->user = Auth::user();
            $level = DB::table('roles')
                ->join('user_roles', 'user_roles.role_id', '=', 'roles.id')
                ->where('user_roles.user_id', '=', $this->user->id)
                ->max('roles.role_level');

            $this->user->level = $level;
        }*/
    }

    public function convertPost($data) {
        if(is_array($data)) {
            //echo 'is array<br>';
            unset($data['_token']);
            $requestData = [];
            foreach($data as $key=>$value) {
                if(strpos($key, ' ') === false) {
                    $requestData = $this->convertPostArray($key, $value, $requestData);
                }
            }
        } else {
            //echo 'is params<br>';
            $params = explode('&', $data);
            //echo '<pre>'; var_dump($params); echo '</pre>';
            $requestData = [];
            foreach($params as $param) {
                $pair = explode('=', $param);
                if(isset($pair[0]) && isset($pair[1])) {
                    $key = $pair[0];
                    $value = $pair[1];
                    if ($key != '_token' &&
                        strpos($key, ' ') === false &&
                        strpos($key, 'utm_') === false) {
                        $requestData = $this->convertPostArray($key, $value, $requestData);
                    }
                }
            }
        }

        //echo '<pre>'; var_dump($requestData); echo '</pre>';

        if(count($requestData) == 0) {
            //check postdata
            //echo 'is post';
            $requestData = $_POST;
            if(isset($requestData['_token'])) {
                unset($requestData['_token']);
            }
        }

        return $requestData;
    }

    private function convertPostArray($key, $value, $requestData) {
        if(strpos($key, '%5B') !== false) {
            $parts = explode('%5B', str_replace('%5D', '', $key));
            //var_dump($parts);
            if(strlen($parts[1]) > 0) {
                $requestData[$parts[1]][$parts[0]] = urldecode($value);
            } else {
                $requestData[$parts[0]][] = urldecode($value);
            }
        } else {
            $requestData[$key] = urldecode($value);
        }

        return $requestData;
    }

    public function hasRole($role_name) {
        return (DB::table('roles')
            ->join('user_roles', 'user_roles.role_id', '=', 'roles.id')
            ->where('user_roles.user_id', '=', $this->user->id)
            ->where(function($where) use($role_name) {
                $where->where('roles.name', '=', $role_name)
                    ->orWhere('roles.name', '=', 'Super Admin');
            })
            ->count() > 0);
    }

    public function FixChars($text)
    {
        try {
            // First, replace UTF-8 characters.
            $text = str_replace( array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6", "\xe2\x80\xB0", "\xc2\xB0", "\xe2\x80\xF7", "\xe2\x80\xBC", "\xe2\x80\xBD", "\xe2\x80\x2b", "\xe2\x80\xB7", "\xc2\B7", "\xc2\A0", "\xAD\x09", "\xC3", "\xEF\xBF","\xE2--\x8F", "\xBA", "\xCA", "\xEF\x82", "\xAE", "\xA9", "\xF4\x8F", "\xE2", "\x8F", "\xAF", "\xF0\x9F", "\x8D\x0A", "\x83\x0A"), array("'", "'", '"', '"', '-', '--', '...', '&deg;', '&deg;', '&divide;', '&frac14;', '&frac12;', '&#43;', '&middot;', '&middot;', '', '', '', '','','','', '', ' ', '&copy;', '', '','', ' ', '', '',''), $text);
            // Next, replace their Windows-1252 equivalents.
            $text = str_replace( array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133), chr(176), chr(247), chr(188), chr(189), chr(43), chr(183), chr(194)),  array("'", "'", '"', '"', '-', '--', '...', '&deg;', '&divide;', '&frac14;', '&frac12;', '&#43;', '&middot;', ''), $text);


            $text = str_replace("\xA0", " ", $text);

            $text = str_replace("\xc2\xa0", " ", $text);
            $text = str_replace("\xe2\x80\xA2", "&bull;", $text);

            $text = str_replace("\x80\xA2\x09", "&bull;", $text);

            $text = str_replace("\x80\xA2", "&bull;", $text);
            $text = str_replace("\xEF\xAC\x81b", " ", $text);

            $text = str_replace("\xA7\x09", "", $text);
            $text = str_replace("\x81\x842", "", $text);
            $text = str_replace("\xA7", "&sect;", $text);
            $text = str_replace("\x09", "", $text);

            $text = str_replace("\xAA", "", $text);
            $text = str_replace("\xAA", "", $text);

            $text = str_replace("\x80", "", $text);
            $text = str_replace("\x81", "", $text);
            $text = str_replace("\x8B", "-", $text);
            //$text = str_replace("'", "''", trim($text));

            $sRV = $text;

        } catch (\Exception $e) {
            $sRV = str_replace("'", "''", trim($text));
        }
        return $sRV;
    }
}
