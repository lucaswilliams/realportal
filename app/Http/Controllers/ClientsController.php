<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;

class ClientsController extends Controller
{
    public function getClients() {
        $clients = DB::table('clients')
            ->leftJoin('contacts', function($join) {
                $join->on('contacts.client_id', '=', 'clients.id')
                    ->on('contacts.is_primary', '=', DB::raw('1'));
            })
            ->leftJoin('users', 'users.id', '=', 'contacts.user_id')
            ->select('clients.*', 'users.first_name', 'users.last_name')
            ->get();
        return $clients;
    }

    public function getClient($id) {
        $clients = DB::table('clients')
            ->leftJoin('ax_ids', 'ax_ids.axid', '=', 'clients.axid')
            ->where('id', '=', $id)
            ->select('clients.*', 'ax_ids.name as ax_name')
            ->get();

        if(count($clients) > 0) {
            $client = $clients[0];
        } else {
            $client = new \stdClass();
            $client->name = null;
            $client->address_1 = null;
            $client->address_2 = null;
            $client->suburb = null;
            $client->state = null;
            $client->postcode = null;
            $client->axid = null;
            $client->ax_name = null;
            $client->xero_guid = null;
            $client->xero_name = null;
            $client->logo = null;
        }

        return $client;
    }

    public function index() {
        $clients = $this->getClients();
        return view('clients.list', ['clients' => $clients]);
    }

    public function showForm($id = 0) {
        $client = $this->getClient($id);
        return view('clients.form', ['client' => $client]);
    }

    public function save(Request $request) {
        $content = $this->convertPost($request);

        //Save the image in storage and put the path to it in the database.
        if(!isset($content['logo'])) {
            //Probably a file upload?
            $file = $request->file('logo');
            if($file != null) {
                $logo = $file->store('public/logos');
                $content['logo'] = str_replace('public/logos/', '/storage/logos/', $logo);
            } else {
                $content['logo'] = '';
            }
        }

        if(isset($content['axid'])) {
            $content['axid'] = intval($content['axid']);
        }

        if($content['id'] == '0') {
            unset($content['id']);
            DB::table('clients')
                ->insert($content);
            $addedit = 'added';
        } else {
            DB::table('clients')
                ->where('id', '=', $content['id'])
                ->update($content);
            $addedit = 'updated';
        }

        return redirect('/clients')->with('success', 'The client has been '.$addedit.' successfully');
    }

    public function getContacts($client_id) {
        $contacts = DB::table('users')
            ->join('contacts', 'contacts.user_id', '=', 'users.id')
            ->where('contacts.client_id', '=', $client_id)
            ->get();

        return $contacts;
    }

    public function getContact($id) {
        $contacts = DB::table('users')
            ->join('contacts', 'contacts.user_id', '=', 'users.id')
            ->where('contacts.id', '=', $id)
            ->get();
        if(count($contacts) > 0) {
            return $contacts[0];
        } else {
            return null;
        }
    }

    public function showContacts($id) {
        $contacts = $this->getContacts($id);

        return view('clients.contacts.list', ['contacts' => $contacts]);
    }

    public function showContactForm($id, $contact_id = 0) {
        $contact = $this->getContact($contact_id);
        return view('clients.contacts.form', ['contact' => $contact, 'client' => $id]);
    }

    public function saveContact(Request $request) {
        $content = $this->convertPost($request);
        $client_id = $content['client_id'];
        unset($content['client_id']);

        if($content['id'] == '0') {
            unset($content['id']);
            $user_id = DB::table('users')
                ->insertGetId($content);
            $addedit = 'added';

            //Is this the only one?  Make primary
            $contacts = DB::table('contacts')
                ->where('client_id', '=', $client_id)
                ->count();

            $contactArr = [
                'user_id' => $user_id,
                'client_id' => $client_id
            ];

            if($contacts == 0) {
                $contactArr['is_primary'] = 1;
            }

            DB::table('contacts')
                ->insert($contactArr);
        } else {
            DB::table('clients')
                ->where('id', '=', $content['id'])
                ->update($content);
            $addedit = 'updated';
        }
        return redirect('/clients/'.$client_id.'/contacts')->with('success', 'The contact has been '.$addedit.' successfully');
    }

    public function deleteContact($id, $contact_id) {
        DB::table('contacts')->where('id', $contact_id)->where('client_id', $id)->delete();
        return redirect('/clients/'.$id.'/contacts');
    }

    public function checkDelete($id) {
        //Do they have contacts, courses, bookings or anything else?
        //Is there a way to get incoming FKs and just loop them?
        $client = $this->getClient($id);
        $contacts = $this->getContacts($id);
        $urls = DB::table('booking_urls')->where('client_id', $id)->get();
        $message = '';

        if(count($contacts) > 0) {
            $message .= 'You cannot delete '.$client->name.' as they have contact people<br>';
        }

        if(count($urls) > 0) {
            $message .= 'You cannot delete '.$client->name.' as they have booking URLs<br>';
        }

        if(strlen($message) > 0) {
            return view('clients.delete', ['message' => $message]);
        }

        DB::table('clients')->where('id', $id)->delete();

        return redirect('/clients');
    }
}
