<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EmailTemplateController extends Controller
{
    private function getQuery() {
        $q = DB::table('email_templates');

        return $q;
    }

    public function getList() {
        $items = $this->getQuery()->get();
        return $items;
    }

    public function getURLList() {
        $items = $this->getQuery()->where('url_confirmation', 1)->get();
        return $items;
    }

    public function showList() {
        $items = $this->getList(true);

        return view('emailtemplates.list', ['items' => $items]);
    }

    public function getOne($id) {
        $query = $this->getQuery()->where('id', $id)->get();
        return $query[0] ?? null;
    }

    public function showForm($id = 0) {
        if($id > 0) {
            $item = $this->getOne($id);
        } else {
            $item = new \stdClass();
            $item->id = 0;
            $item->name = '';
            $item->content = '';
            $item->single_participant = 0;
            $item->multi_participant = 0;
            $item->url_confirmation = 0;
        }

        return view('emailtemplates.form', ['item' => $item]);
    }

    public function saveForm(Request $request) {
        $id = $request->id;
        $data = [
            'name' => $request->name,
            'content' => $request->content
        ];

        if($id > 0) {
            DB::table('email_templates')
                ->where('id', $id)
                ->update($data);
        } else {
            //Insert
            $id = DB::table('email_templates')
                ->insertGetId($data);
        }
        return redirect('/emailtemplates');
    }

    public function delete($id) {
        DB::table('email_templates')
            ->where('id', $id)
            ->delete();
        return redirect('/emailtemplates');
    }

    public function showUsage() {
        $templates = $this->getQuery()->get();

        return view('emailtemplates.usage', ['templates' => $templates]);
    }

    public function saveUsage(Request $request) {
        $content = $this->convertPost($request);
        echo '<pre>'; var_dump($content); echo '</pre>';
        foreach($content as $id => $data) {
            DB::table('email_templates')
                ->where('id', $id)
                ->update([
                    'single_participant' => (isset($data['single_participant']) ? 1 : 0),
                    'multi_participant' => (isset($data['multi_participant']) ? 1 : 0),
                    'url_confirmation' => (isset($data['url_confirmation']) ? 1 : 0),
                    'invoice_email' => (isset($data['invoice_email']) ? 1 : 0),
                    'cq_invoice_email' => (isset($data['cq_invoice_email']) ? 1 : 0),
                ]);
        }

        return redirect('/emailtemplatesusage')->with('success', 'Template usage updated successfully');
    }
}

