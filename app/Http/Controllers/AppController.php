<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class AppController extends Controller
{
    public function listFiles() {
        $files = DB::table('media_files')
            ->orderBy('type', 'desc')
            ->orderBy('name')
            ->get();

        return view('app.files.list', ['files' => $files]);
    }

    public function getFile($id) {
        $files = DB::table('media_files')
            ->where('id', '=', $id)
            ->get();

        if(count($files) > 0) {
            $file = $files[0];
        } else {
            $file = new \stdClass();
            $file->name = null;
            $file->file = null;
            $file->artist = null;
            $file->type = null;
            $file->active = 1;
        }

        return $file;
    }

    public function showFileForm($id = 0) {
        $file = $this->getFile($id);
        return view('app.files.form', ['file' => $file]);
    }

    public function saveFile(Request $request) {
        $content = $this->convertPost($request);

        if(isset($content['id'])) {
            $id = intval($content['id']);
            unset($content['id']);
        } else {
            $id = 0;
        }

        if($id == 0) {
            $content['file'] = '';
            $content['art'] = '';

            $id = DB::table('media_files')
                ->insertGetId($content);
            $addedit = 'added';
        } else {
            DB::table('media_files')
                ->where('id', '=', $id)
                ->update($content);
            $addedit = 'updated';
        }

        //Is there an uploaded media file?
        $f = $request->file('file');
        if($f) {
            $file = $f->store('public/media');

            DB::table('media_files')
                ->where('id', $id)
                ->update(['file' => str_replace('public/media/', '', $file)]);
        }

        //Is there an uploaded album art file?
        $f = $request->file('art');
        if($f) {
            $file = $f->store('public/art');

            DB::table('media_files')
                ->where('id', $id)
                ->update(['art' => str_replace('public/art/', '', $file)]);
        }

        return redirect('/app/files')->with('success', 'The file has been '.$addedit.' successfully');
    }

    public function deleteFile($id) {
        $file = DB::table('media_files')
            ->where('id', $id)
            ->select('file')
            ->get();

        $filepath = storage_path('app/public/media').'/'.$file[0]->file;
        if(strlen($file[0]->file) > 0 && file_exists($filepath)) {
            unlink($filepath);
        }

        DB::table('playlist_files')
            ->where('media_id', $id)
            ->delete();

        DB::table('media_files')
            ->where('id', $id)
            ->delete();

        return redirect('/app/files')->with('success', 'The file has been deleted successfully');
    }

    /* PLAYLISTS */
    public function listPlaylists() {
        $playlists = DB::table('playlists')
            ->get();

        return view('app.playlists.list', ['playlists' => $playlists]);
    }

    public function getPlaylist($id) {
        $playlists = DB::table('playlists')
            ->where('id', '=', $id)
            ->get();

        if(count($playlists) > 0) {
            $playlist = $playlists[0];
        } else {
            $playlist = new \stdClass();
            $playlist->name = null;
            $playlist->artist = null;
            $playlist->active = 1;
        }

        return $playlist;
    }

    public function showPlaylistForm($id = 0) {
        $playlist = $this->getPlaylist($id);
        return view('app.playlists.form', ['playlist' => $playlist]);
    }

    public function savePlaylist(Request $request) {
        $content = $this->convertPost($request);

        if($content['id'] == '0') {
            unset($content['id']);
            $id = DB::table('playlists')
                ->insertGetId($content);
            $addedit = 'added';
        } else {
            $id = $content['id'];
            DB::table('playlists')
                ->where('id', '=', $id)
                ->update($content);
            $addedit = 'updated';
        }
        return redirect('/app/playlists')->with('success', 'The playlist has been '.$addedit.' successfully');
    }

    /* API stuff */
    public function getMedia() {
        $fileQ = DB::table('media_files')
            ->where('active', 1)
            ->select('id', 'name', 'artist', 'type')
            ->get();

        $files = [];
        $media = [];
        $sounds = [];
        $sirens = [];
        $clinical = [];
        foreach($fileQ as $file) {
            $files[] = $file;
            switch($file->type) {
                case 0:
                    $media[] = $file;
                    break;

                case 1:
                    $sounds[] = $file;
                    break;

                case 2:
                    $sirens[] = $file;
                    break;

                case 3:
                    $clinical[] = $file;
                    break;
            }
        }

        $playlistQ = DB::table('playlists')
            ->where('active', 1)
            ->select('id', 'name', 'artist')
            ->get();

        $playlists = [];
        foreach($playlistQ as $playlist) {
            $trackQ = DB::table('playlist_files')
                ->join('media_files', 'media_files.id', '=', 'playlist_files.media_id')
                ->where('media_files.active', 1)
                ->where('playlist_files.playlist_id', $playlist->id)
                ->select('media_files.id', 'media_files.name', 'media_files.artist');

            $tracks = $trackQ->get();
            $filelist = [];
            foreach($tracks as $track) {
                $filelist[] = $track;
            }
            $playlist->files = $filelist;
            $playlists[] = $playlist;
        }

        return json_encode([
            'files' => $files,
            'playlists' => $playlists,
            'media' => $media,
            'sounds' => $sounds,
            'sirens' => $sirens,
            'clinical' => $clinical
        ]);
    }

    public function downloadMedia($id) {
        $file = DB::table('media_files')
            ->where('id', $id)
            ->select('file')
            ->get();

        if(count($file) > 0 && strlen($file[0]->file) > 0) {
            return env('APP_URL').'/storage/media/' . $file[0]->file;
        }

        return null;
    }

    public function downloadArt($id) {
        $file = DB::table('media_files')
            ->where('id', $id)
            ->select('art')
            ->get();

        if(count($file) > 0 && strlen($file[0]->art) > 0) {
            return env('APP_URL').'/storage/art/' . $file[0]->art;
        }

        return null;
    }
}
