<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AxcelerateController extends Controller
{
    public function getOrganisations() {
        $org = $_GET['term'];

        $_WSTOKEN = env('WS_TOKEN');
        $_APITOKEN = env('API_TOKEN');

        $service_url = env('AX_URL').'/organisations?displayLength=50&name='.urlencode($org);
        $headers = array(
            'wstoken: ' . $_WSTOKEN,
            'apitoken: ' . $_APITOKEN
        );

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $curl_response = curl_exec($curl);
        $data = json_decode($curl_response);

        $results = array();
        foreach($data as $item) {
            $results[] = array(
                'id' => $item->ORGID,
                'text' => $item->NAME
            );

            DB::insert(DB::raw('INSERT INTO ax_ids (axid, name) SELECT '.$item->ORGID.', \''.str_replace('\'', '\'\'', $item->NAME).'\' FROM DUAL WHERE NOT EXISTS (SELECT * FROM ax_ids WHERE axid = '.$item->ORGID.')'));
        }

        header('HTTP/1.1 200 OK');
        echo json_encode($results);
    }

    public function getCourseDetails($instanceId) {
        $service_url = env('AX_URL') . 'course/instance/search';

        $headers = array(
            'WSToken: ' . env('WS_TOKEN'),
            'APIToken: ' . env('API_TOKEN')
        );

        $params = array(
            'InstanceID' => $instanceId,
            'type' => 'all'
        );

        $fieldsstring = http_build_query($params);

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
        if (env('PROXY')) {
            curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        }


        $curl_response = curl_exec($curl);
        $data = json_decode($curl_response);

        echo json_encode($data[0]);
    }

    public function enrolPeople() {
        $batch = $_POST['batch'];
        $instance = $_POST['course_id'];

        //People to enrol
        $people = DB::table('upload_batches')
            ->where('batch_id', '=', $batch)
            ->where('validated', '=', 1)
            ->get();

        foreach($people as $person) {
            $service_url = env('AX_URL') . 'course/enrol';

            $headers = array(
                'WSToken: ' . env('WS_TOKEN'),
                'APIToken: ' . env('API_TOKEN')
            );

            $params = array(
                'contactId' => $person->contact_id,
                'instanceId' => $instance,
                'type' => 'w',
                'generateInvoice' => false,
                /*'cost' => $pricePer,
                'discountIDList' => '1528',*/
                'suppressNotifications' => 'true'
            );

            //echo '<pre>'; var_dump($params); echo '</pre>';

            $fieldsstring = http_build_query($params);

            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
            if (env('PROXY')) {
                curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            }


            $curl_response = curl_exec($curl);
            $data = json_decode($curl_response);

            DB::table('upload_batches')
                ->where('id', '=', $person->id)
                ->update(['validated' => 2]);
        }

        echo json_encode(['success' => true]);
    }
}
