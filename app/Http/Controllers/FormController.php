<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest;
use DB;
use Storage;
use Log;

class FormController extends Controller
{
    /**
     * Get the states and locations from Salesforce in a tiered structure
     * @return void
     */
    public function getLocations() {
        Forrest::authenticate();
        $sql = "SELECT Id, Name, BillingStreet, BillingState, BillingCity, BillingPostalcode, BillingCountry, Pickup_Message__c, Drop_off_Message__c, ParentId, Hosted_State_Image_URL__c, (SELECT Id, Name FROM ChildAccounts), (SELECT Id, Name, Delivery_Mode__c, rio_ed__Fee_Type__c, rio_ed__Current_Fee_Domestic__c, rio_ed__Parent_Fee__c, Maximum_Attendees__c, Flat_Rate__c FROM Fees__r) FROM Account WHERE RecordType.Name = 'Location' AND ParentId = '' ORDER BY Order_Display__c, Name";
        //Log::debug($sql);
        $res = Forrest::query($sql);
        //Log::debug($res);

        $locations = [];
        $states = [];
        $suburbs = [];
        foreach($res['records'] as $location) {
            //echo '<pre>'; var_dump($location); echo '</pre>';
            try {
                $newState = new \stdClass();
                $newState->id = $location['Id'];
                $newState->title = $location['Name'];
                $newState->pickup = $location['Pickup_Message__c'];
                $newState->delivery = $location['Drop_off_Message__c'];
                $newState->image = $location['Hosted_State_Image_URL__c'];
                //$newState->fee = $location['Delivery_Fee_by_City__c'];

                $states[] = $newState;

                //echo $location['Name'].' has '.count($location['ChildAccounts']['records'])." children.\n";

                if(isset($location['ChildAccounts']) && count($location['ChildAccounts']['records']) > 0) {
                    //Get the locations for this parent.
                    $locres = Forrest::query("SELECT Id, Name, BillingStreet, BillingState, BillingCity, BillingPostalcode, BillingCountry, Pickup_Message__c, Drop_off_Message__c, ParentId, Hosted_State_Image_URL__c, (SELECT Id, Name FROM ChildAccounts), (SELECT Id, Name, Delivery_Mode__c, rio_ed__Fee_Type__c, rio_ed__Current_Fee_Domestic__c, rio_ed__Parent_Fee__c, Maximum_Attendees__c, Flat_Rate__c FROM Fees__r), Delivery_Days__c, Delivery_Fee_by_City__c, Available_for_Equipment_Pickup__c FROM Account WHERE RecordType.Name = 'Location' AND ParentId = '".$location['Id']."' ORDER BY Order_Display__c, Name");
                    //foreach of them, get their children as the delivery locations.
                    foreach($locres['records'] as $region) {
                        $newState = new \stdClass();
                        $newState->id = $region['Id'];
                        $newState->title = $region['Name'];
                        $newState->pickupmessage = $region['Pickup_Message__c'];
                        $newState->dropoffmessage = $region['Drop_off_Message__c'];
                        $newState->fee = $region['Delivery_Fee_by_City__c'];
                        $newState->days = $region['Delivery_Days__c'];
                        $newState->state = $location['Id'];
                        $newState->pickup = $region['Available_for_Equipment_Pickup__c'];
                        $newState->pickupAddress = $region['BillingStreet'].'<br>'.$region['BillingCity'].' '.$region['BillingState'];
                        if(isset($region['ChildAccounts']) && count($region['ChildAccounts']['records']) > 0) {
                            $dels = [];
                            $subres = Forrest::query("SELECT Id, Name, BillingStreet, BillingState, BillingCity, BillingPostalcode, BillingCountry, Pickup_Message__c, Drop_off_Message__c, ParentId, Hosted_State_Image_URL__c, (SELECT Id, Name FROM ChildAccounts), (SELECT Id, Name, Delivery_Mode__c, rio_ed__Fee_Type__c, rio_ed__Current_Fee_Domestic__c, rio_ed__Parent_Fee__c, Maximum_Attendees__c, Flat_Rate__c FROM Fees__r) FROM Account WHERE RecordType.Name = 'Location' AND ParentId = '".$region['Id']."' ORDER BY Order_Display__c, Name");
                            foreach($subres['records'] as $suburb) {
                                $dels[] = $suburb['Id'];

                                $newRegion = new \stdClass();
                                $newRegion->id = $suburb['Id'];
                                $newRegion->title = $suburb['Name'];
                                $newRegion->parent = $region['Id'];
                                $suburbs = $newRegion;
                            }

                            $newState->children = join(',', $dels);
                        }

                        $locations[] = $newState;
                    }
                }
            } catch(Exception $ex) {

            }
        }

        $r = new \stdClass();
        $r->states = $states;
        $r->locations = $locations;
        $r->suburbs = $suburbs;

        //echo '<pre>'; var_dump($r); echo '</pre>';
        return json_encode($r);
    }

    /**
     * Get the courses, instances and delivery locations from Salesforce
     * @param string $locations an array of Location IDs
     * @param $delivery Either "Public" or "Public_Virtual"
     * @return \stdClass
     */
    public function getCourses($locations = '') {
        $sql = "SELECT Id, rio_ed__Description__c, Name, Delivery_Mode__c, Template__c, rio_ed__Start_Date__c,
            Start_Date__c, End_Date__c, Overall_Start_Time__c, Overall_End_Time__c,
            Pricing__c, Available_Partial_Enrolment__c, rio_ed__Available_Places__c,
            Blended_Delivery__c, Blended_Delivery_Description__c,
            Equipment_Collection_Options__c, Location__r.Id, Location__r.Name,
            Location__r.BillingCountry, Location__r.BillingStreet, Location__r.BillingCity,
            Location__r.BillingState, Location__r.BillingPostalcode, (
                SELECT Id, rio_ed__Start_Date__c FROM rio_ed__Session_Times__r
            )
            FROM rio_ed__Session__c
            WHERE Visible__c = true ".
            "AND rio_ed__Start_Date__c > ".date('Y-m-d')." ".
            "AND rio_ed__Inactive__c = false ";

        /*if(strlen($locations) > 0) {
            $sql .= "AND (
                    (
                        Delivery_Mode__c = 'Public' AND
                        Location__r.Id in ('".str_replace(",", "','", $locations)."')
                    )
                    OR
                    Delivery_Mode__c = 'Public Virtual'
                )
                ";
        } else {
            $sql .= "AND Delivery_Mode__c in ('Public', 'Public_Virtual') ";
        }*/
        $sql .= "AND Delivery_Mode__c in ('Public', 'Public_Virtual') ";
        $locs = explode(',', $locations);

        $sqlOnsite = "SELECT Id, Onsite_Session_Template__r.Id, Onsite_Session_Template__r.rio_ed__Description__c, Onsite_Session_Template__r.Duration__c,
                      Fee__r.Delivery_Mode__c, Fee__r.rio_ed__Fee_Type__c, Fee__r.Fee_Amount__c, Fee__r.Maximum_Attendees__c, Fee__r.Flat_Rate__c, Fee__r.Location__c
                      FROM Course_Variation__c
                      WHERE Fee__r.Delivery_Mode__c = 'Onsite'
                      AND Inactive__c = false
                      AND Publish_On_Website__c = true";

        Forrest::authenticate();
        //Log::debug($sql);
        $res = Forrest::query($sql);
        $resOnsite = Forrest::query($sqlOnsite);
        //Log::debug($res);
        //Log::debug($resOnsite);


        $courses = [];
        $courseIds = [];
        $sessions = [];
        $types = [];
        $locations = [];
        $deliveries = [];
        foreach($res['records'] as $course) {
            if($course['Delivery_Mode__c'] == 'Public_Virtual' || in_array($course['Location__r']['Id'], $locs)) {
                //echo '<pre>'; var_dump($course); echo '</pre>';
                if(isset($course['Onsite_Session_Template__r'])) {
                    $template = $course['Onsite_Session_Template__r']['Id'];
                } else {
                    $template = $course['Template__c'];
                }
                try {
                    $newSession = new \stdClass();
                    $newSession->id = $course['Id'];
                    $newSession->template = $template;
                    $newSession->desc = $course['rio_ed__Description__c'];
                    $newSession->title = $course['Name'];
                    $newSession->delivery = $course['Delivery_Mode__c'];
                    $newSession->startdate = $course['rio_ed__Start_Date__c'];
                    $newSession->starttime = $course['Overall_Start_Time__c'];
                    $newSession->enddate = $course['End_Date__c'];
                    $newSession->endtime = $course['Overall_End_Time__c'];
                    $newSession->price = $course['Pricing__c'];
                    $newSession->title = $course['Available_Partial_Enrolment__c'];
                    $newSession->places = $course['rio_ed__Available_Places__c'];
                    $newSession->blended = $course['Blended_Delivery__c'];
                    $newSession->blendeddesc = $course['Blended_Delivery_Description__c'];
                    $newSession->locationid = $course['Delivery_Mode__c'] == 'Public_Virtual' ? 'online' : $course['Location__r']['Id'];
                    $newSession->locationname = $course['Delivery_Mode__c'] == 'Public_Virtual' ? 'Online' : $course['Location__r']['Name'];
                    $newSession->address = $course['Delivery_Mode__c'] == 'Public_Virtual' ? 'Your chosen online location' : $course['Location__r']['BillingStreet'] . ' ' . $course['Location__r']['BillingCity'] . ' ' . $course['Location__r']['BillingState'] . ' ' . $course['Location__r']['BillingPostalCode'];

                    if(!in_array($template, $courseIds)) {
                        $courseIds[] = $template;
                    }

                    if(!isset($deliveries[$template])) {
                        $deliveries[$template] = [
                            'public' => 0,
                            'online' => 0
                        ];
                    }

                    if ($newSession->delivery == 'Public') {
                        $deliveries[$template]['public'] = 1;
                    }

                    if ($newSession->delivery == 'Public_Virtual') {
                        $deliveries[$template]['online'] = 1;
                    }

                    $sessions[] = $newSession;
                    if (!in_array($newSession->delivery, $types)) {
                        $types[] = $newSession->delivery;
                    }

                    if (!in_array($newSession->locationname, $locations)) {
                        $locations[$newSession->locationid] = $newSession->locationname;
                    }
                } catch (Exception $ex) {

                }
            }
        }

        $sqlCourses = "SELECT Id, rio_ed__Description__c, Name, Equipment_Collection_Options__c
            FROM rio_ed__Session__c
            WHERE Id in ('".implode("', '", $courseIds)."')";
        $resCourses = Forrest::query($sqlCourses);
        foreach($resCourses['records'] as $cse) {
            $newCourse = new \stdClass();
            $newCourse->id = $cse['Id'];
            $newCourse->desc = $cse['rio_ed__Description__c'];
            $newCourse->title = $cse['Name'];
            $newCourse->equipment = $cse['Equipment_Collection_Options__c'];
            $newCourse->public = $deliveries[$cse['Id']]['public'];
            $newCourse->online = $deliveries[$cse['Id']]['online'];
            $newCourse->onsite = 0;

            if(!isset($courses[$newCourse->desc])) {
                $courses[$newCourse->desc] = $newCourse;
            }
        }

        $onsiteCourses = [];
        foreach($resOnsite['records'] as $course) {
            //echo '<pre>'; var_dump($course); echo '</pre>';
            if(isset($course['Onsite_Session_Template__r']) && in_array($course['Fee__r']['Location__c'], $locs)) {
                if(isset($onsiteCourses[$course['Onsite_Session_Template__r']['rio_ed__Description__c']])) {
                    $newCourse = $onsiteCourses[$course['Onsite_Session_Template__r']['rio_ed__Description__c']];
                } else {
                    $newCourse = new \stdClass();
                    $newCourse->fees = [];
                }
                $newCourse->desc = $course['Onsite_Session_Template__r']['rio_ed__Description__c'];
                $newCourse->duration = $course['Onsite_Session_Template__r']['Duration__c'];
                $newFee = new \stdClass();
                $newFee->fee = $course['Fee__r']['Fee_Amount__c'];
                $newFee->maximum = $course['Fee__r']['Maximum_Attendees__c'];
                $newFee->batch = $course['Fee__r']['Flat_Rate__c'];
                $newCourse->fees[] = $newFee;
                $onsiteCourses[$newCourse->desc] = $newCourse;
            }
        }

        if(count($onsiteCourses) > 0) {
            $types[] = 'Onsite';
        }

        //Order courses by date
        usort($sessions,[$this, 'dateSort']);

        $r = new \stdClass();
        $r->courses = $courses;
        $r->sessions = $sessions;
        $r->onsiteCourses = $onsiteCourses;
        $r->types = $types;
        $r->locations = $locations;
        return json_encode($r);
    }

    private static function dateSort($a,$b) {
        if (strtotime($a->startdate)==strtotime($b->startdate)) {
            return 0;
        } else {
            return (strtotime($a->startdate) < strtotime($b->startdate)) ? -1 : 1;
        }
    }

    /**
     * Attempt to find a person within Salesforce
     * @param $first The given name of the person we're searching for
     * @param $last The surname of the person we're searching for
     * @return \stdClass
     */
    public function matchPeople($first, $last) {
        Forrest::authenticate();

        $sql = "select Id, FirstName, LastName, Birthdate, Email, HomePhone, MobilePhone from contact where FirstName = '".$first."' and LastName = '".$last."'";
        $res = Forrest::query($sql);

        $people = [];
        foreach($res['records'] as $person) {
            $p = new \stdClass();
            $p->id = $person['Id'];
            $p->firstname = $person['FirstName'];
            $p->lastname = $person['LastName'];
            $p->email = $person['Email'];
            $p->phone = (strlen($person['MobilePhone']) > 0 ? $person['MobilePhone'] : $person['HomePhone']);
            $p->dob = $person['Birthdate'];

            $people[] = $p;
        }

        $r = new \stdClass();
        $r->people = $people;
        return json_encode($r);
    }

    public function sendBooking(Request $request) {
        $input = file_get_contents('php://input');
        //echo '<pre>'; var_dump($input); echo '</pre>';
        $bookingData = json_decode($input, true);
        //echo '<pre>'; var_dump($bookingData); echo '</pre>';
        $time = time();

        Log::debug("Booking $time Data:\n".json_encode($bookingData));

        $oppData = [
            'method' => 'post',
            'body' => $bookingData
        ];

        Forrest::authenticate();
        //$response = Forrest::sobjects('Opportunity', $oppData);
        $response = Forrest::custom('/sf/booking', $oppData);
        Log::debug("Booking $time Response:\n".json_encode($response));

        return $response;
    }
}
