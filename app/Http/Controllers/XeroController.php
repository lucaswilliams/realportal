<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webfox\Xero\OauthCredentialManager;
use XeroAPI\XeroPHP\Models\Accounting\Contact;
use XeroAPI\XeroPHP\Api\AccountingApi;
use DB;
use Log;
use XeroAPI\XeroPHP\Configuration;
use GuzzleHttp\Client;

class XeroController extends Controller
{
    private $xeroTenantId;
    private $accessToken;
    private $refreshToken;
    private $expires;
    private $config;
    private $accountingApi;

    private function setup() {
        $dbconfig = DB::table('xero_config')
            ->get();

        $this->expires = 0;

        if(count($dbconfig) > 0) {
            $this->xeroTenantId = $dbconfig[0]->tenant_id;
            $this->accessToken = $dbconfig[0]->access_token;
            $this->refreshToken = $dbconfig[0]->refresh_token;
            $this->expires = $dbconfig[0]->expires;
        }

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => env('XERO_CLIENT_ID'),
            'clientSecret' => env('XERO_CLIENT_SECRET'),
            'redirectUri' => env('XERO_REDIRECT'),
            'urlAuthorize' => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken' => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);

        if($this->expires > 0 && $this->expires < time()) {
            $newAccessToken = $provider->getAccessToken('refresh_token', [
                'refresh_token' => $this->refreshToken
            ]);

            $this->accessToken = $newAccessToken->getToken();
            $this->expires = $newAccessToken->getExpires();
            $this->refreshToken = $newAccessToken->getRefreshToken();

            DB::table('xero_config')
                ->update([
                    'expires' => $this->expires,
                    'access_token' => $this->accessToken,
                    'refresh_token' => $this->refreshToken
                ]);
        }

        $this->config = Configuration::getDefaultConfiguration()->setAccessToken($this->accessToken);
        //var_dump($this->config); echo '<br>';
        $this->accountingApi = new AccountingApi(
            new \GuzzleHttp\Client(),
            $this->config
        );
    }

    public function index(Request $request, OauthCredentialManager $xeroCredentials)
    {
        //look in the DB for config first
        $dbconfig = DB::table('xero_config')
            ->get();
        $this->expires = 0;
        if(count($dbconfig) > 0) {
            $this->xeroTenantId = $dbconfig[0]->tenant_id;
            $this->accessToken = $dbconfig[0]->access_token;
            $this->refreshToken = $dbconfig[0]->refresh_token;
            $this->expires = $dbconfig[0]->expires;

            //There's info
            if($this->expires == 0) {
                $connected = false;
            } else {
                $connected = true;
            }
        } else {
            //There isn't.
            $connected = false;
        }

        return view('xero', [
            'connected'        => $connected,
            'error'            => $error ?? null
        ]);
    }

    public function callback() {
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => env('XERO_CLIENT_ID'),
            'clientSecret' => env('XERO_CLIENT_SECRET'),
            'redirectUri' => env('XERO_REDIRECT'),
            'urlAuthorize' => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken' => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);

        if(isset($_GET['code'])) {
            try {
                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                $config = \XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken((string)$accessToken->getToken());
                $identityApi = new \XeroAPI\XeroPHP\Api\IdentityApi(
                    new \GuzzleHttp\Client(),
                    $config
                );

                $result = $identityApi->getConnections();

                $this->accessToken = $accessToken->getToken();
                $this->expires = $accessToken->getExpires();
                $this->refreshToken = $accessToken->getRefreshToken();
                $this->xeroTenantId = $result[0]->getTenantId();

                DB::table('xero_config')
                    ->update([
                        'expires' => $this->expires,
                        'access_token' => $this->accessToken,
                        'refresh_token' => $this->refreshToken,
                        'tenant_id' => $this->xeroTenantId
                    ]);
            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                echo "Callback failed";
            }

            Log::channel('single')->info('User '.$this->user->id.' added Xero connection');
        }

        return redirect('/manage/xero');
    }

    public function delete() {
        DB::table('xero_config')
            ->update([
                'expires' => 0,
                'access_token' => '',
                'refresh_token' => '',
                'tenant_id' => ''
            ]);

        Log::channel('single')->info('User '.$this->user->id.' deleted Xero connection');

        return redirect('/manage/xero');
    }

    public function lookupContact() {
        $term = $_GET['term'];

        $this->setup();
        $if_modified_since = null;
        $where = 'Name=="'.$term.'"'; // string
        $order = null;
        $ids = null;
        $invoice_numbers = null;
        $contact_ids = null;
        $statuses = null;
        $page = 1;
        $include_archived = null;

        try {
            $apiResponse = $this->accountingApi->getContacts($this->xeroTenantId, $if_modified_since, $where, $order, $ids, $invoice_numbers, $contact_ids, $statuses, $page, $include_archived);
            $contacts = $apiResponse->getContacts();
            $clist = [];
            //var_dump($contacts);
            if (  count($contacts) > 0 ) {
                foreach($contacts as $contact) {
                    //var_dump($contact);
                    $clist[] = ['id' => $contact->getContactId(), 'text' => $contact->getName()];
                }
            } else {
                $message = "No contacts found matching filter criteria";
            }

            return json_encode($clist);

            echo $message;
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->getContacts: ', $e->getMessage(), PHP_EOL;
        }
        return json_encode([]);
    }

    public function getContacts(Request $request) {
        $this->setup();
        $content = $this->convertPost($request);
        echo '<pre>'; var_dump($content); echo '</pre>';

        $name = str_replace('\\', '', (strlen(trim($content['organisation'])) > 0 ? $content['organisation'] : $content['first_name'].' '.$content['last_name'].' ('.$content['email'].')'));

        $if_modified_since = null;
        $where = 'Name=="'.$name.'"'; // string
        $order = null;
        $ids = null;
        $invoice_numbers = null;
        $contact_ids = null;
        $statuses = null;
        $page = 1;
        $include_archived = null;

        try {
            $apiResponse = $this->accountingApi->getContacts($this->xeroTenantId, $if_modified_since, $where, $order, $ids, $invoice_numbers, $contact_ids, $statuses, $page, $include_archived);
            if (  count($apiResponse->getContacts()) > 0 ) {
                $message = 'Total contacts found: ' . count($apiResponse->getContacts());
            } else {
                $message = "No contacts found matching filter criteria";
            }

            echo $message;
        } catch (Exception $e) {
            echo 'Exception when calling AccountingApi->getContacts: ', $e->getMessage(), PHP_EOL;
        }
    }

    public function getOrCreateContact(Request $request) {
        $this->setup();
        //$content = $this->convertPost($request);
        $content = $_REQUEST;
        //echo '<pre>'; var_dump($content); echo '</pre>';

        $search_name = str_replace('\\', '', (strlen(trim($content['organisation'])) > 0 ? $content['organisation'] : $content['first_name'].' '.$content['last_name']));
        $name = $search_name.' ('.$content['email'].')';

        $content['name'] = $name;

        $if_modified_since = null;
        //$where = 'Name=="'.$search_name.'"'; // string
        $where = 'Name.StartsWith("'.$search_name.'")';
        $order = null;
        $ids = null;
        $invoice_numbers = null;
        $contact_ids = null;
        $statuses = null;
        $page = 1;
        $include_archived = null;

        try {
            $apiResponse = $this->accountingApi->getContacts($this->xeroTenantId, $if_modified_since, $where, $order, $ids, $invoice_numbers, $contact_ids, $statuses, $page, $include_archived);
            $contacts = $apiResponse->getContacts();
            $outcontacts = [];
            if (count($contacts) > 0) {
                foreach($contacts as $contact) {
                    $outcontacts[] = [
                        'first_name' => $contact->getFirstName(),
                        'last_name' => $contact->getLastName(),
                        'email' => $contact->getEmailAddress(),
                        'organisation' => $contact->getName(),
                        'guid' => $contact->getContactId()
                    ];
                }
            } else {
                //Create a new contact
                //$contact = $this->createContact($content);
                //$outcontacts[] = $contact;
            }

            echo json_encode($outcontacts);


        } catch (Exception $e) {
            echo $e->getMessage(), PHP_EOL;
        }
    }

    private function createContact($content) {
        try {
            $contact = new Contact;
            $contact->setName($content['name'])
                ->setFirstName($content['first_name'])
                ->setLastName($content['last_name'])
                ->setEmailAddress($content['email']);

            $apiResponse = $this->accountingApi->createContacts($this->xeroTenantId, $contact);
            $content['guid'] = $apiResponse[0]->getContactId();
        } catch (\XeroAPI\XeroPHP\ApiException $e) {
            echo $e->getResponseBody();
        }

        return $content;
    }
}
