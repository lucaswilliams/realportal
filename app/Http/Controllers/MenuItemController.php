<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MenuItemController extends Controller
{
    private function getQuery() {
        $q = DB::table('menu_items');

        return $q;
    }

    public function getList($children = false) {
        $items = $this->getQuery()->whereNull('parent_id')->orderBy('menu_order', 'asc')->orderBy('label')->get();
        $itm = [];
        foreach($items as $item) {
            $itm[] = $item;
            if($children) {
                $children = $this->getChildren($item->id, 1);
                $itm += $children;
            }
        }
        return $itm;
    }

    private function getChildren($id, $iterations) {
        $items = $this->getQuery()->where('parent_id', $id)->orderBy('menu_order', 'asc')->orderBy('label')->get();

        $itemlist = array();
        foreach($items as $item) {
            $item->label = '<span class="indent-'.$iterations.'">'.$item->label.'</span>';
            $itemlist[$item->id] = $item;
            $itemlist += $this->getChildren($item->id, $iterations + 1);
        }

        return $itemlist;
    }

    public function getRoles($id) {
        $roles = DB::table('roles')
            ->leftJoin('menu_item_roles', function($join) use ($id) {
                $join->on('menu_item_roles.role_id', '=', 'roles.id')
                    ->where('menu_item_roles.menu_id', '=', $id);
                })
            ->select('roles.id', 'roles.name', 'menu_item_roles.menu_id')
            ->get();

        return $roles;
    }

    public function showList() {
        $items = $this->getList(true);

        return view('menuitems.list', ['items' => $items]);
    }

    public function getOne($id) {
        $query = $this->getQuery()->where('id', $id)->get();
        return $query[0] ?? null;
    }

    public function showForm($id = 0) {
        if($id > 0) {
            $item = $this->getOne($id);
        } else {
            $item = new \stdClass();
            $item->id = 0;
            $item->label = '';
            $item->route = '';
            $item->parent_id = '';
            $item->menu_order = '';
        }

        $roles = $this->getRoles($id);
        $parents = $this->getList(false);

        return view('menuitems.form', ['item' => $item, 'roles' => $roles, 'parents' => $parents]);
    }

    public function saveForm(Request $request) {
        $id = $request->id;
        $order = $request->menu_order;
        if($order == '' || $order == 0) {
            //put it at the end.
            if($request->parent_id > 0) {
                $ids = DB::table('menu_items')->where('parent_id', $request->parent_id)->where('menu_order', '<', 99)->max('menu_order');
            } else {
                $ids = DB::table('menu_items')->whereNull('parent_id')->where('menu_order', '<', 99)->max('menu_order');
            }
            $order = $ids + 1;
        }

        $parent_id = null;
        if($request->parent_id > 0) {
            $parent_id = $request->parent_id;
        }

        $data = [
            'label' => $request->label,
            'route' => $request->route,
            'parent_id' => $parent_id,
            'menu_order' => $order
        ];

        if($id > 0) {
            //echo '<pre>'; var_dump($data); echo '</pre>';
            DB::table('menu_items')
                ->where('id', $id)
                ->update($data);
        } else {
            //Insert
            $id = DB::table('menu_items')
                ->insertGetId($data);
        }

        //delete all role associations
        DB::table('menu_item_roles')
            ->where('menu_id', $id)
            ->delete();

        //var_dump($request->role_id);
        //then put in all of the roles
        if(isset($request->role_id) && is_array($request->role_id) && count($request->role_id) > 0) {
            foreach ($request->role_id as $idx => $role) {
                DB::table('menu_item_roles')
                    ->insert([
                        'menu_id' => $id,
                        'role_id' => $role
                    ]);
            }
        }

        return redirect('/menuitems');
    }

    public function delete($id) {
        DB::table('menu_items')
            ->where('id', $id)
            ->delete();
        return redirect('/menuitems');
    }
}
