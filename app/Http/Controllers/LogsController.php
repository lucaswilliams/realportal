<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class LogsController extends Controller
{
    public function showList()
    {
        $filepath = storage_path('logs');
        $di = new \DirectoryIterator($filepath);
        $logs = [];
        foreach($di as $fileinfo) {
            $filename = $fileinfo->getFilename();
            if(strpos($filename, '.log')) {
                $filename = str_replace(['laravel-', '.log'], '', $filename);
                //echo $filename.'<br>';
                $logs[] = $filename;
            }
        }

        rsort($logs);

        return view('logs.list', ['logs' => $logs]);
    }

    public function showLogs($date) {
        if($date == 'laravel') {
            $filepath = storage_path('logs/'.$date.'.log');
        } else {
            $filepath = storage_path('logs/laravel-'.$date.'.log');
        }
        $content = File::get($filepath);
        return view('logs.single', ['content' => '<pre>'.$content.'</pre>']);
        /*
        $contentparts = explode("\n", $content);
        $logcontent = [];
        $i = -1;
        foreach($contentparts as $part) {
            if(substr($part, 0, 1) == "[") {
                //Title
                $i++;
                $logcontent[$i]['title'] = $part;
                $logcontent[$i]['content'] = '';
            } else {
                //Body
                $logcontent[$i]['content'] .= $part;
            }
        }
        $outcontent = '<div class="accordion" id="accordionExample">';
        foreach($logcontent as $key => $log) {
            $outcontent .= '<div class="card">
                    <div class="card-header" id="heading'.$key.'">
                        <a class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse'.$key.'" aria-expanded="false" aria-controls="collapse'.$key.'">'.$log['title'].'</a>
                    </div>';
            if(strlen($log['content']) > 0) {
                $outcontent .= '<div id="collapse' . $key . '" class="collapse" aria-labelledby="heading' . $key . '" data-parent="#accordionExample">
                        <div class="card-body"><pre>' . $log['content'] . '</pre></div>
                    </div>';
            }
            $outcontent .= '</div>';
        }
        $outcontent .= '</div>';

        return view('logs.single', ['content' => $outcontent]);*/
    }

    public function processWebhook(Request $request) {
        $content = $this->convertPost($request);

        file_put_contents(storage_path('logs/webhook-'.date('Y-m-d_h:i:s').'.log'), print_r($content, true));
    }
}
