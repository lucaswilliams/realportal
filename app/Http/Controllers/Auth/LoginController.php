<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Request;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*public function username() {
        return 'username';
    }*/

    public function apiLogin(Request $request) {
        $email = $_POST['email'] ?? '';
        $password = $_POST['password'] ?? '';

        $query = DB::table('users')
            ->where('email', $email);

        $user = $query->get();
        if(count($user) > 0) {
            if(!Hash::check($password, $user[0]->password)) {
                return response('Email or password is incorrect', 400);
            }

            //In all cases, we make a token!
            $token = hash('sha256', \Str::random(60));
            DB::table('users')
                ->where('id', $user[0]->id)
                ->update([
                    'api_token' => $token,
                ]);

            return ['token' => $token];
        }

        return response('Email or password is incorrect', 400);
    }
}
