<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WebhookController extends Controller
{
    private function saveMoodle($type) {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE); //convert JSON into array
        $myinput = json_encode($input, JSON_PRETTY_PRINT);

        DB::table('moodle_post')
            ->insert([
                'env' => $type,
                'postdata' => $myinput,
                'submitted' => date('Y-m-d H:i:s')
            ]);
    }

    public function receiveMoodle() {
        $this->saveMoodle('live');
    }

    public function receiveMoodleBeta() {
        $this->saveMoodle('beta');
    }

    public function prepareMoodle() {
        echo '<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">

                <meta name="robots" content="noindex,nofollow">

                <title>Send To Moodle</title>

                <!-- Scripts -->
                <script src="'.asset('js/app.js').'" defer></script>

                <!-- Fonts -->
                <link rel="dns-prefetch" href="//fonts.gstatic.com">
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&display=swap" rel="stylesheet">

                <!-- Styles -->
                <link href="'.asset('css/bootstrap.css').'" rel="stylesheet">
                <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
                <link href="'.asset('css/custom.css').'" rel="stylesheet">
                <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
                <link href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css" rel="stylesheet" />
            </head>
            <body>
                <div id="app">
                    <div class="accordion" id="accordionExample">';
        $lrres = DB::connection('moodle')->table('mdl_rr_result_config')->get();
        $lastrun = '1970-01-01 00:00:00';
        $nextrun = date('Y-m-d H:i:s');
        foreach($lrres as $lrrow) {
            $lastrun = $lrrow->last_run;
        }

        $res = DB::connection('moodle')->table('mdl_rr_results')
            ->where('timestamp', '>', $lastrun)
            ->select('userid', 'courseid')->distinct()
            ->get();

        $finals = [];
        foreach($res as $row) {
            //$row is only those with the same timestamp.  We now need all userid/courseid records
            $res2 = DB::connection('moodle')->table('mdl_rr_results')
                ->where('courseid', $row->courseid)
                ->where('userid', $row->userid)
                ->get();
            foreach($res2 as $row2) {
                $finals[$row2->courseid][$row2->userid][$row2->activity] = ['complete' => $row2->complete, 'date' => $row2->timestamp];
            }
        }

        $i = 1;
        foreach($finals as $course => $cdata) {
            foreach($cdata as $student => $assessments) {
                echo '<div class="card">
            <div class="card-header" id="heading'.$i.'">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse'.$i.'" aria-expanded="false" aria-controls="collapse'.$i.'">
                    course: '.$course.'; userid: '.$student.';<br>
                </button>
              </h2>
            </div>
            <div id="collapse'.$i.'" class="collapse" aria-labelledby="heading'.$i.'" data-parent="#accordionExample">
              <div class="card-body">';
                $complete = true;
                $onlineLearning = false;
                $mydate = null;
                foreach($assessments as $activity => $assessment) {
                    echo '<pre>'; var_dump($assessment); echo '</pre>';
                    if($assessment['complete'] == 0) {
                        $complete = false;
                    } else {
                        if($activity == 'scorm') {
                            $onlineLearning = true;
                            $mydate = date('Y-m-d', strtotime($assessment['date']));
                        }
                    }
                }

                if($complete) {
                    $this->sendComplete($course, $student, true, $mydate);
                    //do any others complete with this one.
                    $extracomplete = DB::connection('moodle')->table('mdl_course_completion_criteria')
                        ->where('courseinstance', $course)
                        ->get();
                    foreach($extracomplete as $toComplete) {
                        //complete them too.
                        $this->sendComplete($toComplete->course, $student, true, $mydate);
                    }
                } else {
                    if($onlineLearning) {
                        //only online learning is complete.  Only send that
                        $this->sendOnlineComplete($course, $student, $mydate);
                    }
                }
                echo '</div></div></div>';
                $i++;
            }
        }
        echo '</div></div>
        <script type="text/javascript" src="'.asset('js/jquery.min.js').'"></script>
        <script type="text/javascript" src="'.asset('js/popper.min.js').'"></script>
        <script type="text/javascript" src="'.asset('js/bootstrap.min.js').'"></script>
        <script type="text/javascript" src="'.asset('js/jquery.validate.min.js').'"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
        <script type="text/javascript" src="'.asset('js/app.js').'"></script>
    </body>
</html>';

        DB::connection('moodle')->table('mdl_rr_result_config')->update([
            'last_run' => $nextrun
        ]);
    }

    private function sendComplete($courseid, $userid, $online, $date) {
        $webhookData = new \stdClass();
        $webhookData->finalGrade = 'Competency_achieved';
        //$webhookData->assessmentID = '';
        $webhookData->assessmentAttemptedDate = $date;
        $webhookData->onlineAssessmentCompletion = $online;

        echo 'Sending course completion.  Course: '.$courseid.' User: ' .$userid.'<br>';
        $this->sendWebhookData($courseid, $userid,$webhookData);
    }

    private function sendOnlineComplete($courseid, $userid, $date) {
        $webhookData = new \stdClass();
        $webhookData->assessmentAttemptedDate = $date;
        $webhookData->onlineAssessmentCompletion = true;

        echo 'Sending online completion.  Course: '.$courseid.' User: ' .$userid.'<br>';
        $this->sendWebhookData($courseid, $userid,$webhookData);
    }

    private function sendWebhookData($courseid, $userid, $webhookData) {
        echo '<pre>'; var_dump($webhookData); echo '</pre>';
        //look up the person.
        $sql = 'SELECT ue.userid, e.courseid, u.firstname, u.lastname, ue.salesforceID AS SFUnitEnrolmentID, d.`data` AS SFContactID
            FROM mdl_user_enrolments ue
            JOIN mdl_user u ON u.id = ue.userid
            JOIN mdl_enrol e ON e.id = ue.enrolid
            JOIN mdl_user_info_data d ON d.userid = u.id
            JOIN mdl_user_info_field f ON f.id = d.fieldid
            WHERE ue.userid = '.$userid.' AND courseid = '.$courseid.' AND f.shortname = \'SFContactID\'';
        $person = DB::connection('moodle')->select($sql);

        if(count($person) > 0) {
            $p = $person[0];
            $webhookData->sfContactID = $p->SFContactID;
            $webhookData->contactFName = $p->firstname;
            $webhookData->contactLName = $p->lastname;
            $webhookData->authCode = 'Nzk1NGQ1YzMtNTcwNi00ODJmLTkwZjEtNzNjZWQwNzk4Yzg4';

            $enrolments = [];
            $enrolment = new \stdClass();
            $enrolment->sfUnitEnrolmentID = $p->SFUnitEnrolmentID;
            $enrolments[] = $enrolment;

            $webhookData->unitEnrolments = $enrolments;

            $url = 'https://wdci-h.cyclr.com/api/webhook/EpPYP2WM';

            $fieldsstring = json_encode($webhookData);
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            if(strpos($_SERVER['HTTP_HOST'], '.local') !== false) {
                curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            }
            echo '<pre>'.$fieldsstring.'</pre>';
            $curl_response = curl_exec($curl);
            echo '<pre>'; var_dump($curl_response); echo '</pre>';
        }
    }

    public function processMoodle() {
        //Find all non-processed ones
        //Do some stuff to look in the moodle database and get the extra ids
        $togo = DB::table('moodle_post')
            ->where('processed', 0)
            ->get();

        /*
        $finalGrades['NYS'] = 'Not Yet Started';
        $finalGrades['CON'] = 'Continuing Activity';
        $finalGrades['CA'] = 'Competent';
        $finalGrades['CT'] = 'Credit Transfer';
        $finalGrades['RPL'] = 'Recognition of Prior Learning';
        $finalGrades['CNA'] = 'Competency Not Achieved';
        $finalGrades['WD'] = 'Withdrawn';
        */

        foreach($togo as $item) {
            //Look at the item's text to see if it was a SCORM pack (online learning complete)
        }
    }
}
