<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function getList() {
        $filters = $_GET['filter'];
        $studentQ = DB::table('users')
            ->where('user_type', 1);

        $students = $studentQ->get();

        return $students;
    }

    public function getOne($id) {
        $studentQ = DB::table('users')
            ->where('id', $id);

        $student = $studentQ->get();

        return $student;
    }

    public function showList() {
        $students = $this->getList();
        return view('students.list', $students);
    }

    public function showOne($id) {
        $student = $this->getOne($id);
        return view('students.form', $student);
    }
}
